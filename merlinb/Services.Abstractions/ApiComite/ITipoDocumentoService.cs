﻿using Contracts.DataTransferObjects.ApiComite.TipoDocumentoDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface ITipoDocumentoService
    {
        Task<List<TipoDocumentoDto>> GetAllAsync();
        Task<TipoDocumentoDto> GetByIdAsync(int id);
        Task<TipoDocumentoDto> CreateAsync(TipoDocumentoCUDto tipoDocumentoCUDto);
        Task<TipoDocumentoDto> UpdateAsync(int id, TipoDocumentoCUDto tipoDocumentoCUDto);
        Task<bool> StateChange(int id);
    }
}
