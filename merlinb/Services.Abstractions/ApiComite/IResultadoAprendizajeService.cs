﻿using Contracts.DataTransferObjects.ApiComite.ResultadoAprendizajeDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IResultadoAprendizajeService
    {
        Task<List<ResultadoAprendizajeDto>> GetAllAsync();
        Task<ResultadoAprendizajeDtoIncludes> GetByIdAsync(int id);
        Task<ResultadoAprendizajeDto> CreateAsync(ResultadoAprendizajeCUDto resultadoAprendizajeCUDto);
        Task<bool> UpdateAsync(int id, ResultadoAprendizajeCUDto resultadoAprendizajeCUDto);
        Task<bool> StateChange(int id);
    }
}
