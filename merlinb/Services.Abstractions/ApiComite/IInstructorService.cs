﻿using Contracts.DataTransferObjects.ApiComite.InstructorDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IInstructorService
    {
        Task<List<InstructorDto>> GetAllAsync();
        Task<InstructorDtoIncludes> GetByIdAsync(int id);
        Task<InstructorDto> CreateAsync(InstructorCUDto instructorCUDto);
        Task<bool> UpdateAsync(int id, InstructorCUDto instructorCUDto);
        Task<bool> StateChange(int id);
    }
}
