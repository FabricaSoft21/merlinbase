﻿using Contracts.DataTransferObjects.ApiComite.ComiteDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IComiteService
    {
        Task<List<ComiteDto>> GetAllAsync();
        Task<ComiteDtoInclude> GetByIdAsync(int id);
        Task<ComiteDto> CreateAsync(ComiteCUDto comiteCUDto);
        Task<bool> UpdateAsync(int id, ComiteCUDto comiteCUDto);
        Task<bool> StateChange(int id);
    }
}
