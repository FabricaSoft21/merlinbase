﻿using Contracts.DataTransferObjects.ApiComite.ProgramaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IProgramaService
    {
        Task<List<ProgramaDtoInclude>> GetAllAsync();
        Task<ProgramaDtoInclude> GetByIdAsync(int id);
        Task<ProgramaDto> CreateAsync(ProgramaCUDto programaCUDto);
        Task<ProgramaDtoInclude> UpdateAsync(int id, ProgramaCUDto programaCUDto);
        Task<bool> StateChange(int id);
    }
}
