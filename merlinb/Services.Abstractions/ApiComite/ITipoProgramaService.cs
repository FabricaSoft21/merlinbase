﻿using Contracts.DataTransferObjects.ApiComite.TipoProgramaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface ITipoProgramaService
    {
        Task<List<TipoProgramaDto>> GetAllAsync();
        Task<TipoProgramaDto> GetByIdAsync(int id);
        Task<TipoProgramaDto> CreateAsync(TipoProgramaCUDto tipoProgramaCUDto);
        Task<TipoProgramaDto> UpdateAsync(int id, TipoProgramaCUDto tipoProgramaCUDto);
        Task<bool> StateChange(int id);
    }
}
