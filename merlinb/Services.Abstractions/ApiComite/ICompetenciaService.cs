﻿using Contracts.DataTransferObjects.ApiComite.CompetenciaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface ICompetenciaService
    {
        Task<List<CompetenciaDtoInclude>> GetAllAsync();
        Task<CompetenciaDtoInclude> GetByIdAsync(int id);
        Task<CompetenciaDto> CreateAsync(CompetenciaCUDto competenciaCUDto);
        Task<bool> UpdateAsync(int id, CompetenciaCUDto competenciaCUDto);
        Task<bool> StateChange(int id);
    }
}
