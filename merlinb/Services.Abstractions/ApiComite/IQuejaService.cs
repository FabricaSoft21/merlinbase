﻿using Contracts.DataTransferObjects.ApiComite.QuejaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IQuejaService
    {
        Task<List<QuejaDto>> GetAllAsync();
        Task<QuejaDtoIncludes> GetByIdAsync(int id);
        Task<QuejaDto> CreateAsync(QuejaCUDto quejaCUDto);
        Task<bool> UpdateAsync(int id, QuejaCUDto quejaCUDto);
        Task<bool> StateChange(int id);
    }
}
