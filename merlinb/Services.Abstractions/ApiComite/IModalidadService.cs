﻿using Contracts.DataTransferObjects.ApiComite.ModalidadDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IModalidadService
    {
        Task<List<ModalidadDto>> GetAllAsync();
        Task<ModalidadDto> GetByIdAsync(int id);
        Task<ModalidadDto> CreateAsync(ModalidadCUDto modalidadCUDto);
        Task<ModalidadDto> UpdateAsync(int id, ModalidadCUDto modalidadCUDto);
        Task<bool> StateChange(int id);
    }
}
