﻿using Contracts.DataTransferObjects.ApiComite.EstadoQuejaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IEstadoQuejaService
    {
        Task<List<EstadoQuejaDto>> GetAllAsync();
        Task<EstadoQuejaDto> GetByIdAsync(int id);
        Task<EstadoQuejaDto> CreateAsync(EstadoQuejaCUDto estadoQuejaCUDto);
        Task<EstadoQuejaDto> UpdateAsync(int id, EstadoQuejaCUDto estadoQuejaCUDto);
        Task<bool> StateChange(int id);
    }
}
