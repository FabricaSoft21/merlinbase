﻿using Contracts.DataTransferObjects.ApiComite.TipoQuejaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface ITipoQuejaService
    {
        Task<List<TipoQuejaDto>> GetAllAsync();
        Task<TipoQuejaDto> GetByIdAsync(int id);
        Task<TipoQuejaDto> CreateAsync(TipoQuejaCUDto tipoQuejaCUDto);
        Task<TipoQuejaDto> UpdateAsync(int id, TipoQuejaCUDto tipoQuejaCUDto);
        Task<bool> StateChange(int id);
    }
}
