﻿using Contracts.DataTransferObjects.ApiComite.FichaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IFichaService
    {
        Task<List<FichaDtoInclude>> GetAllAsync();
        Task<FichaDtoInclude> GetByIdAsync(int id);
        Task<FichaDto> CreateAsync(FichaCUDto fichaCUDto);
        Task<FichaDtoInclude> UpdateAsync(int id, FichaCUDto fichaCUDto);
        Task<bool> StateChange(int id);
    }
}
