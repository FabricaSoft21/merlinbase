﻿using Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface ICoordinacionAcademicaService
    {
        Task<List<CoordinacionAcademicaDtoIncludes>> GetAllAsync();
        Task<CoordinacionAcademicaDtoIncludes> GetByIdAsync(int id);
        Task<CoordinacionAcademicaDto> CreateAsync(CoordinacionAcademicaCUDto coordinacionAcademicaCUDto);
        Task<CoordinacionAcademicaDtoIncludes> UpdateAsync(int id, CoordinacionAcademicaCUDto coordinacionAcademicaCUDto);
        Task<bool> StateChange(int id);
    }
}
