﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IServiceManager
    {
        ITipoDocumentoService TipoDocumentoService { get; }
        ITipoProgramaService TipoProgramaService { get; }
        IModalidadService ModalidadService { get; }
        ITipoQuejaService TipoQuejaService { get; }
        IEstadoQuejaService EstadoQuejaService { get; }
        ICoordinacionAcademicaService CoordinacionAcademicaService { get; }
        IFichaService FichaService { get; }
        IInstructorService InstructorService { get; }
        IAprendizService AprendizService { get; }
        IQuejaService QuejaService { get; }
        IResultadoAprendizajeService ResultadoAprendizajeService { get; }
        ICompetenciaService CompetenciaService { get; }
        IProgramaService ProgramaService { get; }
        IComiteService ComiteService { get; }
    }
}
