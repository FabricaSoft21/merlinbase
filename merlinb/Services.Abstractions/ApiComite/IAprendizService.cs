﻿using Contracts.DataTransferObjects.ApiComite.AprendizDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions.ApiComite
{
    public interface IAprendizService
    {
        Task<List<AprendizDtoIncludes>> GetAllAsync();
        Task<AprendizDtoIncludes> GetByIdAsync(int id);
        Task<AprendizDto> CreateAsync(AprendizCUDto aprendizCUDto);
        Task<AprendizDtoIncludes> UpdateAsync(int id, AprendizCUDto aprendizCUDto);
        Task<bool> StateChange(int id);
    }
}
