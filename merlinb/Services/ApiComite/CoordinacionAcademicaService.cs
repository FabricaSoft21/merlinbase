﻿using Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.CoordinacionAcademica;
using Domain.Exceptions.ApiComite.TipoDocumento;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class CoordinacionAcademicaService : ICoordinacionAcademicaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public CoordinacionAcademicaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<CoordinacionAcademicaDtoIncludes>> GetAllAsync()
        {
            List<CoordinacionAcademica> coordinacionesAcademicas = await _repositoryManager.CoordinacionAcademicaRepository.GetAllAsync();
            List<CoordinacionAcademicaDtoIncludes> coordinacionAcademicaDtoIncludes = new();
            coordinacionesAcademicas.ForEach(x =>
            {
                coordinacionAcademicaDtoIncludes.Add(new CoordinacionAcademicaDtoIncludes
                {
                    IdCoordinacionAcademica = x.IdCoordinacionAcademica,
                    NumDocumento = x.NumDocumento,
                    Nombre = x.Nombre,
                    Apellido = x.Apellido,
                    Email = x.Email,
                    Telefono = x.Telefono,
                    TipoDocumentoId = x.TipoDocumentoId,
                    NombreTipoDocumento = x.TipoDocumento.Nombre
                }) ;
            });
            return coordinacionAcademicaDtoIncludes;
        }

        public async Task<CoordinacionAcademicaDtoIncludes> GetByIdAsync(int id)
        {
            CoordinacionAcademica coordinacionAcademica = await _repositoryManager.CoordinacionAcademicaRepository.GetByIdAsync(id);
            if (coordinacionAcademica is null)
                throw new CoordinacionAcademicaNotFoundException(id);
            CoordinacionAcademicaDtoIncludes coordinacionAcademicaDtoIncludes = new()
            {
                IdCoordinacionAcademica = coordinacionAcademica.IdCoordinacionAcademica,
                NumDocumento = coordinacionAcademica.NumDocumento,
                Nombre = coordinacionAcademica.Nombre,
                Apellido = coordinacionAcademica.Apellido,
                Email = coordinacionAcademica.Email,
                Telefono = coordinacionAcademica.Telefono,
                TipoDocumentoId = coordinacionAcademica.TipoDocumentoId,
                NombreTipoDocumento = coordinacionAcademica.TipoDocumento.Nombre,
            };
            return coordinacionAcademicaDtoIncludes;
        }

        public async Task<CoordinacionAcademicaDto> CreateAsync(CoordinacionAcademicaCUDto coordinacionAcademicaCUDto)
        {
            int existeNumDocumentoEmail = await _repositoryManager.CoordinacionAcademicaRepository.GetByNumDocumentoEmailIsUnique(coordinacionAcademicaCUDto.NumDocumento, coordinacionAcademicaCUDto.Email);
            if (existeNumDocumentoEmail == 1)
                throw new CoordinacionAcademicaNumDocumentoIsNotUniqueBadRequestException(coordinacionAcademicaCUDto.NumDocumento);
            if (existeNumDocumentoEmail == 2)
                throw new CoordinacionAcademicaEmailIsNotUniqueBadRequestException(coordinacionAcademicaCUDto.Email);
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(coordinacionAcademicaCUDto.TipoDocumentoId.Value);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(coordinacionAcademicaCUDto.TipoDocumentoId.Value);
            coordinacionAcademicaCUDto.NumDocumento = coordinacionAcademicaCUDto.NumDocumento.Trim();
            coordinacionAcademicaCUDto.Nombre = coordinacionAcademicaCUDto.Nombre.Trim();
            coordinacionAcademicaCUDto.Apellido = coordinacionAcademicaCUDto.Apellido.Trim();
            coordinacionAcademicaCUDto.Email = coordinacionAcademicaCUDto.Email.Trim().ToLower();
            coordinacionAcademicaCUDto.Telefono = coordinacionAcademicaCUDto.Telefono.Trim();
            CoordinacionAcademica coordinacionAcademica = coordinacionAcademicaCUDto.Adapt<CoordinacionAcademica>();
            _repositoryManager.CoordinacionAcademicaRepository.Insert(coordinacionAcademica);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return coordinacionAcademica.Adapt<CoordinacionAcademicaDto>();
        }

        public async Task<CoordinacionAcademicaDtoIncludes> UpdateAsync(int id, CoordinacionAcademicaCUDto coordinacionAcademicaCUDto)
        {
            CoordinacionAcademica coordinacionAcademica = await _repositoryManager.CoordinacionAcademicaRepository.GetByIdAsync(id);
            if (coordinacionAcademica is null)
                throw new CoordinacionAcademicaNotFoundException(id);
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(coordinacionAcademicaCUDto.TipoDocumentoId.Value);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(coordinacionAcademicaCUDto.TipoDocumentoId.Value);
            coordinacionAcademica.NumDocumento = coordinacionAcademicaCUDto.NumDocumento.Trim();
            coordinacionAcademica.Nombre = coordinacionAcademicaCUDto.Nombre.Trim();
            coordinacionAcademica.Apellido = coordinacionAcademicaCUDto.Apellido.Trim();
            coordinacionAcademica.Email = coordinacionAcademicaCUDto.Email.Trim().ToLower();
            coordinacionAcademica.Telefono = coordinacionAcademicaCUDto.Telefono.Trim();
            coordinacionAcademica.TipoDocumentoId = coordinacionAcademicaCUDto.TipoDocumentoId.Value;
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            CoordinacionAcademicaDtoIncludes coordinacionAcademicaDtoIncludes = new()
            {
                IdCoordinacionAcademica = id,
                NumDocumento = coordinacionAcademicaCUDto.NumDocumento,
                Nombre = coordinacionAcademicaCUDto.Nombre,
                Apellido = coordinacionAcademicaCUDto.Apellido,
                Email = coordinacionAcademicaCUDto.Email,
                Telefono = coordinacionAcademicaCUDto.Telefono,
                TipoDocumentoId = coordinacionAcademicaCUDto.TipoDocumentoId.Value,
                NombreTipoDocumento = coordinacionAcademica.TipoDocumento.Nombre
            };
            return coordinacionAcademicaDtoIncludes;
        }

        public async Task<bool> StateChange(int id)
        {
            CoordinacionAcademica coordinacionAcademica = await _repositoryManager.CoordinacionAcademicaRepository.GetByIdNotWhereAsync(id);
            if (coordinacionAcademica is null)
                throw new CoordinacionAcademicaNotFoundException(id);
            if (coordinacionAcademica.Estado)
                coordinacionAcademica.Estado = false;
            else
                coordinacionAcademica.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
