﻿using Contracts.DataTransferObjects.ApiComite.ModalidadDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Modalidad;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class ModalidadService : IModalidadService
    {
        private readonly IRepositoryManager _repositoryManager;
        public ModalidadService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<ModalidadDto>> GetAllAsync()
        {
            List<Modalidad> modalidades = await _repositoryManager.ModalidadRepository.GetAllAsync();
            return modalidades.Adapt<List<ModalidadDto>>();
        }

        public async Task<ModalidadDto> GetByIdAsync(int id)
        {
            Modalidad modalidad = await _repositoryManager.ModalidadRepository.GetByIdAsync(id);
            if (modalidad is null)
                throw new ModalidadNotFoundException(id);
            return modalidad.Adapt<ModalidadDto>();
        }

        public async Task<ModalidadDto> CreateAsync(ModalidadCUDto modalidadCUDto)
        {
            int existeNombre = await _repositoryManager.ModalidadRepository.GetByNombreIsUnique(modalidadCUDto.Nombre);
            if (existeNombre == 1)
                throw new ModalidadNombreIsNotUniqueBadRequestException(modalidadCUDto.Nombre);
            modalidadCUDto.Nombre = modalidadCUDto.Nombre.Trim();
            Modalidad modalidad = modalidadCUDto.Adapt<Modalidad>();
            _repositoryManager.ModalidadRepository.Insert(modalidad);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return modalidad.Adapt<ModalidadDto>();
        }

        public async Task<ModalidadDto> UpdateAsync(int id, ModalidadCUDto modalidadCUDto)
        {
            Modalidad modalidad = await _repositoryManager.ModalidadRepository.GetByIdAsync(id);
            if (modalidad is null)
                throw new ModalidadNotFoundException(id);
            modalidad.Nombre = modalidadCUDto.Nombre.Trim();
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return modalidad.Adapt<ModalidadDto>();
        }

        public async Task<bool> StateChange(int id)
        {
            Modalidad modalidad = await _repositoryManager.ModalidadRepository.GetByIdNotWhereAsync(id);
            if (modalidad is null)
                throw new ModalidadNotFoundException(id);
            if (modalidad.Estado)
                modalidad.Estado = false;
            else
                modalidad.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
