﻿using Contracts.DataTransferObjects.ApiComite.TipoDocumentoDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.TipoDocumento;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class TipoDocumentoService : ITipoDocumentoService
    {
        private readonly IRepositoryManager _repositoryManager;
        public TipoDocumentoService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<TipoDocumentoDto>> GetAllAsync()
        {
            List<TipoDocumento> tiposDocumentos = await _repositoryManager.TipoDocumentoRepository.GetAllAsync();
            return tiposDocumentos.Adapt<List<TipoDocumentoDto>>();
        }

        public async Task<TipoDocumentoDto> GetByIdAsync(int id)
        {
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(id);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(id);
            return tipoDocumento.Adapt<TipoDocumentoDto>();
        }

        public async Task<TipoDocumentoDto> CreateAsync(TipoDocumentoCUDto tipoDocumentoCUDto)
        {
            int existeNombre = await _repositoryManager.TipoDocumentoRepository.GetByNombreIsUnique(tipoDocumentoCUDto.Nombre);
            if (existeNombre == 1)
                throw new TipoDocumentoNombreIsNotUniqueBadRequestException(tipoDocumentoCUDto.Nombre);
            tipoDocumentoCUDto.Nombre = tipoDocumentoCUDto.Nombre.Trim();
            TipoDocumento tipoDocumento = tipoDocumentoCUDto.Adapt<TipoDocumento>();
            _repositoryManager.TipoDocumentoRepository.Insert(tipoDocumento);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoDocumento.Adapt<TipoDocumentoDto>();
        }

        public async Task<TipoDocumentoDto> UpdateAsync(int id, TipoDocumentoCUDto tipoDocumentoCUDto)
        {
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(id);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(id);
            tipoDocumento.Nombre = tipoDocumentoCUDto.Nombre.Trim();
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoDocumento.Adapt<TipoDocumentoDto>();
        }

        public async Task<bool> StateChange(int id)
        {
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdNotWhereAsync(id);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(id);
            if (tipoDocumento.Estado)
                tipoDocumento.Estado = false;
            else
                tipoDocumento.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
