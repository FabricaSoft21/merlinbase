﻿using Contracts.DataTransferObjects.ApiComite.TipoProgramaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.TipoPrograma;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class TipoProgramaService : ITipoProgramaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public TipoProgramaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<TipoProgramaDto>> GetAllAsync()
        {
            List<TipoPrograma> tiposProgramas = await _repositoryManager.TipoProgramaRepository.GetAllAsync();
            return tiposProgramas.Adapt<List<TipoProgramaDto>>();
        }

        public async Task<TipoProgramaDto> GetByIdAsync(int id)
        {
            TipoPrograma tipoPrograma = await _repositoryManager.TipoProgramaRepository.GetByIdAsync(id);
            if (tipoPrograma is null)
                throw new TipoProgramaNotFoundException(id);
            return tipoPrograma.Adapt<TipoProgramaDto>();
        }

        public async Task<TipoProgramaDto> CreateAsync(TipoProgramaCUDto tipoProgramaCUDto)
        {
            int existeNombre = await _repositoryManager.TipoProgramaRepository.GetByNombreIsUnique(tipoProgramaCUDto.Nombre);
            if (existeNombre == 1)
                throw new TipoProgramaNombreIsNotUniqueBadRequestException(tipoProgramaCUDto.Nombre);
            tipoProgramaCUDto.Nombre = tipoProgramaCUDto.Nombre.Trim();
            TipoPrograma tipoPrograma = tipoProgramaCUDto.Adapt<TipoPrograma>();
            _repositoryManager.TipoProgramaRepository.Insert(tipoPrograma);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoPrograma.Adapt<TipoProgramaDto>();
        }

        public async Task<TipoProgramaDto> UpdateAsync(int id, TipoProgramaCUDto tipoProgramaCUDto)
        {
            TipoPrograma tipoPrograma = await _repositoryManager.TipoProgramaRepository.GetByIdAsync(id);
            if (tipoPrograma is null)
                throw new TipoProgramaNotFoundException(id);
            tipoPrograma.Nombre = tipoProgramaCUDto.Nombre.Trim();
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoPrograma.Adapt<TipoProgramaDto>();
        }

        public async Task<bool> StateChange(int id)
        {
            TipoPrograma tipoPrograma = await _repositoryManager.TipoProgramaRepository.GetByIdNotWhereAsync(id);
            if (tipoPrograma is null)
                throw new TipoProgramaNotFoundException(id);
            if (tipoPrograma.Estado)
                tipoPrograma.Estado = false;
            else
                tipoPrograma.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
