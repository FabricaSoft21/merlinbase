﻿using Contracts.DataTransferObjects.ApiComite.QuejaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Queja;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class QuejaService : IQuejaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public QuejaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<QuejaDto>> GetAllAsync()
        {
            List<Queja> quejas = await _repositoryManager.QuejaRepository.GetAllAsync();
            return quejas.Adapt<List<QuejaDto>>();
        }

        public async Task<QuejaDtoIncludes> GetByIdAsync(int id)
        {
            Queja queja = await _repositoryManager.QuejaRepository.GetByIdAsync(id);
            if (queja is null)
                throw new QuejaNotFoundException(id);
            QuejaDtoIncludes quejaDtoIncludes = new()
            {
                IdQueja = queja.IdQueja,
                Descripcion = queja.Descripcion,
                Fecha = queja.Fecha,
                TipoQuejaId = queja.TipoQuejaId,
                NombreTipoQueja = queja.TipoQueja.Nombre,
                EstadoQuejaId = queja.EstadoQuejaId,
                NombreEstadoQueja = queja.EstadoQueja.Nombre,
                InstructorId = queja.InstructorId,
                NombreInstructor = $"{queja.Instructor.Nombre} {queja.Instructor.Apellido}",
                AprendizId = queja.AprendizId,
                NombreAprendiz = $"{queja.Aprendiz.Nombre} {queja.Aprendiz.Apellido}",
                CoordinacionAcademicaId = queja.CoordinacionAcademicaId,
                NombreCoordinacionAcademica = $"{queja.CoordinacionAcademica.Nombre} {queja.CoordinacionAcademica.Apellido}"
            };
            return quejaDtoIncludes;
        }

        public async Task<QuejaDto> CreateAsync(QuejaCUDto quejaCUDto)
        {
            quejaCUDto.Descripcion = quejaCUDto.Descripcion.Trim();
            Queja queja = quejaCUDto.Adapt<Queja>();
            _repositoryManager.QuejaRepository.Insert(queja);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return queja.Adapt<QuejaDto>();
        }

        public async Task<bool> UpdateAsync(int id, QuejaCUDto quejaCUDto)
        {
            Queja queja = await _repositoryManager.QuejaRepository.GetByIdAsync(id);
            if (queja is null)
                throw new QuejaNotFoundException(id);
            queja.Descripcion = quejaCUDto.Descripcion.Trim();
            queja.TipoQuejaId = quejaCUDto.TipoQuejaId.Value;
            queja.EstadoQuejaId = quejaCUDto.EstadoQuejaId.Value;
            queja.InstructorId = quejaCUDto.InstructorId.Value;
            queja.AprendizId = quejaCUDto.AprendizId.Value;
            queja.CoordinacionAcademicaId = quejaCUDto.CoordinacionAcademicaId.Value;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }

        public async Task<bool> StateChange(int id)
        {
            Queja queja = await _repositoryManager.QuejaRepository.GetByIdNotWhereAsync(id);
            if (queja is null)
                throw new QuejaNotFoundException(id);
            if (queja.Estado)
                queja.Estado = false;
            else
                queja.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
