﻿using Contracts.DataTransferObjects.ApiComite.FichaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Ficha;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class FichaService : IFichaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public FichaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<FichaDtoInclude>> GetAllAsync()
        {
            List<Ficha> fichas = await _repositoryManager.FichaRepository.GetAllAsync();
            List<FichaDtoInclude> fichaDtoIncludes = new();
            fichas.ForEach(x =>
            {
                fichaDtoIncludes.Add(new FichaDtoInclude()
                {
                    IdFicha = x.IdFicha,
                    Codigo = x.Codigo,
                    ProgramaId = x.ProgramaId,
                    NombrePrograma = x.Programa.Nombre
                });
            });
            return fichaDtoIncludes;
        }

        public async Task<FichaDtoInclude> GetByIdAsync(int id)
        {
            Ficha ficha = await _repositoryManager.FichaRepository.GetByIdAsync(id);
            if (ficha is null)
                throw new FichaNotFoundException(id);
            FichaDtoInclude fichaDtoInclude = new()
            {
                IdFicha = ficha.IdFicha,
                Codigo = ficha.Codigo,
                ProgramaId = ficha.ProgramaId,
                NombrePrograma = ficha.Programa.Nombre
            };
            return fichaDtoInclude;
        }

        public async Task<FichaDto> CreateAsync(FichaCUDto fichaCUDto)
        {
            int existeCodigo = await _repositoryManager.FichaRepository.GetByCodigoIsUnique(fichaCUDto.Codigo);
            if (existeCodigo == 1)
                throw new FichaCodigoNotUniqueBadRequestException(fichaCUDto.Codigo);
            /*======== Validacion de que programa existe ========*/
            fichaCUDto.Codigo = fichaCUDto.Codigo.Trim();
            Ficha ficha = fichaCUDto.Adapt<Ficha>();
            _repositoryManager.FichaRepository.Insert(ficha);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return ficha.Adapt<FichaDto>();
        }

        public async Task<FichaDtoInclude> UpdateAsync(int id, FichaCUDto fichaCUDto)
        {
            Ficha ficha = await _repositoryManager.FichaRepository.GetByIdAsync(id);
            if (ficha is null)
                throw new FichaNotFoundException(id);
            /*======== Validacion de que programa existe ========*/
            ficha.Codigo = fichaCUDto.Codigo.Trim();
            ficha.ProgramaId = fichaCUDto.ProgramaId.Value;
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            FichaDtoInclude fichaDtoInclude = new FichaDtoInclude()
            {
                IdFicha = id,
                Codigo = fichaCUDto.Codigo,
                ProgramaId = fichaCUDto.ProgramaId.Value,
                NombrePrograma = ficha.Programa.Nombre
            };
            return fichaDtoInclude;
        }

        public async Task<bool> StateChange(int id)
        {
            Ficha ficha = await _repositoryManager.FichaRepository.GetByIdNotWhereAsync(id);
            if (ficha is null)
                throw new FichaNotFoundException(id);
            if (ficha.Estado)
                ficha.Estado = false;
            else
                ficha.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
