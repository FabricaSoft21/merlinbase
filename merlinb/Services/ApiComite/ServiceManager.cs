﻿using Domain.IRepositories.ApiComite;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly Lazy<ITipoDocumentoService> _lazyTipoDocumentoService;
        private readonly Lazy<ITipoProgramaService> _lazyTipoProgramaService;
        private readonly Lazy<IModalidadService> _lazyModalidadService;
        private readonly Lazy<ITipoQuejaService> _lazyTipoQuejaService;
        private readonly Lazy<IEstadoQuejaService> _lazyEstadoQuejaService;
        private readonly Lazy<ICoordinacionAcademicaService> _lazyCoordinacionAcademicaService;
        private readonly Lazy<IFichaService> _lazyFichaService;
        private readonly Lazy<IInstructorService> _lazyInstructorService;
        private readonly Lazy<IAprendizService> _lazyAprendizService;
        private readonly Lazy<IQuejaService> _lazyQuejaService;
        private readonly Lazy<IResultadoAprendizajeService> _lazyResultadoAprendizajeService;
        private readonly Lazy<ICompetenciaService> _lazyCompetenciaService;
        private readonly Lazy<IProgramaService> _lazyProgramaService;
        private readonly Lazy<IComiteService> _lazyComiteService;
        public ServiceManager(IRepositoryManager repositoryManager)
        {
            this._lazyTipoDocumentoService = new Lazy<ITipoDocumentoService>(() => new TipoDocumentoService(repositoryManager));
            this._lazyTipoProgramaService = new Lazy<ITipoProgramaService>(() => new TipoProgramaService(repositoryManager));
            this._lazyModalidadService = new Lazy<IModalidadService>(() => new ModalidadService(repositoryManager));
            this._lazyTipoQuejaService = new Lazy<ITipoQuejaService>(() => new TipoQuejaService(repositoryManager));
            this._lazyEstadoQuejaService = new Lazy<IEstadoQuejaService>(() => new EstadoQuejaService(repositoryManager));
            this._lazyCoordinacionAcademicaService = new Lazy<ICoordinacionAcademicaService>(() => new CoordinacionAcademicaService(repositoryManager));
            this._lazyFichaService = new Lazy<IFichaService>(() => new FichaService(repositoryManager));
            this._lazyInstructorService = new Lazy<IInstructorService>(() => new InstructorService(repositoryManager));
            this._lazyAprendizService = new Lazy<IAprendizService>(() => new AprendizService(repositoryManager));
            this._lazyQuejaService = new Lazy<IQuejaService>(() => new QuejaService(repositoryManager));
            this._lazyResultadoAprendizajeService = new Lazy<IResultadoAprendizajeService>(() => new ResultadoAprendizajeService(repositoryManager));
            this._lazyCompetenciaService = new Lazy<ICompetenciaService>(() => new CompetenciaService(repositoryManager));
            this._lazyProgramaService = new Lazy<IProgramaService>(() => new ProgramaService(repositoryManager));
            this._lazyComiteService = new Lazy<IComiteService>(() => new ComiteService(repositoryManager));
        }

        public ITipoDocumentoService TipoDocumentoService => _lazyTipoDocumentoService.Value;
        public ITipoProgramaService TipoProgramaService => _lazyTipoProgramaService.Value;
        public IModalidadService ModalidadService => _lazyModalidadService.Value;
        public ITipoQuejaService TipoQuejaService => _lazyTipoQuejaService.Value;
        public IEstadoQuejaService EstadoQuejaService => _lazyEstadoQuejaService.Value;
        public ICoordinacionAcademicaService CoordinacionAcademicaService => _lazyCoordinacionAcademicaService.Value;
        public IFichaService FichaService => _lazyFichaService.Value;
        public IInstructorService InstructorService => _lazyInstructorService.Value;
        public IAprendizService AprendizService => _lazyAprendizService.Value;
        public IQuejaService QuejaService => _lazyQuejaService.Value;
        public IResultadoAprendizajeService ResultadoAprendizajeService => _lazyResultadoAprendizajeService.Value;
        public ICompetenciaService CompetenciaService => _lazyCompetenciaService.Value;
        public IProgramaService ProgramaService => _lazyProgramaService.Value;
        public IComiteService ComiteService => _lazyComiteService.Value;
    }
}
