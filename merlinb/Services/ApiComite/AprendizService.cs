﻿using Contracts.DataTransferObjects.ApiComite.AprendizDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Aprendiz;
using Domain.Exceptions.ApiComite.Ficha;
using Domain.Exceptions.ApiComite.TipoDocumento;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class AprendizService : IAprendizService
    {
        private readonly IRepositoryManager _repositoryManager;
        public AprendizService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<AprendizDtoIncludes>> GetAllAsync()
        {
            List<Aprendiz> aprendices = await _repositoryManager.AprendizRepository.GetAllAsync();
            List<AprendizDtoIncludes> aprendizDtoIncludes = new List<AprendizDtoIncludes>();
            aprendices.ForEach(x =>
            {
                aprendizDtoIncludes.Add(new AprendizDtoIncludes
                {
                    IdAprendiz = x.IdAprendiz,
                    NumDocumento = x.NumDocumento,
                    Nombre = x.Nombre,
                    Apellido = x.Apellido,
                    Email = x.Email,
                    Telefono = x.Telefono,
                    TipoDocumentoId = x.TipoDocumentoId,
                    FichaId = x.FichaId,
                    NombreTipoDocumento = x.TipoDocumento.Nombre,
                    CodigoFicha = x.Ficha.Codigo
                });
            });
            return aprendizDtoIncludes;
        }

        public async Task<AprendizDtoIncludes> GetByIdAsync(int id)
        {
            Aprendiz aprendiz = await _repositoryManager.AprendizRepository.GetByIdAsync(id);
            if (aprendiz is null)
                throw new AprendizNotFoundException(id);
            AprendizDtoIncludes aprendizDtoIncludes = new()
            {
                IdAprendiz = aprendiz.IdAprendiz,
                NumDocumento = aprendiz.NumDocumento,
                Nombre = aprendiz.Nombre,
                Apellido = aprendiz.Apellido,
                Email = aprendiz.Email,
                Telefono = aprendiz.Telefono,
                TipoDocumentoId = aprendiz.TipoDocumentoId,
                NombreTipoDocumento = aprendiz.TipoDocumento.Nombre,
                FichaId = aprendiz.FichaId,
                CodigoFicha = aprendiz.Ficha.Codigo,
            };
            return aprendizDtoIncludes;
        }

        public async Task<AprendizDto> CreateAsync(AprendizCUDto aprendizCUDto)
        {
            int existeNumDocumentoEmail = await _repositoryManager.AprendizRepository.GetByNumDocumentoEmailIsUnique(aprendizCUDto.NumDocumento, aprendizCUDto.Email);
            if (existeNumDocumentoEmail == 1)
                throw new AprendizNumDocumentoIsNotUniqueBadRequestException(aprendizCUDto.NumDocumento);
            if (existeNumDocumentoEmail == 2)
                throw new AprendizEmailIsNotUniqueBadRequestException(aprendizCUDto.Email);
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(aprendizCUDto.TipoDocumentoId.Value);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(aprendizCUDto.TipoDocumentoId.Value);
            Ficha ficha = await _repositoryManager.FichaRepository.GetByIdAsync(aprendizCUDto.FichaId.Value);
            if (ficha is null)
                throw new FichaNotFoundException(aprendizCUDto.FichaId.Value);
            aprendizCUDto.NumDocumento = aprendizCUDto.NumDocumento.Trim();
            aprendizCUDto.Nombre = aprendizCUDto.Nombre.Trim();
            aprendizCUDto.Apellido = aprendizCUDto.Apellido.Trim();
            aprendizCUDto.Email = aprendizCUDto.Email.Trim().ToLower();
            aprendizCUDto.Telefono = aprendizCUDto.Telefono.Trim();
            Aprendiz aprendiz = aprendizCUDto.Adapt<Aprendiz>();
            _repositoryManager.AprendizRepository.Insert(aprendiz);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return aprendiz.Adapt<AprendizDto>();
        }

        public async Task<AprendizDtoIncludes> UpdateAsync(int id, AprendizCUDto aprendizCUDto)
        {
            Aprendiz aprendiz = await _repositoryManager.AprendizRepository.GetByIdAsync(id);
            if (aprendiz is null)
                throw new AprendizNotFoundException(id);
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(aprendizCUDto.TipoDocumentoId.Value);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(aprendizCUDto.TipoDocumentoId.Value);
            Ficha ficha = await _repositoryManager.FichaRepository.GetByIdAsync(aprendizCUDto.FichaId.Value);
            if (ficha is null)
                throw new FichaNotFoundException(aprendizCUDto.FichaId.Value);
            aprendiz.NumDocumento = aprendizCUDto.NumDocumento.Trim();
            aprendiz.Nombre = aprendizCUDto.Nombre.Trim();
            aprendiz.Apellido = aprendizCUDto.Apellido.Trim();
            aprendiz.Email = aprendizCUDto.Email.Trim().ToLower();
            aprendiz.Telefono = aprendizCUDto.Telefono.Trim();
            aprendiz.TipoDocumentoId = aprendizCUDto.TipoDocumentoId.Value;
            aprendiz.FichaId = aprendizCUDto.FichaId.Value;
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            AprendizDtoIncludes aprendizDtoIncludes = new AprendizDtoIncludes()
            {
                IdAprendiz = id,
                NumDocumento = aprendizCUDto.NumDocumento,
                Nombre = aprendizCUDto.Nombre,
                Apellido = aprendizCUDto.Apellido,
                Email = aprendizCUDto.Email,
                Telefono = aprendizCUDto.Telefono,
                TipoDocumentoId = aprendizCUDto.TipoDocumentoId.Value,
                NombreTipoDocumento = aprendiz.TipoDocumento.Nombre,
                FichaId = aprendizCUDto.FichaId.Value,
                CodigoFicha = aprendiz.Ficha.Codigo
            };
            return aprendizDtoIncludes;
        }

        public async Task<bool> StateChange(int id)
        {
            Aprendiz aprendiz = await _repositoryManager.AprendizRepository.GetByIdNotWhereAsync(id);
            if (aprendiz is null)
                throw new AprendizNotFoundException(id);
            if (aprendiz.Estado)
                aprendiz.Estado = false;
            else
                aprendiz.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
