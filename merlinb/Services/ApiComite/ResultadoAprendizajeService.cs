﻿using Contracts.DataTransferObjects.ApiComite.ResultadoAprendizajeDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.ResultadoAprendizaje;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class ResultadoAprendizajeService : IResultadoAprendizajeService
    {
        private readonly IRepositoryManager _repositoryManager;
        public ResultadoAprendizajeService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<ResultadoAprendizajeDto>> GetAllAsync()
        {
            List<ResultadoAprendizaje> resultadosAprendizajes = await _repositoryManager.ResultadoAprendizajeRepository.GetAllAsync();
            return resultadosAprendizajes.Adapt<List<ResultadoAprendizajeDto>>();
        }

        public async Task<ResultadoAprendizajeDtoIncludes> GetByIdAsync(int id)
        {
            ResultadoAprendizaje resultadoAprendizaje = await _repositoryManager.ResultadoAprendizajeRepository.GetByIdAsync(id);
            if (resultadoAprendizaje is null)
                throw new ResultadoAprendizajeNotFoundException(id);
            ResultadoAprendizajeDtoIncludes resultadoAprendizajeDtoIncludes = new()
            {
                IdResultadoAprendizaje = resultadoAprendizaje.IdResultadoAprendizaje,
                Codigo = resultadoAprendizaje.Codigo,
                Nombre = resultadoAprendizaje.Nombre,
                CompetenciaId = resultadoAprendizaje.CompetenciaId,
                NombreCompetencia = resultadoAprendizaje.Competencia.Nombre
            };
            return resultadoAprendizajeDtoIncludes;
        }

        public async Task<ResultadoAprendizajeDto> CreateAsync(ResultadoAprendizajeCUDto resultadoAprendizajeCUDto)
        {
            int existeCodigoNombre = await _repositoryManager.ResultadoAprendizajeRepository.GetByCodigoNombreIsUnique(resultadoAprendizajeCUDto.Codigo, resultadoAprendizajeCUDto.Nombre);
            if (existeCodigoNombre == 1)
                throw new ResultadoAprendizajeCodigoIsNotUniqueBadRequestException(resultadoAprendizajeCUDto.Codigo);
            if (existeCodigoNombre == 2)
                throw new ResultadoAprendizajeNombreIsNotUniqueBadRequestException(resultadoAprendizajeCUDto.Nombre);
            /*========Validacion del programa=======*/
            resultadoAprendizajeCUDto.Codigo = resultadoAprendizajeCUDto.Codigo.Trim();
            resultadoAprendizajeCUDto.Nombre = resultadoAprendizajeCUDto.Nombre.Trim();
            ResultadoAprendizaje resultadoAprendizaje = resultadoAprendizajeCUDto.Adapt<ResultadoAprendizaje>();
            _repositoryManager.ResultadoAprendizajeRepository.Insert(resultadoAprendizaje);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return resultadoAprendizaje.Adapt<ResultadoAprendizajeDto>();
        }

        public async Task<bool> UpdateAsync(int id, ResultadoAprendizajeCUDto resultadoAprendizajeCUDto)
        {
            ResultadoAprendizaje resultadoAprendizaje = await _repositoryManager.ResultadoAprendizajeRepository.GetByIdAsync(id);
            if (resultadoAprendizaje is null)
                throw new ResultadoAprendizajeNotFoundException(id);
            /*========Validacion del programa=======*/
            resultadoAprendizaje.Codigo = resultadoAprendizajeCUDto.Codigo.Trim();
            resultadoAprendizaje.Nombre = resultadoAprendizajeCUDto.Nombre.Trim();
            resultadoAprendizaje.CompetenciaId = resultadoAprendizajeCUDto.CompetenciaId.Value;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }

        public async Task<bool> StateChange(int id)
        {
            ResultadoAprendizaje resultadoAprendizaje = await _repositoryManager.ResultadoAprendizajeRepository.GetByIdNotWhereAsync(id);
            if (resultadoAprendizaje is null)
                throw new ResultadoAprendizajeNotFoundException(id);
            if (resultadoAprendizaje.Estado)
                resultadoAprendizaje.Estado = false;
            else
                resultadoAprendizaje.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
