﻿using Contracts.DataTransferObjects.ApiComite.InstructorDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Instructor;
using Domain.Exceptions.ApiComite.TipoDocumento;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class InstructorService : IInstructorService
    {
        private readonly IRepositoryManager _repositoryManager;
        public InstructorService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<InstructorDto>> GetAllAsync()
        {
            List<Instructor> instructores = await _repositoryManager.InstructorRepository.GetAllAsync();
            return instructores.Adapt<List<InstructorDto>>();
        }

        public async Task<InstructorDtoIncludes> GetByIdAsync(int id)
        {
            Instructor instructor = await _repositoryManager.InstructorRepository.GetByIdAsync(id);
            if (instructor is null)
                throw new InstructorNotFoundException(id);
            InstructorDtoIncludes instructorDtoIncludes = new()
            {
                IdInstructor = instructor.IdInstructor,
                NumDocumento = instructor.NumDocumento,
                Nombre = instructor.Nombre,
                Apellido = instructor.Apellido,
                Email = instructor.Email,
                Telefono = instructor.Telefono,
                TipoDocumentoId = instructor.TipoDocumentoId,
                NombreTipoDocumento = instructor.TipoDocumento.Nombre
            };
            instructor.CoordinacionesAcademicasInstructores.ForEach(x =>
            {
                instructorDtoIncludes.NombresCoordinacionesAcademicas.Add(x.CoordinacionAcademica.Nombre + " " + x.CoordinacionAcademica.Apellido);
            });
            return instructorDtoIncludes;
        }

        public async Task<InstructorDto> CreateAsync(InstructorCUDto instructorCUDto)
        {
            int existeNumDocumentoEmail = await _repositoryManager.InstructorRepository.GetByNumDocumentoEmailIsUnique(instructorCUDto.NumDocumento, instructorCUDto.Email);
            if (existeNumDocumentoEmail == 1)
                throw new InstructorNumDocumentoIsNotUniqueBadRequestException(instructorCUDto.NumDocumento);
            if (existeNumDocumentoEmail == 2)
                throw new InstructorEmailIsNotUniqueBadRequestException(instructorCUDto.Email);
            Instructor instructor = new()
            {
                NumDocumento = instructorCUDto.NumDocumento.Trim(),
                Nombre = instructorCUDto.Nombre.Trim(),
                Apellido = instructorCUDto.Apellido.Trim(),
                Email = instructorCUDto.Email.Trim().ToLower(),
                Telefono = instructorCUDto.Telefono.Trim(),
                TipoDocumentoId = instructorCUDto.TipoDocumentoId.Value,
            };
            Instructor instructorCreate = await _repositoryManager.InstructorRepository.Insert(instructor);
            instructorCUDto.CoordinacionesAcademicasIds.ForEach(x =>
            {
                CoordinacionAcademicaInstructor coordinacionAcademicaInstructor = new()
                {
                    CoordinacionAcademicaId = x,
                    InstructorId = instructorCreate.IdInstructor
                };
                _repositoryManager.CoordinacionAcademicaInstructorRepository.Insert(coordinacionAcademicaInstructor);
            });
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return instructor.Adapt<InstructorDto>();
        }

        public async Task<bool> UpdateAsync(int id, InstructorCUDto instructorCUDto)
        {
            Instructor instructor = await _repositoryManager.InstructorRepository.GetByIdAsync(id);
            if (instructor is null)
                throw new InstructorNotFoundException(id);
            TipoDocumento tipoDocumento = await _repositoryManager.TipoDocumentoRepository.GetByIdAsync(instructorCUDto.TipoDocumentoId.Value);
            if (tipoDocumento is null)
                throw new TipoDocumentoNotFoundException(instructorCUDto.TipoDocumentoId.Value);
            instructor.NumDocumento = instructorCUDto.NumDocumento;
            instructor.Nombre = instructorCUDto.Nombre.Trim();
            instructor.Apellido = instructorCUDto.Apellido.Trim();
            instructor.Email = instructorCUDto.Email.Trim().ToLower();
            instructor.Telefono = instructorCUDto.Telefono.Trim();
            instructor.TipoDocumentoId = instructorCUDto.TipoDocumentoId.Value;
            List<CoordinacionAcademicaInstructor> coordinacionesAcademicasInstructores = await _repositoryManager.CoordinacionAcademicaInstructorRepository.GetByIdsAsync(id);
            coordinacionesAcademicasInstructores.ForEach(x =>
            {
                _repositoryManager.CoordinacionAcademicaInstructorRepository.Remove(x);
            });
            instructorCUDto.CoordinacionesAcademicasIds.ForEach(x =>
            {
                CoordinacionAcademicaInstructor coordinacionAcademicaInstructor = new()
                {
                    InstructorId = instructor.IdInstructor,
                    CoordinacionAcademicaId = x
                };
                _repositoryManager.CoordinacionAcademicaInstructorRepository.Insert(coordinacionAcademicaInstructor);
            });
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }

        public async Task<bool> StateChange(int id)
        {
            Instructor instructor = await _repositoryManager.InstructorRepository.GetByIdNotWhereAsync(id);
            if (instructor is null)
                throw new InstructorNotFoundException(id);
            if (instructor.Estado)
                instructor.Estado = false;
            else
                instructor.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
