﻿using Contracts.DataTransferObjects.ApiComite.EstadoQuejaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.EstadoQueja;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class EstadoQuejaService : IEstadoQuejaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public EstadoQuejaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<EstadoQuejaDto>> GetAllAsync()
        {
            List<EstadoQueja> estadosQuejas = await _repositoryManager.EstadoQuejaRepository.GetAllAsync();
            return estadosQuejas.Adapt<List<EstadoQuejaDto>>();
        }

        public async Task<EstadoQuejaDto> GetByIdAsync(int id)
        {
            EstadoQueja estadoQueja = await _repositoryManager.EstadoQuejaRepository.GetByIdAsync(id);
            if (estadoQueja is null)
                throw new EstadoQuejaNotFoundException(id);
            return estadoQueja.Adapt<EstadoQuejaDto>();
        }

        public async Task<EstadoQuejaDto> CreateAsync(EstadoQuejaCUDto estadoQuejaCUDto)
        {
            int existeNombre = await _repositoryManager.EstadoQuejaRepository.GetByNombreIsUnique(estadoQuejaCUDto.Nombre);
            if (existeNombre == 1)
                throw new EstadoQuejaNombreIsNotUniqueBadRequestException(estadoQuejaCUDto.Nombre);
            estadoQuejaCUDto.Nombre = estadoQuejaCUDto.Nombre.Trim();
            EstadoQueja estadoQueja = estadoQuejaCUDto.Adapt<EstadoQueja>();
            _repositoryManager.EstadoQuejaRepository.Insert(estadoQueja);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return estadoQueja.Adapt<EstadoQuejaDto>();
        }

        public async Task<EstadoQuejaDto> UpdateAsync(int id, EstadoQuejaCUDto estadoQuejaCUDto)
        {
            EstadoQueja estadoQueja = await _repositoryManager.EstadoQuejaRepository.GetByIdAsync(id);
            if (estadoQueja is null)
                throw new EstadoQuejaNotFoundException(id);
            estadoQueja.Nombre = estadoQuejaCUDto.Nombre.Trim();
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return estadoQueja.Adapt<EstadoQuejaDto>();
        }

        public async Task<bool> StateChange(int id)
        {
            EstadoQueja estadoQueja = await _repositoryManager.EstadoQuejaRepository.GetByIdNotWhereAsync(id);
            if (estadoQueja is null)
                throw new EstadoQuejaNotFoundException(id);
            if (estadoQueja.Estado)
                estadoQueja.Estado = false;
            else
                estadoQueja.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
