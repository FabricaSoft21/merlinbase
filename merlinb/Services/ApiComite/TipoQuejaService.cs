﻿using Contracts.DataTransferObjects.ApiComite.TipoQuejaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.TipoQueja;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class TipoQuejaService : ITipoQuejaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public TipoQuejaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<TipoQuejaDto>> GetAllAsync()
        {
            List<TipoQueja> tiposQuejas = await _repositoryManager.TipoQuejaRepository.GetAllAsync();
            return tiposQuejas.Adapt<List<TipoQuejaDto>>();
        }

        public async Task<TipoQuejaDto> GetByIdAsync(int id)
        {
            TipoQueja tipoQueja = await _repositoryManager.TipoQuejaRepository.GetByIdAsync(id);
            if (tipoQueja is null)
                throw new TipoQuejaNotFoundException(id);
            return tipoQueja.Adapt<TipoQuejaDto>();
        }

        public async Task<TipoQuejaDto> CreateAsync(TipoQuejaCUDto tipoQuejaCUDto)
        {
            int existeNombre = await _repositoryManager.TipoQuejaRepository.GetByNombreIsUnique(tipoQuejaCUDto.Nombre);
            if (existeNombre == 1)
                throw new TipoQuejaNombreIsNotUniqueBadRequestException(tipoQuejaCUDto.Nombre);
            tipoQuejaCUDto.Nombre = tipoQuejaCUDto.Nombre.Trim();
            TipoQueja tipoQueja = tipoQuejaCUDto.Adapt<TipoQueja>();
            _repositoryManager.TipoQuejaRepository.Insert(tipoQueja);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoQueja.Adapt<TipoQuejaDto>();
        }

        public async Task<TipoQuejaDto> UpdateAsync(int id, TipoQuejaCUDto tipoQuejaCUDto)
        {
            TipoQueja tipoQueja = await _repositoryManager.TipoQuejaRepository.GetByIdAsync(id);
            if (tipoQueja is null)
                throw new TipoQuejaNotFoundException(id);
            tipoQueja.Nombre = tipoQuejaCUDto.Nombre.Trim();
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return tipoQueja.Adapt<TipoQuejaDto>();
        }

        public async Task<bool> StateChange(int id)
        {
            TipoQueja tipoQueja = await _repositoryManager.TipoQuejaRepository.GetByIdNotWhereAsync(id);
            if (tipoQueja is null)
                throw new TipoQuejaNotFoundException(id);
            if (tipoQueja.Estado)
                tipoQueja.Estado = false;
            else
                tipoQueja.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
