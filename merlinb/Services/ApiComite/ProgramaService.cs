﻿using Contracts.DataTransferObjects.ApiComite.ProgramaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.CoordinacionAcademica;
using Domain.Exceptions.ApiComite.Modalidad;
using Domain.Exceptions.ApiComite.Programa;
using Domain.Exceptions.ApiComite.TipoPrograma;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class ProgramaService : IProgramaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public ProgramaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<ProgramaDtoInclude>> GetAllAsync()
        {
            List<Programa> programas = await _repositoryManager.ProgramaRepository.GetAllAsync();
            List<ProgramaDtoInclude> programaDtoIncludes = new List<ProgramaDtoInclude>();
            programas.ForEach(x =>
            {
                programaDtoIncludes.Add(new ProgramaDtoInclude
                {
                    IdPrograma = x.IdPrograma,
                    Codigo = x.Codigo,
                    Nombre = x.Nombre,
                    TipoProgramaId = x.TipoProgramaId,
                    NombreTipoPrograma = x.TipoPrograma.Nombre,
                    ModalidadId = x.ModalidadId,
                    NombreModalidad = x.Modalidad.Nombre,
                    CoordinacionAcademicaId = x.CoordinacionAcademicaId,
                    NombreCoordinacionAcademica = x.CoordinacionAcademica.Nombre
                });
            });
            return programaDtoIncludes;
        }

        public async Task<ProgramaDtoInclude> GetByIdAsync(int id)
        {
            Programa programa = await _repositoryManager.ProgramaRepository.GetByIdAsync(id);
            if (programa is null)
                throw new ProgramaNotFoundException(id);
            ProgramaDtoInclude programaDtoInclude = new()
            {
                IdPrograma = programa.IdPrograma,
                Codigo = programa.Codigo,
                Nombre = programa.Nombre,
                TipoProgramaId = programa.TipoProgramaId,
                NombreTipoPrograma = programa.TipoPrograma.Nombre,
                ModalidadId = programa.ModalidadId,
                NombreModalidad = programa.Modalidad.Nombre,
                CoordinacionAcademicaId = programa.CoordinacionAcademicaId,
                NombreCoordinacionAcademica = programa.CoordinacionAcademica.Nombre
            };
            return programaDtoInclude;
        }

        public async Task<ProgramaDto> CreateAsync(ProgramaCUDto programaCUDto)
        {
            int existeCodigoModalidad = await _repositoryManager.ProgramaRepository.GetByCodigoModalidadIsUnique(programaCUDto.Codigo, programaCUDto.ModalidadId.Value, programaCUDto.TipoProgramaId.Value);
            if (existeCodigoModalidad == 1)
                throw new ProgramaCodigoAndNombreBadRequestException(programaCUDto.Codigo, programaCUDto.ModalidadId.Value, programaCUDto.TipoProgramaId.Value);
            TipoPrograma tipoPrograma = await _repositoryManager.TipoProgramaRepository.GetByIdAsync(programaCUDto.TipoProgramaId.Value);
            if (tipoPrograma is null)
                throw new TipoProgramaNotFoundException(programaCUDto.TipoProgramaId.Value);
            Modalidad modalidad = await _repositoryManager.ModalidadRepository.GetByIdAsync(programaCUDto.ModalidadId.Value);
            if (modalidad is null)
                throw new ModalidadNotFoundException(programaCUDto.ModalidadId.Value);
            CoordinacionAcademica coordinacionAcademica = await _repositoryManager.CoordinacionAcademicaRepository.GetByIdAsync(programaCUDto.CoordinacionAcademicaId.Value);
            if (coordinacionAcademica is null)
                throw new CoordinacionAcademicaNotFoundException(programaCUDto.CoordinacionAcademicaId.Value);
            Programa programa = new()
            {
                Codigo = programaCUDto.Codigo.Trim(),
                Nombre = programaCUDto.Nombre.Trim(),
                TipoProgramaId = programaCUDto.TipoProgramaId.Value,
                ModalidadId = programaCUDto.ModalidadId.Value,
                CoordinacionAcademicaId = programaCUDto.CoordinacionAcademicaId.Value,
            };
            await _repositoryManager.ProgramaRepository.Insert(programa);
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return programa.Adapt<ProgramaDto>();
        }

        public async Task<ProgramaDtoInclude> UpdateAsync(int id, ProgramaCUDto programaCUDto)
        {
            Programa programa = await _repositoryManager.ProgramaRepository.GetByIdAsync(id);
            if (programa is null)
                throw new ProgramaNotFoundException(id);
            TipoPrograma tipoPrograma = await _repositoryManager.TipoProgramaRepository.GetByIdAsync(programaCUDto.TipoProgramaId.Value);
            if (tipoPrograma is null)
                throw new TipoProgramaNotFoundException(programaCUDto.TipoProgramaId.Value);
            Modalidad modalidad = await _repositoryManager.ModalidadRepository.GetByIdAsync(programaCUDto.ModalidadId.Value);
            if (modalidad is null)
                throw new ModalidadNotFoundException(programaCUDto.ModalidadId.Value);
            CoordinacionAcademica coordinacionAcademica = await _repositoryManager.CoordinacionAcademicaRepository.GetByIdAsync(programaCUDto.CoordinacionAcademicaId.Value);
            if (coordinacionAcademica is null)
                throw new CoordinacionAcademicaNotFoundException(programaCUDto.CoordinacionAcademicaId.Value);
            programa.Codigo = programaCUDto.Codigo.Trim();
            programa.Nombre = programaCUDto.Nombre.Trim();
            programa.TipoProgramaId = programaCUDto.TipoProgramaId.Value;
            programa.ModalidadId = programaCUDto.ModalidadId.Value;
            programa.CoordinacionAcademicaId = programaCUDto.CoordinacionAcademicaId.Value;
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            ProgramaDtoInclude programaDtoInclude = new ProgramaDtoInclude()
            {
                IdPrograma = id,
                Codigo = programaCUDto.Codigo,
                Nombre = programaCUDto.Nombre,
                ModalidadId = programaCUDto.ModalidadId.Value,
                NombreModalidad = programa.Modalidad.Nombre,
                TipoProgramaId = programaCUDto.TipoProgramaId.Value,
                NombreTipoPrograma = programa.TipoPrograma.Nombre,
                CoordinacionAcademicaId = programaCUDto.CoordinacionAcademicaId.Value,
                NombreCoordinacionAcademica = programa.CoordinacionAcademica.Nombre
            };
            return programaDtoInclude;
        }

        public async Task<bool> StateChange(int id)
        {
            Programa programa = await _repositoryManager.ProgramaRepository.GetByIdNotWhereAsync(id);
            if (programa is null)
                throw new ProgramaNotFoundException(id);
            if (programa.Estado)
                programa.Estado = false;
            else
                programa.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
