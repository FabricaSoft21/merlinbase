﻿using Contracts.DataTransferObjects.ApiComite.ComiteDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Comite;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class ComiteService : IComiteService
    {
        private readonly IRepositoryManager _repositoryManager;
        public ComiteService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<ComiteDto>> GetAllAsync()
        {
            List<Comite> comites = await _repositoryManager.ComiteRepository.GetAllAsync();
            return comites.Adapt<List<ComiteDto>>();
        }

        public async Task<ComiteDtoInclude> GetByIdAsync(int id)
        {
            Comite comite = await _repositoryManager.ComiteRepository.GetByIdAsync(id);
            if (comite is null)
                throw new ComiteNotFoundException(id);
            ComiteDtoInclude comiteDtoInclude = new()
            {
                IdComite = comite.IdComite,
                Fecha = comite.Fecha,
                Acta = comite.Acta,
                Resolucion = comite.Resolucion,
                CoordinacionAcademicaId = comite.CoordinacionAcademicaId,
                NombreCoordinacionAcademica = comite.CoordinacionAcademica.Nombre,
            };
            comite.DetalleComite.ForEach(x =>
            {
                comiteDtoInclude.DescripcionesQueja.Add(x.Queja.Descripcion);
            });
            return comiteDtoInclude;
        }

        public async Task<ComiteDto> CreateAsync(ComiteCUDto comiteCUDto)
        {
            Comite comite = new()
            {
                Fecha = comiteCUDto.Fecha,
                Acta = comiteCUDto.Acta,
                Resolucion = comiteCUDto.Resolucion,
                CoordinacionAcademicaId = comiteCUDto.CoordinacionAcademicaId.Value
            };
            Comite comiteCreate = await _repositoryManager.ComiteRepository.Insert(comite);
            comiteCUDto.QuejasIds.ForEach(x =>
            {
                DetalleComite detalleComite = new()
                {
                    ComiteId = comiteCreate.IdComite,
                    QuejaId = x
                };
                _repositoryManager.DetalleComiteRepository.Insert(detalleComite);
            });
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return comite.Adapt<ComiteDto>();
        }

        public async Task<bool> UpdateAsync(int id, ComiteCUDto comiteCUDto)
        {
            Comite comite = await _repositoryManager.ComiteRepository.GetByIdAsync(id);
            if (comite is null)
                throw new ComiteNotFoundException(id);
            comite.Fecha = comiteCUDto.Fecha;
            comite.Acta = comiteCUDto.Acta;
            comite.Resolucion = comiteCUDto.Resolucion;
            comite.CoordinacionAcademicaId = comiteCUDto.CoordinacionAcademicaId.Value;
            List<DetalleComite> detallesComites = await _repositoryManager.DetalleComiteRepository.GetByIdsAsync(id);
            detallesComites.ForEach(x =>
            {
                _repositoryManager.DetalleComiteRepository.Remove(x);
            });
            comiteCUDto.QuejasIds.ForEach(x =>
            {
                DetalleComite detalleComite = new()
                {
                    ComiteId = comite.IdComite,
                    QuejaId = x
                };
                _repositoryManager.DetalleComiteRepository.Insert(detalleComite);
            });
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }

        public async Task<bool> StateChange(int id)
        {
            Comite comite = await _repositoryManager.ComiteRepository.GetByIdNotWhereAsync(id);
            if (comite is null)
                throw new ComiteNotFoundException(id);
            if (comite.Estado)
                comite.Estado = false;
            else
                comite.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
