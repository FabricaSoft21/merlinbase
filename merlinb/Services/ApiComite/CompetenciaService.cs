﻿using Contracts.DataTransferObjects.ApiComite.CompetenciaDtos;
using Domain.Entities.ApiComite;
using Domain.Exceptions.ApiComite.Competencia;
using Domain.Exceptions.ApiComite.Programa;
using Domain.IRepositories.ApiComite;
using Mapster;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ApiComite
{
    internal sealed class CompetenciaService : ICompetenciaService
    {
        private readonly IRepositoryManager _repositoryManager;
        public CompetenciaService(IRepositoryManager repositoryManager) => this._repositoryManager = repositoryManager;

        public async Task<List<CompetenciaDtoInclude>> GetAllAsync()
        {
            List<Competencia> competencias = await _repositoryManager.CompetenciaRepository.GetAllAsync();
            List<CompetenciaDtoInclude> competenciaDtoIncludes = new List<CompetenciaDtoInclude>();
            competencias.ForEach(x =>
            {
                competenciaDtoIncludes.Add(new CompetenciaDtoInclude
                {
                    IdCompetencia = x.IdCompetencia,
                    Nombre = x.Nombre,
                });
            });
            return competenciaDtoIncludes;
        }

        public async Task<CompetenciaDtoInclude> GetByIdAsync(int id)
        {
            Competencia competencia = await _repositoryManager.CompetenciaRepository.GetByIdAsync(id);
            if (competencia is null)
                throw new CompetenciaNotFoundException(id);
            CompetenciaDtoInclude competenciaDtoInclude = new()
            {
                IdCompetencia = competencia.IdCompetencia,
                Nombre = competencia.Nombre,
            };
            competencia.CompetenciasProgramas.ForEach(x =>
            {
                competenciaDtoInclude.ProgramasIds.Add(x.ProgramaId);
                competenciaDtoInclude.NombresProgramas.Add(x.Programa.Nombre);
            });
            return competenciaDtoInclude;
        }

        public async Task<CompetenciaDto> CreateAsync(CompetenciaCUDto competenciaCUDto)
        {
            int existeNombreComptencia = await _repositoryManager.CompetenciaRepository.GetByNombreIsUnique(competenciaCUDto.Nombre);
            if (existeNombreComptencia == 1)
                throw new CompetenciaNombreIsNotUniqueBadRequestException(competenciaCUDto.Nombre);
            Competencia competencia = new()
            {
                Nombre = competenciaCUDto.Nombre.Trim()
            };
            Competencia competenciaCreate = await _repositoryManager.CompetenciaRepository.Insert(competencia);
            competenciaCUDto.ProgramasIds.ForEach(x =>
            {
                CompetenciaPrograma competenciaPrograma = new()
                {
                    CompetenciaId = competenciaCreate.IdCompetencia,
                    ProgramaId = x
                };
                _repositoryManager.CompetenciaProgramaRepository.Insert(competenciaPrograma);
            });
            await _repositoryManager.UnitOfWork.SaveChangesAsync();
            return competencia.Adapt<CompetenciaDto>();
        }

        public async Task<bool> UpdateAsync(int id, CompetenciaCUDto competenciaCUDto)
        {
            Competencia competencia = await _repositoryManager.CompetenciaRepository.GetByIdAsync(id);
            if (competencia is null)
                throw new CompetenciaNotFoundException(id);
            competencia.Nombre = competenciaCUDto.Nombre.Trim();
            List<CompetenciaPrograma> competenciasProgramas = await _repositoryManager.CompetenciaProgramaRepository.GetByIdsAsync(id);
            competenciasProgramas.ForEach(x =>
            {
                _repositoryManager.CompetenciaProgramaRepository.Remove(x);
            });
            competenciaCUDto.ProgramasIds.ForEach(x =>
            {
                CompetenciaPrograma competenciaPrograma = new()
                {
                    CompetenciaId = competencia.IdCompetencia,
                    ProgramaId = x
                };
                _repositoryManager.CompetenciaProgramaRepository.Insert(competenciaPrograma);
            });
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }

        public async Task<bool> StateChange(int id)
        {
            Competencia competencia = await _repositoryManager.CompetenciaRepository.GetByIdNotWhereAsync(id);
            if (competencia is null)
                throw new CompetenciaNotFoundException(id);
            if (competencia.Estado)
                competencia.Estado = false;
            else
                competencia.Estado = true;
            return await _repositoryManager.UnitOfWork.SaveChangesAsync();
        }
    }
}
