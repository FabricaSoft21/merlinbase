﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class CoordinacionAcademicaInstructor
    {
        public int CoordinacionAcademicaId { get; set; }
        public int InstructorId { get; set; }
        public CoordinacionAcademica CoordinacionAcademica { get; set; }
        public Instructor Instructor { get; set; }
    }
}
