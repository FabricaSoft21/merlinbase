﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Programa
    {
        public int IdPrograma { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public int TipoProgramaId { get; set; }
        public int ModalidadId { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public bool Estado { get; set; } = true;
        public TipoPrograma TipoPrograma { get; set; }
        public Modalidad Modalidad { get; set; }
        public CoordinacionAcademica CoordinacionAcademica { get; set; }
        public List<CompetenciaPrograma> CompetenciasProgramas { get; set; }
        public List<Ficha> Fichas { get; set; }
    }
}
