﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class TipoQueja
    {
        public int IdTipoQueja { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; } = true;
        public List<Queja> Quejas { get; set; }
    }
}
