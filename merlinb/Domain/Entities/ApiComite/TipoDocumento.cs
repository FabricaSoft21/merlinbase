﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class TipoDocumento
    {
        public int IdTipoDocumento { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; } = true;
        public List<CoordinacionAcademica> CoordinacionesAcademicas { get; set; }
        public List<Instructor> Instructores { get; set; }
        public List<Aprendiz> Aprendices { get; set; }
    }
}
