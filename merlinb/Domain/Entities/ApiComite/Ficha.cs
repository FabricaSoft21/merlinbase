﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Ficha
    {
        public int IdFicha { get; set; }
        public string Codigo { get; set; }
        public int ProgramaId { get; set; }
        public bool Estado { get; set; } = true;
        public Programa Programa { get; set; }
        public List<Aprendiz> Aprendices { get; set; }
    }
}
