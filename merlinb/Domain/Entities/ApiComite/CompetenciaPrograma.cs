﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class CompetenciaPrograma
    {
        public int CompetenciaId { get; set; }
        public int ProgramaId { get; set; }
        public Competencia Competencia { get; set; }
        public Programa Programa { get; set; }
    }
}
