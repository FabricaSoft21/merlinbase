﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class ResultadoAprendizaje
    {
        public int IdResultadoAprendizaje { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public int CompetenciaId { get; set; }
        public bool Estado { get; set; } = true;
        public Competencia Competencia { get; set; }
    }
}
