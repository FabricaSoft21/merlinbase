﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class DetalleComite
    {
        public int ComiteId { get; set; }
        public int QuejaId { get; set; }
        public Comite Comite { get; set; }
        public Queja Queja { get; set; }
    }
}
