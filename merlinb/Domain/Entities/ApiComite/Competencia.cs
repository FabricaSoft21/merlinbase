﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Competencia
    {
        public int IdCompetencia { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; } = true;
        public List<CompetenciaPrograma> CompetenciasProgramas { get; set; }
        public List<ResultadoAprendizaje> ResultadoAprendizajes { get; set; }
    }
}
