﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Queja
    {
        public int IdQueja { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Now;
        public int TipoQuejaId { get; set; }
        public int EstadoQuejaId { get; set; }
        public int InstructorId { get; set; }
        public int AprendizId { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public bool Estado { get; set; } = true;
        public TipoQueja TipoQueja { get; set; }
        public EstadoQueja EstadoQueja { get; set; }
        public Instructor Instructor { get; set; }
        public Aprendiz Aprendiz { get; set; }
        public CoordinacionAcademica CoordinacionAcademica { get; set; }
        public List<DetalleComite> DetalleComite { get; set; }
    }
}
