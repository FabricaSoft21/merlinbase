﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class CoordinacionAcademica
    {
        public int IdCoordinacionAcademica { get; set; }
        public string NumDocumento { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int TipoDocumentoId { get; set; }
        public bool Estado { get; set; } = true;
        public TipoDocumento TipoDocumento { get; set; }
        public List<CoordinacionAcademicaInstructor> CoordinacionesAcademicasInstructores { get; set; }
        public List<Programa> Programas { get; set; }
        public List<Queja> Quejas { get; set; }
        public List<Comite> Comites { get; set; }
    }
}
