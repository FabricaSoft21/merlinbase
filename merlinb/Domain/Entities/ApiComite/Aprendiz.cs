﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Aprendiz
    {
        public int IdAprendiz { get; set; }
        public string NumDocumento { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int TipoDocumentoId { get; set; }
        public int FichaId { get; set; }
        public bool Estado { get; set; } = true;
        public TipoDocumento TipoDocumento { get; set; }
        public Ficha Ficha { get; set; }
        public List<Queja> Quejas { get; set; }
    }
}
