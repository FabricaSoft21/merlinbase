﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.ApiComite
{
    public class Comite
    {
        public int IdComite { get; set; }
        public DateTime Fecha { get; set; }
        public string Acta { get; set; }
        public string Resolucion { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public bool Estado { get; set; } = true;
        public CoordinacionAcademica CoordinacionAcademica { get; set; }
        public List<DetalleComite> DetalleComite { get; set; }
    }
}
