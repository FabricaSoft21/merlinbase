﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoDocumento
{
    public sealed class TipoDocumentoNotFoundException : NotFoundException
    {
        public TipoDocumentoNotFoundException(int id)
            : base($"No se encontró el tipo de documento con el identificador {id} en la base de datos.") { }
    }
}
