﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoDocumento
{
    public sealed class TipoDocumentoNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public TipoDocumentoNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos el tipo de documento {nombre}") { }
    }
}
