﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoPrograma
{
    public sealed class TipoProgramaNotFoundException : NotFoundException
    {
        public TipoProgramaNotFoundException(int id)
            : base($"No se encontró el tipo de programa con el identificador {id} en la base de datos.") { }
    }
}
