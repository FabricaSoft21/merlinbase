﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoPrograma
{
    public sealed class TipoProgramaNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public TipoProgramaNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos el tipo de programa {nombre}") { }
    }
}
