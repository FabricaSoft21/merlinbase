﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Queja
{
    public sealed class QuejaNotFoundException : NotFoundException
    {
        public QuejaNotFoundException(int id)
            : base($"No se encontró la queja con el identificador {id} en la base de datos.") { }
    }
}
