﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Ficha
{
    public sealed class FichaCodigoNotUniqueBadRequestException : BadRequestException
    {
        public FichaCodigoNotUniqueBadRequestException(string codigo)
            : base($"Ya se encuentra registrado en la base de datos la ficha con el codigo {codigo}") { }
    }
}
