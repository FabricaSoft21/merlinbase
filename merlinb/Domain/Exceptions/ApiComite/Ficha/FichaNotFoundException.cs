﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Ficha
{
    public sealed class FichaNotFoundException : NotFoundException
    {
        public FichaNotFoundException(int id)
            : base($"No se encontró la ficha con el identificador {id} en la base de datos.") { }
    }
}
