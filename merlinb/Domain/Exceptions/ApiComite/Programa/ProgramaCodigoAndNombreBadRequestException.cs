﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Programa
{
    public sealed class ProgramaCodigoAndNombreBadRequestException : BadRequestException
    {
        public ProgramaCodigoAndNombreBadRequestException(string codigo, int modalidadId, int tipoProgramaId)
            : base($"Ya se encuentra registrado en la base de datos el programa con codigo {codigo}, modalidad con identificador {modalidadId} y tipo programa con identificador {tipoProgramaId}") { }
    }
}
