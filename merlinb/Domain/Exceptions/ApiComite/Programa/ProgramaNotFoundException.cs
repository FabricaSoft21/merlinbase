﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Programa
{
    public sealed class ProgramaNotFoundException : NotFoundException
    {
        public ProgramaNotFoundException(int id)
            : base($"No se encontró el programa con el identificador {id} en la base de datos.") { }
    }
}
