﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Competencia
{
    public class CompetenciaNotFoundException : NotFoundException
    {
        public CompetenciaNotFoundException(int id)
            : base($"No se encontró la competencia con el identificador {id} en la base de datos.") { }
    }
}
