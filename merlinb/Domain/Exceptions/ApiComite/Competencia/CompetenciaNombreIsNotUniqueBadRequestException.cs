﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Competencia
{
    public class CompetenciaNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public CompetenciaNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos la competencia {nombre}") { }
    }
}
