﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Instructor
{
    public sealed class InstructorEmailIsNotUniqueBadRequestException : BadRequestException
    {
        public InstructorEmailIsNotUniqueBadRequestException(string email)
            : base($"Ya se encuentra registrado en la base de datos el instructor con el email {email}") { }
    }
}
