﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Instructor
{
    public sealed class InstructorNotFoundException : NotFoundException
    {
        public InstructorNotFoundException(int id)
            : base($"No se encontró el instructor con el identificador {id} en la base de datos.") { }
    }
}
