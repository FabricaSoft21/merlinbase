﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.EstadoQueja
{
    public sealed class EstadoQuejaNotFoundException : NotFoundException
    {
        public EstadoQuejaNotFoundException(int id)
            : base($"No se encontró el estado de la queja con el identificador {id} en la base de datos.") { }
    }
}
