﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.EstadoQueja
{
    public sealed class EstadoQuejaNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public EstadoQuejaNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos el estado de la queja {nombre}") { }
    }
}
