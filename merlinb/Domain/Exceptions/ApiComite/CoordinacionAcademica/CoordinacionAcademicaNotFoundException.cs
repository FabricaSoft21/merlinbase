﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.CoordinacionAcademica
{
    public sealed class CoordinacionAcademicaNotFoundException : NotFoundException
    {
        public CoordinacionAcademicaNotFoundException(int id)
            : base($"No se encontró la coordinación academica con el identificador {id} en la base de datos.") { }
    }
}
