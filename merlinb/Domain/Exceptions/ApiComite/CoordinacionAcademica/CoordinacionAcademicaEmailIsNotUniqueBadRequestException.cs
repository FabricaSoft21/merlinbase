﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.CoordinacionAcademica
{
    public sealed class CoordinacionAcademicaEmailIsNotUniqueBadRequestException : BadRequestException
    {
        public CoordinacionAcademicaEmailIsNotUniqueBadRequestException(string email)
            : base($"Ya se encuentra registrado en la base de datos la coordinación academica con el email {email}") { }
    }
}
