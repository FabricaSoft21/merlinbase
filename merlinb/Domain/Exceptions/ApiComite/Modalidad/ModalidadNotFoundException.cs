﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Modalidad
{
    public sealed class ModalidadNotFoundException : NotFoundException
    {
        public ModalidadNotFoundException(int id)
            : base($"No se encontró la modalidad con el identificador {id} en la base de datos.") { }
    }
}
