﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Modalidad
{
    public sealed class ModalidadNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public ModalidadNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos la modalidad {nombre}") { }
    }
}
