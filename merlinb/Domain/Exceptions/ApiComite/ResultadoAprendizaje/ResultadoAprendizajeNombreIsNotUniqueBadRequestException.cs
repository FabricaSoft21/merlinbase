﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.ResultadoAprendizaje
{
    public sealed class ResultadoAprendizajeNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public ResultadoAprendizajeNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos resultado de aprendizaje {nombre}") { }
    }
}
