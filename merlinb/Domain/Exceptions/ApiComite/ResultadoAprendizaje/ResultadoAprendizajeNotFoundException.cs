﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.ResultadoAprendizaje
{
    public sealed class ResultadoAprendizajeNotFoundException : NotFoundException
    {
        public ResultadoAprendizajeNotFoundException(int id)
            : base($"No se encontró el resultado de aprendizaje con el identificador {id} en la base de datos.") { }
    }
}
