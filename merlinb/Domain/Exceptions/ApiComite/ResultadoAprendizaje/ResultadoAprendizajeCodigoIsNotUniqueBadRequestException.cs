﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.ResultadoAprendizaje
{
    public sealed class ResultadoAprendizajeCodigoIsNotUniqueBadRequestException : BadRequestException
    {
        public ResultadoAprendizajeCodigoIsNotUniqueBadRequestException(string codigo)
            : base($"Ya se encuentra registrado en la base de datos resultado de aprendizaje con el codigo {codigo}") { }
    }
}
