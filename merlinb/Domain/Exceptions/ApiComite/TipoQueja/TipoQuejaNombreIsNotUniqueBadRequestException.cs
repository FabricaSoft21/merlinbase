﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoQueja
{
    public sealed class TipoQuejaNombreIsNotUniqueBadRequestException : BadRequestException
    {
        public TipoQuejaNombreIsNotUniqueBadRequestException(string nombre)
            : base($"Ya se encuentra registrado en la base de datos el tipo de queja {nombre}") { }
    }
}
