﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.TipoQueja
{
    public sealed class TipoQuejaNotFoundException : NotFoundException
    {
        public TipoQuejaNotFoundException(int id)
            : base($"No se encontró el tipo de queja con el identificador {id} en la base de datos.") { }
    }
}
