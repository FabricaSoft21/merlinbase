﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Comite
{
    public class ComiteNotFoundException : NotFoundException
    {
        public ComiteNotFoundException(int id)
            : base($"No se encontró el comite con el identificador {id} en la base de datos.") { }
    }
}
