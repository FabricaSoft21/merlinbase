﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Aprendiz
{
    public sealed class AprendizNotFoundException : NotFoundException
    {
        public AprendizNotFoundException(int id)
            : base($"No se encontró el aprendiz con el identificador {id} en la base de datos.") { }
    }
}
