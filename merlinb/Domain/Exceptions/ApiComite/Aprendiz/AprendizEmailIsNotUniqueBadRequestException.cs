﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Aprendiz
{
    public sealed class AprendizEmailIsNotUniqueBadRequestException : BadRequestException
    {
        public AprendizEmailIsNotUniqueBadRequestException(string email)
            : base($"Ya se encuentra registrado en la base de datos el aprendiz con el email {email}") { }
    }
}
