﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exceptions.ApiComite.Aprendiz
{
    public sealed class AprendizNumDocumentoIsNotUniqueBadRequestException : BadRequestException
    {
        public AprendizNumDocumentoIsNotUniqueBadRequestException(string numDocumento)
            : base($"Ya se encuentra registrado en la base de datos el aprendiz con el número de documento {numDocumento}") { }
    }
}
