﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ITipoDocumentoRepository
    {
        Task<List<TipoDocumento>> GetAllAsync();
        Task<TipoDocumento> GetByIdAsync(int id);
        Task<TipoDocumento> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        void Insert(TipoDocumento tipoDocumento);
    }
}
