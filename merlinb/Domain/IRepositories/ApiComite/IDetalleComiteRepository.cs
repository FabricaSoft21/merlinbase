﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IDetalleComiteRepository
    {
        Task<List<DetalleComite>> GetByIdsAsync(int id);
        void Insert(DetalleComite detalleComite);
        void Remove(DetalleComite detalleComite);
    }
}
