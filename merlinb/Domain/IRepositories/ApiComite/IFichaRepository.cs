﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IFichaRepository
    {
        Task<List<Ficha>> GetAllAsync();
        Task<Ficha> GetByIdAsync(int id);
        Task<Ficha> GetByIdNotWhereAsync(int id);
        Task<int> GetByCodigoIsUnique(string codigo);
        void Insert(Ficha ficha);
    }
}
