﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ITipoProgramaRepository
    {
        Task<List<TipoPrograma>> GetAllAsync();
        Task<TipoPrograma> GetByIdAsync(int id);
        Task<TipoPrograma> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        void Insert(TipoPrograma tipoPrograma);
    }
}
