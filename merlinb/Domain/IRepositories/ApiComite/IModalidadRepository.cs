﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IModalidadRepository
    {
        Task<List<Modalidad>> GetAllAsync();
        Task<Modalidad> GetByIdAsync(int id);
        Task<Modalidad> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        void Insert(Modalidad modalidad);
    }
}
