﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IInstructorRepository
    {
        Task<List<Instructor>> GetAllAsync();
        Task<Instructor> GetByIdAsync(int id);
        Task<Instructor> GetByIdNotWhereAsync(int id);
        Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email);
        Task<Instructor> Insert(Instructor instructor);
    }
}
