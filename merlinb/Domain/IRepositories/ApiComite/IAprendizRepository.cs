﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IAprendizRepository
    {
        Task<List<Aprendiz>> GetAllAsync();
        Task<Aprendiz> GetByIdAsync(int id);
        Task<Aprendiz> GetByIdNotWhereAsync(int id);
        Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email);
        void Insert(Aprendiz aprendiz);
    }
}
