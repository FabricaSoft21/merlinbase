﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ITipoQuejaRepository
    {
        Task<List<TipoQueja>> GetAllAsync();
        Task<TipoQueja> GetByIdAsync(int id);
        Task<TipoQueja> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        void Insert(TipoQueja tipoQueja);
    }
}
