﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IEstadoQuejaRepository
    {
        Task<List<EstadoQueja>> GetAllAsync();
        Task<EstadoQueja> GetByIdAsync(int id);
        Task<EstadoQueja> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        void Insert(EstadoQueja estadoQueja);
    }
}
