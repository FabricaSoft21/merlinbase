﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ICompetenciaRepository
    {
        Task<List<Competencia>> GetAllAsync();
        Task<Competencia> GetByIdAsync(int id);
        Task<Competencia> GetByIdNotWhereAsync(int id);
        Task<int> GetByNombreIsUnique(string nombre);
        Task<Competencia> Insert(Competencia competencia);
    }
}
