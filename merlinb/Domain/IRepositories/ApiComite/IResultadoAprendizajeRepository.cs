﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IResultadoAprendizajeRepository
    {
        Task<List<ResultadoAprendizaje>> GetAllAsync();
        Task<ResultadoAprendizaje> GetByIdAsync(int id);
        Task<ResultadoAprendizaje> GetByIdNotWhereAsync(int id);
        Task<int> GetByCodigoNombreIsUnique(string codigo, string nombre);
        void Insert(ResultadoAprendizaje resultadoAprendizaje);
    }
}
