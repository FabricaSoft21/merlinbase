﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IComiteRepository
    {
        Task<List<Comite>> GetAllAsync();
        Task<Comite> GetByIdAsync(int id);
        Task<Comite> GetByIdNotWhereAsync(int id);
        Task<Comite> Insert(Comite comite);
    }
}
