﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IProgramaRepository
    {
        Task<List<Programa>> GetAllAsync();
        Task<Programa> GetByIdAsync(int id);
        Task<Programa> GetByIdNotWhereAsync(int id);
        //Validar que no exita el mismo codigo con una misma modalidad
        Task<int> GetByCodigoModalidadIsUnique(string codigo, int modalidadId, int tipoProgramaId);
        Task<Programa> Insert(Programa programa);
    }
}
