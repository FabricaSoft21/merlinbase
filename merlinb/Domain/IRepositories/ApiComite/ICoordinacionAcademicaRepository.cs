﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ICoordinacionAcademicaRepository
    {
        Task<List<CoordinacionAcademica>> GetAllAsync();
        Task<CoordinacionAcademica> GetByIdAsync(int id);
        Task<CoordinacionAcademica> GetByIdNotWhereAsync(int id);
        Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email);
        void Insert(CoordinacionAcademica coordinacionAcademica);
    }
}
