﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface ICompetenciaProgramaRepository
    {
        Task<List<CompetenciaPrograma>> GetByIdsAsync(int id);
        void Insert(CompetenciaPrograma competenciaPrograma);
        void Remove(CompetenciaPrograma competenciaPrograma);
    }
}
