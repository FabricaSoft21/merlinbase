﻿using Domain.Entities.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IQuejaRepository
    {
        Task<List<Queja>> GetAllAsync();
        Task<Queja> GetByIdAsync(int id);
        Task<Queja> GetByIdNotWhereAsync(int id);
        void Insert(Queja queja);
    }
}
