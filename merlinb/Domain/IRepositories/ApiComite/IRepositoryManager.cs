﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepositories.ApiComite
{
    public interface IRepositoryManager
    {
        IUnitOfWork UnitOfWork { get; }
        ITipoDocumentoRepository TipoDocumentoRepository { get; }
        ITipoProgramaRepository TipoProgramaRepository { get; }
        IModalidadRepository ModalidadRepository { get; }
        ITipoQuejaRepository TipoQuejaRepository { get; }
        IEstadoQuejaRepository EstadoQuejaRepository { get; }
        ICoordinacionAcademicaRepository CoordinacionAcademicaRepository { get; }
        IFichaRepository FichaRepository { get; }
        ICompetenciaRepository CompetenciaRepository { get; }
        IInstructorRepository InstructorRepository { get; }
        IAprendizRepository AprendizRepository { get; }
        IQuejaRepository QuejaRepository { get; }
        IResultadoAprendizajeRepository ResultadoAprendizajeRepository { get; }
        IProgramaRepository ProgramaRepository { get; }
        ICoordinacionAcademicaInstructorRepository CoordinacionAcademicaInstructorRepository { get; }
        ICompetenciaProgramaRepository CompetenciaProgramaRepository { get; }
        IComiteRepository ComiteRepository { get; }
        IDetalleComiteRepository DetalleComiteRepository { get; }
    }
}
