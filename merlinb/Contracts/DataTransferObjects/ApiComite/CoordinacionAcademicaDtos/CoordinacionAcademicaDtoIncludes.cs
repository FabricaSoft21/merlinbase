﻿using Contracts.DataTransferObjects.ApiComite.TipoDocumentoDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos
{
    public class CoordinacionAcademicaDtoIncludes : CoordinacionAcademicaDto
    {
        public int TipoDocumentoId { get; set; }
        public string NombreTipoDocumento { get; set; }
    }
}
