﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos
{
    public class CoordinacionAcademicaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese un número de documento.")]
        [MinLength(3, ErrorMessage = "El número de documento debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El número de documento debe tener máximo {1} caracteres.")]
        public string NumDocumento { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un nombre.")]
        [MinLength(3, ErrorMessage = "El nombre debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El nombre debe tener máximo {1} caracteres.")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un apellido.")]
        [MinLength(3, ErrorMessage = "El apellido debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El apellido debe tener máximo {1} caracteres.")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un email.")]
        [EmailAddress(ErrorMessage = "Ingrese un email valido.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un teléfono.")]
        [Phone(ErrorMessage = "Ingrese un teléfono valido.")]
        public string Telefono { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un tipo de documento.")]
        public int? TipoDocumentoId { get; set; }
    }
}
