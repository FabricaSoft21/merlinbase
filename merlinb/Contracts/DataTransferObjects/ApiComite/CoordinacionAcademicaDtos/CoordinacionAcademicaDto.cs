﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos
{
    public class CoordinacionAcademicaDto
    {
        public int IdCoordinacionAcademica { get; set; }
        public string NumDocumento { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
    }
}
