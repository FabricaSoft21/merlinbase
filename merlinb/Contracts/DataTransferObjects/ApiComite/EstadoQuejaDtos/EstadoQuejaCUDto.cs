﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.EstadoQuejaDtos
{
    public class EstadoQuejaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre del estado de la queja.")]
        [MinLength(3, ErrorMessage = "El nombre del estado de la queja debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El nombre del estado de la queja debe tener máximo {1} caracteres.")]
        public string Nombre { get; set; }
    }
}
