﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.EstadoQuejaDtos
{
    public class EstadoQuejaDto
    {
        public int IdEstadoQueja { get; set; }
        public string Nombre { get; set; }
    }
}
