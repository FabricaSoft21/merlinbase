﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ProgramaDtos
{
    public class ProgramaDto
    {
        public int IdPrograma { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
