﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ProgramaDtos
{
    public class ProgramaDtoInclude : ProgramaDto
    {
        public int TipoProgramaId { get; set; }
        public string NombreTipoPrograma { get; set; }
        public int ModalidadId { get; set; }
        public string NombreModalidad { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public string NombreCoordinacionAcademica { get; set; }
    }
}
