﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ProgramaDtos
{
    public class ProgramaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el codigo del programa.")]
        public string Codigo { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre del programa.")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un tipo de programa")]
        public int? TipoProgramaId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese una modalidad.")]
        public int? ModalidadId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese una coordinación académica.")]
        public int? CoordinacionAcademicaId { get; set; }
    }
}
