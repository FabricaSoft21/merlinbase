﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.QuejaDtos
{
    public class QuejaDtoIncludes : QuejaDto
    {
        public int TipoQuejaId { get; set; }
        public string NombreTipoQueja { get; set; }
        public int EstadoQuejaId { get; set; }
        public string NombreEstadoQueja { get; set; }
        public int InstructorId { get; set; }
        public string NombreInstructor { get; set; }
        public int AprendizId { get; set; }
        public string NombreAprendiz { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public string NombreCoordinacionAcademica { get; set; }
    }
}
