﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.QuejaDtos
{
    public class QuejaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese la descripción de la queja.")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el tipo de queja.")]
        public int? TipoQuejaId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el estado de la queja.")]
        public int? EstadoQuejaId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el instructor que genero la queja.")]
        public int? InstructorId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el aprendiz al cual esta asociada la queja.")]
        public int? AprendizId { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese la coordinación academica a la cual pertenece el aprendiz.")]
        public int? CoordinacionAcademicaId { get; set; }
    }
}
