﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.QuejaDtos
{
    public class QuejaDto
    {
        public int IdQueja { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
    }
}
