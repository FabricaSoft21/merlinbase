﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.FichaDtos
{
    public class FichaDtoInclude : FichaDto
    {
        public int ProgramaId { get; set; }
        public string NombrePrograma { get; set; }
    }
}
