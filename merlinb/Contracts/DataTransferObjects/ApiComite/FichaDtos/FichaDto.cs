﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.FichaDtos
{
    public class FichaDto
    {
        public int IdFicha { get; set; }
        public string Codigo { get; set; }
    }
}
