﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.FichaDtos
{
    public class FichaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el codigo de la ficha.")]
        public string Codigo { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese un programa.")]
        public int? ProgramaId { get; set; }
    }
}
