﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ResultadoAprendizajeDtos
{
    public class ResultadoAprendizajeDtoIncludes : ResultadoAprendizajeDto
    {
        public int CompetenciaId { get; set; }
        public string NombreCompetencia { get; set; }
    }
}
