﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ResultadoAprendizajeDtos
{
    public class ResultadoAprendizajeCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el codigo del resultado de aprendizaje.")]
        public string Codigo { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre del resultado de aprendizaje.")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese una competencia.")]
        public int? CompetenciaId { get; set; }
    }
}
