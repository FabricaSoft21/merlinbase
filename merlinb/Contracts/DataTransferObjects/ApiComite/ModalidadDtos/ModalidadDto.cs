﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ModalidadDtos
{
    public class ModalidadDto
    {
        public int IdModalidad { get; set; }
        public string Nombre { get; set; }
    }
}
