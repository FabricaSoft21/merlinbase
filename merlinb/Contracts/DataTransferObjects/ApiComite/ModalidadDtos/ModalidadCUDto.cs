﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ModalidadDtos
{
    public class ModalidadCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre de la modaldad.")]
        [MinLength(3, ErrorMessage = "El nombre de la modalidad debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El nombre de la modalidad debe tener máximo {1} caracteres.")]
        public string Nombre { get; set; }
    }
}
