﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ComiteDtos
{
    public class ComiteCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese la fecha del comite.")]
        public DateTime Fecha { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese el acta del comite.")]
        public string Acta { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese la resolución del comite.")]
        public string Resolucion { get; set; }
        [Required(ErrorMessage = "Campo requerido... Ingrese una coordinación academica.")]
        public int? CoordinacionAcademicaId { get; set; }
        public List<int> QuejasIds { get; set; }
    }
}
