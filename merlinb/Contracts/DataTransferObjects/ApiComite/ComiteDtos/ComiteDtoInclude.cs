﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ComiteDtos
{
    public class ComiteDtoInclude
    {
        public int IdComite { get; set; }
        public DateTime Fecha { get; set; }
        public string Acta { get; set; }
        public string Resolucion { get; set; }
        public int CoordinacionAcademicaId { get; set; }
        public string NombreCoordinacionAcademica { get; set; }
        public List<string> DescripcionesQueja { get; set; } = new List<string>();
    }
}
