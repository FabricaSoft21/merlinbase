﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.ComiteDtos
{
    public class ComiteDto
    {
        public int IdComite { get; set; }
        public DateTime Fecha { get; set; }
        public string Acta { get; set; }
        public string Resolucion { get; set; }
    }
}
