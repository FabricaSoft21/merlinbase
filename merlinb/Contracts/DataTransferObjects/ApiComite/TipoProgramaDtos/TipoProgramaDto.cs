﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.TipoProgramaDtos
{
    public class TipoProgramaDto
    {
        public int IdTipoPrograma { get; set; }
        public string Nombre { get; set; }
    }
}
