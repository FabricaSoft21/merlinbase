﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.TipoProgramaDtos
{
    public class TipoProgramaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre del tipo de programa.")]
        [MinLength(3, ErrorMessage = "El nombre del tipo de programa debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El nombre del tipo de programa debe tener máximo {1} caracteres.")]
        public string Nombre { get; set; }
    }
}
