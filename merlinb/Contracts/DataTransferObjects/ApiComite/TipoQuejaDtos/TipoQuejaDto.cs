﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.TipoQuejaDtos
{
    public class TipoQuejaDto
    {
        public int IdTipoQueja { get; set; }
        public string Nombre { get; set; }
    }
}
