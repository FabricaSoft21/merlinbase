﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.TipoQuejaDtos
{
    public class TipoQuejaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre del tipo de queja.")]
        [MinLength(3, ErrorMessage = "El nombre del tipo de queja debe tener mínimo {1} caracteres.")]
        [MaxLength(45, ErrorMessage = "El nombre del tipo de queja debe tener máximo {1} caracteres.")]
        public string Nombre { get; set; }
    }
}
