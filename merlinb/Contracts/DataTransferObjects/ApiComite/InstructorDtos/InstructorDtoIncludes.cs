﻿using Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.InstructorDtos
{
    public class InstructorDtoIncludes : InstructorDto
    {
        public int TipoDocumentoId { get; set; }
        public string NombreTipoDocumento { get; set; }
        public List<string> NombresCoordinacionesAcademicas { get; set; } = new List<string>();
    }
}
