﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.AprendizDtos
{
    public class AprendizDtoIncludes : AprendizDto
    {
        public int TipoDocumentoId { get; set; }
        public string NombreTipoDocumento { get; set; }
        public int FichaId { get; set; }
        public string CodigoFicha { get; set; }
    }
}
