﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.CompetenciaDtos
{
    public class CompetenciaDtoInclude : CompetenciaDto
    {
        public List<int> ProgramasIds { get; set; } = new List<int>();
        public List<string> NombresProgramas { get; set; } = new List<string>();
    }
}
