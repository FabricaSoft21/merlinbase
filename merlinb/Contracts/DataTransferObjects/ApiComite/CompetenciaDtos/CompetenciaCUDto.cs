﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.DataTransferObjects.ApiComite.CompetenciaDtos
{
    public class CompetenciaCUDto
    {
        [Required(ErrorMessage = "Campo requerido... Ingrese el nombre de la competencia.")]
        public string Nombre { get; set; }
        public List<int> ProgramasIds { get; set; }
    }
}
