﻿using Contracts.DataTransferObjects.ApiComite.TipoDocumentoDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDocumentoController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public TipoDocumentoController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<TipoDocumentoDto>> Gets() =>
            await _serviceManager.TipoDocumentoService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<TipoDocumentoDto> Get([FromRoute] int id) =>
            await _serviceManager.TipoDocumentoService.GetByIdAsync(id);

        [HttpPost]
        public async Task<ActionResult<TipoDocumentoDto>> Post([FromBody] TipoDocumentoCUDto tipoDocumentoCUDto)
        {
            TipoDocumentoDto tipoDocumentoDto = await _serviceManager.TipoDocumentoService.CreateAsync(tipoDocumentoCUDto);
            return tipoDocumentoDto;
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TipoDocumentoDto>> Put([FromRoute] int id, [FromBody] TipoDocumentoCUDto tipoDocumentoCUDto)
        {
            TipoDocumentoDto tipoDocumentoDto = await _serviceManager.TipoDocumentoService.UpdateAsync(id, tipoDocumentoCUDto);
            if (tipoDocumentoDto != null)
                return tipoDocumentoDto;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.TipoDocumentoService.StateChange(id))
            {
                return true;
            }
            return Conflict();
        }
    }
}
