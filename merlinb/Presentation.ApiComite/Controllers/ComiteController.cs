﻿using Contracts.DataTransferObjects.ApiComite.ComiteDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComiteController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public ComiteController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<ComiteDto>> Gets() =>
            await _serviceManager.ComiteService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<ComiteDtoInclude> Get([FromRoute] int id) =>
            await _serviceManager.ComiteService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ComiteCUDto comiteCUDto)
        {
            ComiteDto comiteDto = await _serviceManager.ComiteService.CreateAsync(comiteCUDto);
            return CreatedAtAction(nameof(Get), new { id = comiteDto.IdComite }, comiteDto);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] ComiteCUDto comiteCUDto)
        {
            if (await _serviceManager.ComiteService.UpdateAsync(id, comiteCUDto))
                return Ok();
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<IActionResult> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.ComiteService.StateChange(id))
                return Ok();
            return Conflict();
        }
    }
}
