﻿using Contracts.DataTransferObjects.ApiComite.FichaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FichaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public FichaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<FichaDtoInclude>> Gets() =>
            await _serviceManager.FichaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<FichaDtoInclude> Get([FromRoute] int id) =>
            await _serviceManager.FichaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FichaCUDto fichaCUDto)
        {
            FichaDto fichaDto = await _serviceManager.FichaService.CreateAsync(fichaCUDto);
            return CreatedAtAction(nameof(Get), new { id = fichaDto.IdFicha }, fichaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<FichaDtoInclude>> Put([FromRoute] int id, [FromBody] FichaCUDto fichaCUDto)
        {
            FichaDtoInclude fichaDtoInclude = await _serviceManager.FichaService.UpdateAsync(id, fichaCUDto);
            if (fichaDtoInclude != null)
                return fichaDtoInclude;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.FichaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
