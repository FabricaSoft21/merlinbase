﻿using Contracts.DataTransferObjects.ApiComite.ModalidadDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModalidadController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public ModalidadController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<ModalidadDto>> Gets() =>
            await _serviceManager.ModalidadService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<ModalidadDto> Get([FromRoute] int id) =>
            await _serviceManager.ModalidadService.GetByIdAsync(id);

        [HttpPost]
        public async Task<ActionResult<ModalidadDto>> Post([FromBody] ModalidadCUDto modalidadCUDto)
        {
            ModalidadDto modalidadDto = await _serviceManager.ModalidadService.CreateAsync(modalidadCUDto);
            return modalidadDto;
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<ModalidadDto>> Put([FromRoute] int id, [FromBody] ModalidadCUDto modalidadCUDto)
        {
            ModalidadDto modalidadDto = await _serviceManager.ModalidadService.UpdateAsync(id, modalidadCUDto);
            if (modalidadDto != null)
                return modalidadDto;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange(int id)
        {
            if (await _serviceManager.ModalidadService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
