﻿using Contracts.DataTransferObjects.ApiComite.ResultadoAprendizajeDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultadoAprendizajeController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public ResultadoAprendizajeController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<ResultadoAprendizajeDto>> Gets() =>
            await _serviceManager.ResultadoAprendizajeService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<ResultadoAprendizajeDtoIncludes> Get([FromRoute] int id) =>
            await _serviceManager.ResultadoAprendizajeService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ResultadoAprendizajeCUDto resultadoAprendizajeCUDto)
        {
            ResultadoAprendizajeDto resultadoAprendizajeDto = await _serviceManager.ResultadoAprendizajeService.CreateAsync(resultadoAprendizajeCUDto);
            return CreatedAtAction(nameof(Get), new { id = resultadoAprendizajeDto.IdResultadoAprendizaje }, resultadoAprendizajeDto);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] ResultadoAprendizajeCUDto resultadoAprendizajeCUDto)
        {
            if (await _serviceManager.ResultadoAprendizajeService.UpdateAsync(id, resultadoAprendizajeCUDto))
                return Ok();
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<IActionResult> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.ResultadoAprendizajeService.StateChange(id))
                return Ok();
            return Conflict();
        }
    }
}
