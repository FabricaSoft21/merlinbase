﻿using Contracts.DataTransferObjects.ApiComite.ProgramaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public ProgramaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<ProgramaDtoInclude>> Gets() =>
            await _serviceManager.ProgramaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<ProgramaDtoInclude> Get([FromRoute] int id) =>
            await _serviceManager.ProgramaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProgramaCUDto programaCUDto)
        {
            ProgramaDto programaDto = await _serviceManager.ProgramaService.CreateAsync(programaCUDto);
            return CreatedAtAction(nameof(Get), new { id = programaDto.IdPrograma }, programaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<ProgramaDtoInclude>> Put([FromRoute] int id, [FromBody] ProgramaCUDto programaCUDto)
        {
            ProgramaDtoInclude programaDtoInclude = await _serviceManager.ProgramaService.UpdateAsync(id, programaCUDto);
            if (programaDtoInclude != null)
                return programaDtoInclude;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.ProgramaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
