﻿using Contracts.DataTransferObjects.ApiComite.AprendizDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AprendizController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public AprendizController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<AprendizDtoIncludes>> Gets() =>
            await _serviceManager.AprendizService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<AprendizDto> Get([FromRoute] int id) =>
            await _serviceManager.AprendizService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AprendizCUDto aprendizCUDto)
        {
            AprendizDto aprendizDto = await _serviceManager.AprendizService.CreateAsync(aprendizCUDto);
            return CreatedAtAction(nameof(Get), new { id = aprendizDto.IdAprendiz }, aprendizDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<AprendizDtoIncludes>> Put([FromRoute] int id, [FromBody] AprendizCUDto aprendizCUDto)
        {
            AprendizDtoIncludes aprendizDtoIncludes = await _serviceManager.AprendizService.UpdateAsync(id, aprendizCUDto);
            if (aprendizDtoIncludes != null)
                return aprendizDtoIncludes;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.AprendizService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
