﻿using Contracts.DataTransferObjects.ApiComite.QuejaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuejaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public QuejaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<QuejaDto>> Gets() =>
            await _serviceManager.QuejaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<QuejaDto> Get([FromRoute] int id) =>
            await _serviceManager.QuejaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] QuejaCUDto quejaCUDto)
        {
            QuejaDto quejaDto = await _serviceManager.QuejaService.CreateAsync(quejaCUDto);
            return CreatedAtAction(nameof(Get), new { id = quejaDto.IdQueja }, quejaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] QuejaCUDto quejaCUDto)
        {
            if (await _serviceManager.QuejaService.UpdateAsync(id, quejaCUDto))
                return Ok();
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<IActionResult> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.QuejaService.StateChange(id))
                return Ok();
            return Conflict();
        }
    }
}
