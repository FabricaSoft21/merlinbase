﻿using Contracts.DataTransferObjects.ApiComite.CompetenciaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompetenciaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public CompetenciaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<CompetenciaDtoInclude>> Gets() =>
            await _serviceManager.CompetenciaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<CompetenciaDtoInclude> Get([FromRoute] int id) =>
            await _serviceManager.CompetenciaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CompetenciaCUDto competenciaCUDto)
        {
            CompetenciaDto competenciaDto = await _serviceManager.CompetenciaService.CreateAsync(competenciaCUDto);
            return CreatedAtAction(nameof(Get), new { id = competenciaDto.IdCompetencia }, competenciaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] CompetenciaCUDto competenciaCUDto)
        {
            if (await _serviceManager.CompetenciaService.UpdateAsync(id, competenciaCUDto))
                return Ok();
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<IActionResult> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.CompetenciaService.StateChange(id))
                return Ok();
            return Conflict();
        }
    }
}
