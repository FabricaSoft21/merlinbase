﻿using Contracts.DataTransferObjects.ApiComite.TipoQuejaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoQuejaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public TipoQuejaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<TipoQuejaDto>> Gets() =>
            await _serviceManager.TipoQuejaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<TipoQuejaDto> Get([FromRoute] int id) =>
            await _serviceManager.TipoQuejaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TipoQuejaCUDto tipoQuejaCUDto)
        {
            TipoQuejaDto tipoQuejaDto = await _serviceManager.TipoQuejaService.CreateAsync(tipoQuejaCUDto);
            return CreatedAtAction(nameof(Get), new { id = tipoQuejaDto.IdTipoQueja }, tipoQuejaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TipoQuejaDto>> Put([FromRoute] int id, [FromBody] TipoQuejaCUDto tipoQuejaCUDto)
        {
            TipoQuejaDto tipoQuejaDto = await _serviceManager.TipoQuejaService.UpdateAsync(id, tipoQuejaCUDto);
            if (tipoQuejaDto != null)
                return tipoQuejaDto;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.TipoQuejaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
