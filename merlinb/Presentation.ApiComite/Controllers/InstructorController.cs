﻿using Contracts.DataTransferObjects.ApiComite.InstructorDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InstructorController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public InstructorController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<InstructorDto>> Gets() =>
            await _serviceManager.InstructorService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<InstructorDtoIncludes> Get([FromRoute] int id) =>
            await _serviceManager.InstructorService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] InstructorCUDto instructorCUDto)
        {
            InstructorDto instructorDto = await _serviceManager.InstructorService.CreateAsync(instructorCUDto);
            return CreatedAtAction(nameof(Get), new { id = instructorDto.IdInstructor }, instructorDto);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] InstructorCUDto instructorCUDto)
        {
            if (await _serviceManager.InstructorService.UpdateAsync(id, instructorCUDto))
                return Ok();
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<IActionResult> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.InstructorService.StateChange(id))
                return Ok();
            return Conflict();
        }
    }
}
