﻿using Contracts.DataTransferObjects.ApiComite.EstadoQuejaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoQuejaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public EstadoQuejaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<EstadoQuejaDto>> Gets() =>
            await _serviceManager.EstadoQuejaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<EstadoQuejaDto> Get([FromRoute] int id) =>
            await _serviceManager.EstadoQuejaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EstadoQuejaCUDto estadoQuejaCUDto)
        {
            EstadoQuejaDto estadoQuejaDto = await _serviceManager.EstadoQuejaService.CreateAsync(estadoQuejaCUDto);
            return CreatedAtAction(nameof(Get), new { id = estadoQuejaDto.IdEstadoQueja }, estadoQuejaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<EstadoQuejaDto>> Put([FromRoute] int id, [FromBody] EstadoQuejaCUDto estadoQuejaCUDto)
        {
            EstadoQuejaDto estadoQuejaDto = await _serviceManager.EstadoQuejaService.UpdateAsync(id, estadoQuejaCUDto);
            if (estadoQuejaDto != null)
                return estadoQuejaDto;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.EstadoQuejaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
