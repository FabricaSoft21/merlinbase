﻿using Contracts.DataTransferObjects.ApiComite.TipoProgramaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoProgramaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public TipoProgramaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<TipoProgramaDto>> Gets() =>
            await _serviceManager.TipoProgramaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<TipoProgramaDto> Get([FromRoute] int id) =>
            await _serviceManager.TipoProgramaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<ActionResult<TipoProgramaDto>> Post([FromBody] TipoProgramaCUDto tipoProgramaCUDto)
        {
            TipoProgramaDto tipoProgramaDto = await _serviceManager.TipoProgramaService.CreateAsync(tipoProgramaCUDto);
            return tipoProgramaDto;
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TipoProgramaDto>> Put([FromRoute] int id, [FromBody] TipoProgramaCUDto tipoProgramaCUDto)
        {
            TipoProgramaDto tipoProgramaDto = await _serviceManager.TipoProgramaService.UpdateAsync(id, tipoProgramaCUDto);
            if (tipoProgramaDto != null)
                return tipoProgramaDto;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.TipoProgramaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
