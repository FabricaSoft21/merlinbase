﻿using Contracts.DataTransferObjects.ApiComite.CoordinacionAcademicaDtos;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ApiComite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoordinacionAcademicaController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public CoordinacionAcademicaController(IServiceManager serviceManager) => this._serviceManager = serviceManager;

        [HttpGet]
        public async Task<List<CoordinacionAcademicaDtoIncludes>> Gets() =>
            await _serviceManager.CoordinacionAcademicaService.GetAllAsync();

        [HttpGet("{id:int}")]
        public async Task<CoordinacionAcademicaDtoIncludes> Get([FromRoute] int id) =>
            await _serviceManager.CoordinacionAcademicaService.GetByIdAsync(id);

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CoordinacionAcademicaCUDto coordinacionAcademicaCUDto)
        {
            CoordinacionAcademicaDto coordinacionAcademicaDto = await _serviceManager.CoordinacionAcademicaService.CreateAsync(coordinacionAcademicaCUDto);
            return CreatedAtAction(nameof(Get), new { id = coordinacionAcademicaDto.IdCoordinacionAcademica }, coordinacionAcademicaDto);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<CoordinacionAcademicaDtoIncludes>> Put([FromRoute] int id, [FromBody] CoordinacionAcademicaCUDto coordinacionAcademicaCUDto)
        {
            CoordinacionAcademicaDtoIncludes coordinacionAcademicaDtoIncludes = await _serviceManager.CoordinacionAcademicaService.UpdateAsync(id, coordinacionAcademicaCUDto);
            if (coordinacionAcademicaDtoIncludes != null)
                return coordinacionAcademicaDtoIncludes;
            return Conflict();
        }

        [HttpPut("StateChange/{id:int}")]
        public async Task<ActionResult<bool>> StateChange([FromRoute] int id)
        {
            if (await _serviceManager.CoordinacionAcademicaService.StateChange(id))
                return true;
            return Conflict();
        }
    }
}
