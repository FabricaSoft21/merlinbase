﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class QuejaConfiguration : IEntityTypeConfiguration<Queja>
    {
        public void Configure(EntityTypeBuilder<Queja> builder)
        {
            builder.ToTable(nameof(Queja));
            builder.HasKey(q => q.IdQueja).HasName("PK_IdQueja");
            builder.Property(q => q.Descripcion).IsRequired();
            builder.Property(q => q.Fecha).IsRequired();
            builder.Property(q => q.Estado).HasDefaultValueSql("((1))");
            builder.Property(q => q.TipoQuejaId).IsRequired();
            builder.Property(q => q.EstadoQuejaId).IsRequired();
            builder.Property(q => q.InstructorId).IsRequired();
            builder.Property(q => q.AprendizId).IsRequired();
            builder.Property(q => q.CoordinacionAcademicaId).IsRequired();
            builder.HasOne(tq => tq.TipoQueja).WithMany().HasForeignKey(q => q.TipoQuejaId).HasConstraintName("FK_Queja_TipoQuejaId");
            builder.HasOne(tq => tq.EstadoQueja).WithMany().HasForeignKey(q => q.EstadoQuejaId).HasConstraintName("FK_Queja_EstadoQuejaId");
            builder.HasOne(tq => tq.Instructor).WithMany().HasForeignKey(q => q.InstructorId).HasConstraintName("FK_Queja_InstructorId");
            builder.HasOne(tq => tq.Aprendiz).WithMany().HasForeignKey(q => q.AprendizId).HasConstraintName("FK_Queja_AprendizId");
            builder.HasOne(tq => tq.CoordinacionAcademica).WithMany().HasForeignKey(q => q.CoordinacionAcademicaId).HasConstraintName("FK_Queja_CoordinacionAcademicaId");
        }
    }
}
