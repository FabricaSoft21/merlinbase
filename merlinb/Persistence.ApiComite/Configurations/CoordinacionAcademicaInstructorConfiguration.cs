﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class CoordinacionAcademicaInstructorConfiguration : IEntityTypeConfiguration<CoordinacionAcademicaInstructor>
    {
        public void Configure(EntityTypeBuilder<CoordinacionAcademicaInstructor> builder)
        {
            builder.ToTable(nameof(CoordinacionAcademicaInstructor));
            builder.HasKey(cai => new { cai.CoordinacionAcademicaId, cai.InstructorId }).HasName("PK_CoordinacionAcademicaId_InstructorId");
        }
    }
}
