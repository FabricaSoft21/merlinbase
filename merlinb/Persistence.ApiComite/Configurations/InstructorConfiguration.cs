﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class InstructorConfiguration : IEntityTypeConfiguration<Instructor>
    {
        public void Configure(EntityTypeBuilder<Instructor> builder)
        {
            builder.ToTable(nameof(Instructor));
            builder.HasKey(i => i.IdInstructor).HasName("PK_IdInstructor");
            builder.HasIndex(i => i.NumDocumento).IsUnique();
            builder.HasIndex(i => i.Email).IsUnique();
            builder.Property(i => i.NumDocumento).IsRequired().HasMaxLength(45);
            builder.Property(i => i.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(i => i.Apellido).IsRequired().HasMaxLength(45);
            builder.Property(i => i.Email).IsRequired().HasMaxLength(45);
            builder.Property(i => i.Telefono).IsRequired().HasMaxLength(20);
            builder.Property(i => i.Estado).HasDefaultValueSql("((1))");
            builder.Property(i => i.TipoDocumentoId).IsRequired();
            builder.HasOne(td => td.TipoDocumento).WithMany().HasForeignKey(i => i.TipoDocumentoId).HasConstraintName("FK_Instructor_TipoDocumentoId");
        }
    }
}
