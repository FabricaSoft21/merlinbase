﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class TipoDocumentoConfiguration : IEntityTypeConfiguration<TipoDocumento>
    {
        public void Configure(EntityTypeBuilder<TipoDocumento> builder)
        {
            builder.ToTable(nameof(TipoDocumento));
            builder.HasKey(td => td.IdTipoDocumento).HasName("PK_IdTipoDocumento");
            builder.HasIndex(td => td.Nombre).IsUnique();
            builder.Property(td => td.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Estado).HasDefaultValueSql("((1))");
        }
    }
}
