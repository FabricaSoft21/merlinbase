﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class ComiteConfiguration : IEntityTypeConfiguration<Comite>
    {
        public void Configure(EntityTypeBuilder<Comite> builder)
        {
            builder.ToTable(nameof(Comite));
            builder.HasKey(c => c.IdComite).HasName("PK_IdComite");
            builder.Property(c => c.Fecha).IsRequired();
            builder.Property(c => c.Acta).IsRequired();
            builder.Property(c => c.Resolucion).IsRequired();
            builder.Property(c => c.Estado).HasDefaultValueSql("((1))");
            builder.Property(c => c.CoordinacionAcademicaId).IsRequired();
            builder.HasOne(ca => ca.CoordinacionAcademica).WithMany().HasForeignKey(c => c.CoordinacionAcademicaId).HasConstraintName("FK_Comite_CoordinacionAcademicaId");
        }
    }
}
