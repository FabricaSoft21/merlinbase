﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class CompetenciaConfiguration : IEntityTypeConfiguration<Competencia>
    {
        public void Configure(EntityTypeBuilder<Competencia> builder)
        {
            builder.ToTable(nameof(Competencia));
            builder.HasKey(c => c.IdCompetencia).HasName("PK_IdCompetencia");
            builder.HasIndex(c => c.Nombre).IsUnique();
            builder.Property(c => c.Nombre).IsRequired();
            builder.Property(a => a.Estado).HasDefaultValueSql("((1))");
        }
    }
}
