﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class FichaConfiguration : IEntityTypeConfiguration<Ficha>
    {
        public void Configure(EntityTypeBuilder<Ficha> builder)
        {
            builder.ToTable(nameof(Ficha));
            builder.HasKey(f => f.IdFicha).HasName("PK_IdFicha");
            builder.HasIndex(f => f.Codigo).IsUnique();
            builder.Property(f => f.Codigo).IsRequired();
            builder.Property(f => f.Estado).HasDefaultValueSql("((1))");
            builder.Property(f => f.ProgramaId).IsRequired();
            builder.HasOne(p => p.Programa).WithMany().HasForeignKey(f => f.ProgramaId).HasConstraintName("FK_Ficha_ProgramaId");
        }
    }
}
