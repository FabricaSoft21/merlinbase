﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class ModalidadConfiguration : IEntityTypeConfiguration<Modalidad>
    {
        public void Configure(EntityTypeBuilder<Modalidad> builder)
        {
            builder.ToTable(nameof(Modalidad));
            builder.HasKey(m => m.IdModalidad).HasName("PK_IdModalidad");
            builder.HasIndex(m => m.Nombre).IsUnique();
            builder.Property(m => m.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(m => m.Estado).HasDefaultValueSql("((1))");
        }
    }
}
