﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class TipoQuejaConfiguration : IEntityTypeConfiguration<TipoQueja>
    {
        public void Configure(EntityTypeBuilder<TipoQueja> builder)
        {
            builder.HasKey(tq => tq.IdTipoQueja).HasName("PK_IdTipoQueja");
            builder.HasIndex(tq => tq.Nombre).IsUnique();
            builder.Property(tq => tq.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(tq => tq.Estado).HasDefaultValueSql("((1))");
        }
    }
}
