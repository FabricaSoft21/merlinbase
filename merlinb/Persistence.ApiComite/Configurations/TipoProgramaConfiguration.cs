﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class TipoProgramaConfiguration : IEntityTypeConfiguration<TipoPrograma>
    {
        public void Configure(EntityTypeBuilder<TipoPrograma> builder)
        {
            builder.ToTable(nameof(TipoPrograma));
            builder.HasKey(tp => tp.IdTipoPrograma).HasName("PK_IdTipoPrograma");
            builder.HasIndex(tp => tp.Nombre).IsUnique();
            builder.Property(tp => tp.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(tp => tp.Estado).HasDefaultValueSql("((1))");
        }
    }
}
