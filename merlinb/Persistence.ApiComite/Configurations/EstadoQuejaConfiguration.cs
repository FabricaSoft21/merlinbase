﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class EstadoQuejaConfiguration : IEntityTypeConfiguration<EstadoQueja>
    {
        public void Configure(EntityTypeBuilder<EstadoQueja> builder)
        {
            builder.ToTable(nameof(EstadoQueja));
            builder.HasKey(eq => eq.IdEstadoQueja).HasName("PK_IdEstadoQueja");
            builder.HasIndex(eq => eq.Nombre).IsUnique();
            builder.Property(eq => eq.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Estado).HasDefaultValueSql("((1))");
        }
    }
}
