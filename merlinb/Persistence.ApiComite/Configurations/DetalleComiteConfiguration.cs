﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class DetalleComiteConfiguration : IEntityTypeConfiguration<DetalleComite>
    {
        public void Configure(EntityTypeBuilder<DetalleComite> builder)
        {
            builder.ToTable(nameof(DetalleComite));
            builder.HasKey(dc => new { dc.ComiteId, dc.QuejaId }).HasName("PK_ComiteId_QuejaId");
        }
    }
}
