﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class ProgramaConfiguration : IEntityTypeConfiguration<Programa>
    {
        public void Configure(EntityTypeBuilder<Programa> builder)
        {
            builder.ToTable(nameof(Programa));
            builder.HasKey(p => p.IdPrograma).HasName("PK_IdPrograma");
            builder.Property(p => p.Codigo).IsRequired();
            builder.Property(p => p.Nombre).IsRequired();
            builder.Property(p => p.Estado).HasDefaultValueSql("((1))");
            builder.Property(p => p.TipoProgramaId).IsRequired();
            builder.Property(p => p.ModalidadId).IsRequired();
            builder.Property(p => p.CoordinacionAcademicaId).IsRequired();
            builder.HasOne(tp => tp.TipoPrograma).WithMany().HasForeignKey(p => p.TipoProgramaId).HasConstraintName("FK_Programa_TipoProgramaId");
            builder.HasOne(m => m.Modalidad).WithMany().HasForeignKey(p => p.ModalidadId).HasConstraintName("FK_Programa_ModalidadId");
            builder.HasOne(m => m.CoordinacionAcademica).WithMany().HasForeignKey(p => p.CoordinacionAcademicaId).HasConstraintName("FK_Programa_CoordinacionAcademicaId");
        }
    }
}
