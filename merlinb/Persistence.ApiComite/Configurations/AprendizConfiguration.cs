﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class AprendizConfiguration : IEntityTypeConfiguration<Aprendiz>
    {
        public void Configure(EntityTypeBuilder<Aprendiz> builder)
        {
            builder.ToTable(nameof(Aprendiz));
            builder.HasKey(a => a.IdAprendiz).HasName("PK_IdAprendiz");
            builder.HasIndex(a => a.NumDocumento).IsUnique();
            builder.HasIndex(a => a.Email).IsUnique();
            builder.Property(a => a.NumDocumento).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Apellido).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Email).IsRequired().HasMaxLength(45);
            builder.Property(a => a.Telefono).IsRequired().HasMaxLength(20);
            builder.Property(a => a.Estado).HasDefaultValueSql("((1))");
            builder.Property(a => a.TipoDocumentoId).IsRequired();
            builder.Property(a => a.FichaId).IsRequired();
            builder.HasOne(td => td.TipoDocumento).WithMany().HasForeignKey(a => a.TipoDocumentoId).HasConstraintName("FK_Aprendiz_TipoDocumentoId");
            builder.HasOne(f => f.Ficha).WithMany().HasForeignKey(a => a.FichaId).HasConstraintName("FK_Aprendiz_FichaId");
        }
    }
}
