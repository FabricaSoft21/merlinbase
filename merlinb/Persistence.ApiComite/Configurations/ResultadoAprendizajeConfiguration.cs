﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class ResultadoAprendizajeConfiguration : IEntityTypeConfiguration<ResultadoAprendizaje>
    {
        public void Configure(EntityTypeBuilder<ResultadoAprendizaje> builder)
        {
            builder.ToTable(nameof(ResultadoAprendizaje));
            builder.HasKey(ra => ra.IdResultadoAprendizaje).HasName("PK_IdResultadoAprendizaje");
            builder.HasIndex(ra => ra.Codigo).IsUnique();
            builder.HasIndex(ra => ra.Nombre).IsUnique();
            builder.Property(ra => ra.Codigo).IsRequired();
            builder.Property(ra => ra.Nombre).IsRequired();
            builder.Property(ra => ra.Estado).HasDefaultValueSql("((1))");
            builder.Property(ra => ra.CompetenciaId).IsRequired();
            builder.HasOne(c => c.Competencia).WithMany().HasForeignKey(ra => ra.CompetenciaId).HasConstraintName("FK_ResultadoAprendizaje_CompetenciaId");
        }
    }
}
