﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class CoordinacionAcademicaConfiguration : IEntityTypeConfiguration<CoordinacionAcademica>
    {
        public void Configure(EntityTypeBuilder<CoordinacionAcademica> builder)
        {
            builder.ToTable(nameof(CoordinacionAcademica));
            builder.HasKey(ca => ca.IdCoordinacionAcademica).HasName("PK_IdCoordinacionAcademica");
            builder.HasIndex(ca => ca.NumDocumento).IsUnique();
            builder.HasIndex(ca => ca.Email).IsUnique();
            builder.Property(ca => ca.NumDocumento).IsRequired().HasMaxLength(45);
            builder.Property(ca => ca.Nombre).IsRequired().HasMaxLength(45);
            builder.Property(ca => ca.Apellido).IsRequired().HasMaxLength(45);
            builder.Property(ca => ca.Email).IsRequired().HasMaxLength(45);
            builder.Property(ca => ca.Telefono).IsRequired().HasMaxLength(20);
            builder.Property(ca => ca.Estado).HasDefaultValueSql("((1))");
            builder.Property(ca => ca.TipoDocumentoId).IsRequired();
            builder.HasOne(td => td.TipoDocumento).WithMany().HasForeignKey(ca => ca.TipoDocumentoId).HasConstraintName("FK_CoordinacionAcademica_TipoDocumentoId");
        }
    }
}
