﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Configurations
{
    internal sealed class CompetenciaProgramaConfiguration : IEntityTypeConfiguration<CompetenciaPrograma>
    {
        public void Configure(EntityTypeBuilder<CompetenciaPrograma> builder)
        {
            builder.ToTable(nameof(CompetenciaPrograma));
            builder.HasKey(cp => new { cp.CompetenciaId, cp.ProgramaId }).HasName("PK_CompetenciaId_ProgramaId");
        }
    }
}
