﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.ApiComite.Migrations
{
    public partial class MigrationCorrecionesEntityPrograma : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramaCoordinacionAcademica");

            migrationBuilder.AddColumn<int>(
                name: "CoordinacionAcademicaId",
                table: "Programa",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Programa",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Programa_CoordinacionAcademicaId",
                table: "Programa",
                column: "CoordinacionAcademicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Programa_CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Programa",
                column: "CoordinacionAcademicaIdCoordinacionAcademica");

            migrationBuilder.AddForeignKey(
                name: "FK_Programa_CoordinacionAcademica_CoordinacionAcademicaIdCoordi~",
                table: "Programa",
                column: "CoordinacionAcademicaIdCoordinacionAcademica",
                principalTable: "CoordinacionAcademica",
                principalColumn: "IdCoordinacionAcademica",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Programa_CoordinacionAcademicaId",
                table: "Programa",
                column: "CoordinacionAcademicaId",
                principalTable: "CoordinacionAcademica",
                principalColumn: "IdCoordinacionAcademica",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Programa_CoordinacionAcademica_CoordinacionAcademicaIdCoordi~",
                table: "Programa");

            migrationBuilder.DropForeignKey(
                name: "FK_Programa_CoordinacionAcademicaId",
                table: "Programa");

            migrationBuilder.DropIndex(
                name: "IX_Programa_CoordinacionAcademicaId",
                table: "Programa");

            migrationBuilder.DropIndex(
                name: "IX_Programa_CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Programa");

            migrationBuilder.DropColumn(
                name: "CoordinacionAcademicaId",
                table: "Programa");

            migrationBuilder.DropColumn(
                name: "CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Programa");

            migrationBuilder.CreateTable(
                name: "ProgramaCoordinacionAcademica",
                columns: table => new
                {
                    ProgramaId = table.Column<int>(type: "int", nullable: false),
                    CoordinacionAcademicaId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramaId_CoordinacionAcademicaId", x => new { x.ProgramaId, x.CoordinacionAcademicaId });
                    table.ForeignKey(
                        name: "FK_ProgramaCoordinacionAcademica_CoordinacionAcademica_Coordina~",
                        column: x => x.CoordinacionAcademicaId,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramaCoordinacionAcademica_Programa_ProgramaId",
                        column: x => x.ProgramaId,
                        principalTable: "Programa",
                        principalColumn: "IdPrograma",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramaCoordinacionAcademica_CoordinacionAcademicaId",
                table: "ProgramaCoordinacionAcademica",
                column: "CoordinacionAcademicaId");
        }
    }
}
