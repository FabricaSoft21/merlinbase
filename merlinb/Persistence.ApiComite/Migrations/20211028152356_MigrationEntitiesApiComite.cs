﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.ApiComite.Migrations
{
    public partial class MigrationEntitiesApiComite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Competencia",
                columns: table => new
                {
                    IdCompetencia = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdCompetencia", x => x.IdCompetencia);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "EstadoQueja",
                columns: table => new
                {
                    IdEstadoQueja = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdEstadoQueja", x => x.IdEstadoQueja);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Modalidad",
                columns: table => new
                {
                    IdModalidad = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdModalidad", x => x.IdModalidad);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "TipoDocumento",
                columns: table => new
                {
                    IdTipoDocumento = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdTipoDocumento", x => x.IdTipoDocumento);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "TipoPrograma",
                columns: table => new
                {
                    IdTipoPrograma = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdTipoPrograma", x => x.IdTipoPrograma);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "TipoQueja",
                columns: table => new
                {
                    IdTipoQueja = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdTipoQueja", x => x.IdTipoQueja);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ResultadoAprendizaje",
                columns: table => new
                {
                    IdResultadoAprendizaje = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nombre = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CompetenciaId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    CompetenciaIdCompetencia = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdResultadoAprendizaje", x => x.IdResultadoAprendizaje);
                    table.ForeignKey(
                        name: "FK_ResultadoAprendizaje_Competencia_CompetenciaIdCompetencia",
                        column: x => x.CompetenciaIdCompetencia,
                        principalTable: "Competencia",
                        principalColumn: "IdCompetencia",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResultadoAprendizaje_CompetenciaId",
                        column: x => x.CompetenciaId,
                        principalTable: "Competencia",
                        principalColumn: "IdCompetencia",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "CoordinacionAcademica",
                columns: table => new
                {
                    IdCoordinacionAcademica = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NumDocumento = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Apellido = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Telefono = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TipoDocumentoId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    TipoDocumentoIdTipoDocumento = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdCoordinacionAcademica", x => x.IdCoordinacionAcademica);
                    table.ForeignKey(
                        name: "FK_CoordinacionAcademica_TipoDocumento_TipoDocumentoIdTipoDocum~",
                        column: x => x.TipoDocumentoIdTipoDocumento,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoordinacionAcademica_TipoDocumentoId",
                        column: x => x.TipoDocumentoId,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Instructor",
                columns: table => new
                {
                    IdInstructor = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NumDocumento = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Apellido = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Telefono = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TipoDocumentoId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    TipoDocumentoIdTipoDocumento = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdInstructor", x => x.IdInstructor);
                    table.ForeignKey(
                        name: "FK_Instructor_TipoDocumento_TipoDocumentoIdTipoDocumento",
                        column: x => x.TipoDocumentoIdTipoDocumento,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Instructor_TipoDocumentoId",
                        column: x => x.TipoDocumentoId,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Programa",
                columns: table => new
                {
                    IdPrograma = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nombre = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TipoProgramaId = table.Column<int>(type: "int", nullable: false),
                    ModalidadId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    ModalidadIdModalidad = table.Column<int>(type: "int", nullable: true),
                    TipoProgramaIdTipoPrograma = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdPrograma", x => x.IdPrograma);
                    table.ForeignKey(
                        name: "FK_Programa_Modalidad_ModalidadIdModalidad",
                        column: x => x.ModalidadIdModalidad,
                        principalTable: "Modalidad",
                        principalColumn: "IdModalidad",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Programa_ModalidadId",
                        column: x => x.ModalidadId,
                        principalTable: "Modalidad",
                        principalColumn: "IdModalidad",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Programa_TipoPrograma_TipoProgramaIdTipoPrograma",
                        column: x => x.TipoProgramaIdTipoPrograma,
                        principalTable: "TipoPrograma",
                        principalColumn: "IdTipoPrograma",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Programa_TipoProgramaId",
                        column: x => x.TipoProgramaId,
                        principalTable: "TipoPrograma",
                        principalColumn: "IdTipoPrograma",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Comite",
                columns: table => new
                {
                    IdComite = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Fecha = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Acta = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Resolucion = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CoordinacionAcademicaId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    CoordinacionAcademicaIdCoordinacionAcademica = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdComite", x => x.IdComite);
                    table.ForeignKey(
                        name: "FK_Comite_CoordinacionAcademica_CoordinacionAcademicaIdCoordina~",
                        column: x => x.CoordinacionAcademicaIdCoordinacionAcademica,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comite_CoordinacionAcademicaId",
                        column: x => x.CoordinacionAcademicaId,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "CoordinacionAcademicaInstructor",
                columns: table => new
                {
                    CoordinacionAcademicaId = table.Column<int>(type: "int", nullable: false),
                    InstructorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoordinacionAcademicaId_InstructorId", x => new { x.CoordinacionAcademicaId, x.InstructorId });
                    table.ForeignKey(
                        name: "FK_CoordinacionAcademicaInstructor_CoordinacionAcademica_Coordi~",
                        column: x => x.CoordinacionAcademicaId,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoordinacionAcademicaInstructor_Instructor_InstructorId",
                        column: x => x.InstructorId,
                        principalTable: "Instructor",
                        principalColumn: "IdInstructor",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "CompetenciaPrograma",
                columns: table => new
                {
                    CompetenciaId = table.Column<int>(type: "int", nullable: false),
                    ProgramaId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenciaId_ProgramaId", x => new { x.CompetenciaId, x.ProgramaId });
                    table.ForeignKey(
                        name: "FK_CompetenciaPrograma_Competencia_CompetenciaId",
                        column: x => x.CompetenciaId,
                        principalTable: "Competencia",
                        principalColumn: "IdCompetencia",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetenciaPrograma_Programa_ProgramaId",
                        column: x => x.ProgramaId,
                        principalTable: "Programa",
                        principalColumn: "IdPrograma",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Ficha",
                columns: table => new
                {
                    IdFicha = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ProgramaId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    ProgramaIdPrograma = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdFicha", x => x.IdFicha);
                    table.ForeignKey(
                        name: "FK_Ficha_Programa_ProgramaIdPrograma",
                        column: x => x.ProgramaIdPrograma,
                        principalTable: "Programa",
                        principalColumn: "IdPrograma",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ficha_ProgramaId",
                        column: x => x.ProgramaId,
                        principalTable: "Programa",
                        principalColumn: "IdPrograma",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ProgramaCoordinacionAcademica",
                columns: table => new
                {
                    ProgramaId = table.Column<int>(type: "int", nullable: false),
                    CoordinacionAcademicaId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramaId_CoordinacionAcademicaId", x => new { x.ProgramaId, x.CoordinacionAcademicaId });
                    table.ForeignKey(
                        name: "FK_ProgramaCoordinacionAcademica_CoordinacionAcademica_Coordina~",
                        column: x => x.CoordinacionAcademicaId,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramaCoordinacionAcademica_Programa_ProgramaId",
                        column: x => x.ProgramaId,
                        principalTable: "Programa",
                        principalColumn: "IdPrograma",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Aprendiz",
                columns: table => new
                {
                    IdAprendiz = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    NumDocumento = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Nombre = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Apellido = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Email = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Telefono = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TipoDocumentoId = table.Column<int>(type: "int", nullable: false),
                    FichaId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    FichaIdFicha = table.Column<int>(type: "int", nullable: true),
                    TipoDocumentoIdTipoDocumento = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdAprendiz", x => x.IdAprendiz);
                    table.ForeignKey(
                        name: "FK_Aprendiz_Ficha_FichaIdFicha",
                        column: x => x.FichaIdFicha,
                        principalTable: "Ficha",
                        principalColumn: "IdFicha",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Aprendiz_FichaId",
                        column: x => x.FichaId,
                        principalTable: "Ficha",
                        principalColumn: "IdFicha",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Aprendiz_TipoDocumento_TipoDocumentoIdTipoDocumento",
                        column: x => x.TipoDocumentoIdTipoDocumento,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Aprendiz_TipoDocumentoId",
                        column: x => x.TipoDocumentoId,
                        principalTable: "TipoDocumento",
                        principalColumn: "IdTipoDocumento",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Queja",
                columns: table => new
                {
                    IdQueja = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Fecha = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    TipoQuejaId = table.Column<int>(type: "int", nullable: false),
                    EstadoQuejaId = table.Column<int>(type: "int", nullable: false),
                    InstructorId = table.Column<int>(type: "int", nullable: false),
                    AprendizId = table.Column<int>(type: "int", nullable: false),
                    CoordinacionAcademicaId = table.Column<int>(type: "int", nullable: false),
                    Estado = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValueSql: "((1))"),
                    AprendizIdAprendiz = table.Column<int>(type: "int", nullable: true),
                    CoordinacionAcademicaIdCoordinacionAcademica = table.Column<int>(type: "int", nullable: true),
                    EstadoQuejaIdEstadoQueja = table.Column<int>(type: "int", nullable: true),
                    InstructorIdInstructor = table.Column<int>(type: "int", nullable: true),
                    TipoQuejaIdTipoQueja = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdQueja", x => x.IdQueja);
                    table.ForeignKey(
                        name: "FK_Queja_Aprendiz_AprendizIdAprendiz",
                        column: x => x.AprendizIdAprendiz,
                        principalTable: "Aprendiz",
                        principalColumn: "IdAprendiz",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queja_AprendizId",
                        column: x => x.AprendizId,
                        principalTable: "Aprendiz",
                        principalColumn: "IdAprendiz",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Queja_CoordinacionAcademica_CoordinacionAcademicaIdCoordinac~",
                        column: x => x.CoordinacionAcademicaIdCoordinacionAcademica,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queja_CoordinacionAcademicaId",
                        column: x => x.CoordinacionAcademicaId,
                        principalTable: "CoordinacionAcademica",
                        principalColumn: "IdCoordinacionAcademica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Queja_EstadoQueja_EstadoQuejaIdEstadoQueja",
                        column: x => x.EstadoQuejaIdEstadoQueja,
                        principalTable: "EstadoQueja",
                        principalColumn: "IdEstadoQueja",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queja_EstadoQuejaId",
                        column: x => x.EstadoQuejaId,
                        principalTable: "EstadoQueja",
                        principalColumn: "IdEstadoQueja",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Queja_Instructor_InstructorIdInstructor",
                        column: x => x.InstructorIdInstructor,
                        principalTable: "Instructor",
                        principalColumn: "IdInstructor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queja_InstructorId",
                        column: x => x.InstructorId,
                        principalTable: "Instructor",
                        principalColumn: "IdInstructor",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Queja_TipoQueja_TipoQuejaIdTipoQueja",
                        column: x => x.TipoQuejaIdTipoQueja,
                        principalTable: "TipoQueja",
                        principalColumn: "IdTipoQueja",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Queja_TipoQuejaId",
                        column: x => x.TipoQuejaId,
                        principalTable: "TipoQueja",
                        principalColumn: "IdTipoQueja",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DetalleComite",
                columns: table => new
                {
                    ComiteId = table.Column<int>(type: "int", nullable: false),
                    QuejaId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComiteId_QuejaId", x => new { x.ComiteId, x.QuejaId });
                    table.ForeignKey(
                        name: "FK_DetalleComite_Comite_ComiteId",
                        column: x => x.ComiteId,
                        principalTable: "Comite",
                        principalColumn: "IdComite",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DetalleComite_Queja_QuejaId",
                        column: x => x.QuejaId,
                        principalTable: "Queja",
                        principalColumn: "IdQueja",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_Email",
                table: "Aprendiz",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_FichaId",
                table: "Aprendiz",
                column: "FichaId");

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_FichaIdFicha",
                table: "Aprendiz",
                column: "FichaIdFicha");

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_NumDocumento",
                table: "Aprendiz",
                column: "NumDocumento",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_TipoDocumentoId",
                table: "Aprendiz",
                column: "TipoDocumentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Aprendiz_TipoDocumentoIdTipoDocumento",
                table: "Aprendiz",
                column: "TipoDocumentoIdTipoDocumento");

            migrationBuilder.CreateIndex(
                name: "IX_Comite_CoordinacionAcademicaId",
                table: "Comite",
                column: "CoordinacionAcademicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Comite_CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Comite",
                column: "CoordinacionAcademicaIdCoordinacionAcademica");

            migrationBuilder.CreateIndex(
                name: "IX_Competencia_Nombre",
                table: "Competencia",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompetenciaPrograma_ProgramaId",
                table: "CompetenciaPrograma",
                column: "ProgramaId");

            migrationBuilder.CreateIndex(
                name: "IX_CoordinacionAcademica_Email",
                table: "CoordinacionAcademica",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CoordinacionAcademica_NumDocumento",
                table: "CoordinacionAcademica",
                column: "NumDocumento",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CoordinacionAcademica_TipoDocumentoId",
                table: "CoordinacionAcademica",
                column: "TipoDocumentoId");

            migrationBuilder.CreateIndex(
                name: "IX_CoordinacionAcademica_TipoDocumentoIdTipoDocumento",
                table: "CoordinacionAcademica",
                column: "TipoDocumentoIdTipoDocumento");

            migrationBuilder.CreateIndex(
                name: "IX_CoordinacionAcademicaInstructor_InstructorId",
                table: "CoordinacionAcademicaInstructor",
                column: "InstructorId");

            migrationBuilder.CreateIndex(
                name: "IX_DetalleComite_QuejaId",
                table: "DetalleComite",
                column: "QuejaId");

            migrationBuilder.CreateIndex(
                name: "IX_EstadoQueja_Nombre",
                table: "EstadoQueja",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ficha_Codigo",
                table: "Ficha",
                column: "Codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ficha_ProgramaId",
                table: "Ficha",
                column: "ProgramaId");

            migrationBuilder.CreateIndex(
                name: "IX_Ficha_ProgramaIdPrograma",
                table: "Ficha",
                column: "ProgramaIdPrograma");

            migrationBuilder.CreateIndex(
                name: "IX_Instructor_Email",
                table: "Instructor",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Instructor_NumDocumento",
                table: "Instructor",
                column: "NumDocumento",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Instructor_TipoDocumentoId",
                table: "Instructor",
                column: "TipoDocumentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Instructor_TipoDocumentoIdTipoDocumento",
                table: "Instructor",
                column: "TipoDocumentoIdTipoDocumento");

            migrationBuilder.CreateIndex(
                name: "IX_Modalidad_Nombre",
                table: "Modalidad",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Programa_ModalidadId",
                table: "Programa",
                column: "ModalidadId");

            migrationBuilder.CreateIndex(
                name: "IX_Programa_ModalidadIdModalidad",
                table: "Programa",
                column: "ModalidadIdModalidad");

            migrationBuilder.CreateIndex(
                name: "IX_Programa_TipoProgramaId",
                table: "Programa",
                column: "TipoProgramaId");

            migrationBuilder.CreateIndex(
                name: "IX_Programa_TipoProgramaIdTipoPrograma",
                table: "Programa",
                column: "TipoProgramaIdTipoPrograma");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramaCoordinacionAcademica_CoordinacionAcademicaId",
                table: "ProgramaCoordinacionAcademica",
                column: "CoordinacionAcademicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_AprendizId",
                table: "Queja",
                column: "AprendizId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_AprendizIdAprendiz",
                table: "Queja",
                column: "AprendizIdAprendiz");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_CoordinacionAcademicaId",
                table: "Queja",
                column: "CoordinacionAcademicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_CoordinacionAcademicaIdCoordinacionAcademica",
                table: "Queja",
                column: "CoordinacionAcademicaIdCoordinacionAcademica");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_EstadoQuejaId",
                table: "Queja",
                column: "EstadoQuejaId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_EstadoQuejaIdEstadoQueja",
                table: "Queja",
                column: "EstadoQuejaIdEstadoQueja");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_InstructorId",
                table: "Queja",
                column: "InstructorId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_InstructorIdInstructor",
                table: "Queja",
                column: "InstructorIdInstructor");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_TipoQuejaId",
                table: "Queja",
                column: "TipoQuejaId");

            migrationBuilder.CreateIndex(
                name: "IX_Queja_TipoQuejaIdTipoQueja",
                table: "Queja",
                column: "TipoQuejaIdTipoQueja");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoAprendizaje_Codigo",
                table: "ResultadoAprendizaje",
                column: "Codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoAprendizaje_CompetenciaId",
                table: "ResultadoAprendizaje",
                column: "CompetenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoAprendizaje_CompetenciaIdCompetencia",
                table: "ResultadoAprendizaje",
                column: "CompetenciaIdCompetencia");

            migrationBuilder.CreateIndex(
                name: "IX_ResultadoAprendizaje_Nombre",
                table: "ResultadoAprendizaje",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TipoDocumento_Nombre",
                table: "TipoDocumento",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TipoPrograma_Nombre",
                table: "TipoPrograma",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TipoQueja_Nombre",
                table: "TipoQueja",
                column: "Nombre",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetenciaPrograma");

            migrationBuilder.DropTable(
                name: "CoordinacionAcademicaInstructor");

            migrationBuilder.DropTable(
                name: "DetalleComite");

            migrationBuilder.DropTable(
                name: "ProgramaCoordinacionAcademica");

            migrationBuilder.DropTable(
                name: "ResultadoAprendizaje");

            migrationBuilder.DropTable(
                name: "Comite");

            migrationBuilder.DropTable(
                name: "Queja");

            migrationBuilder.DropTable(
                name: "Competencia");

            migrationBuilder.DropTable(
                name: "Aprendiz");

            migrationBuilder.DropTable(
                name: "CoordinacionAcademica");

            migrationBuilder.DropTable(
                name: "EstadoQueja");

            migrationBuilder.DropTable(
                name: "Instructor");

            migrationBuilder.DropTable(
                name: "TipoQueja");

            migrationBuilder.DropTable(
                name: "Ficha");

            migrationBuilder.DropTable(
                name: "TipoDocumento");

            migrationBuilder.DropTable(
                name: "Programa");

            migrationBuilder.DropTable(
                name: "Modalidad");

            migrationBuilder.DropTable(
                name: "TipoPrograma");
        }
    }
}
