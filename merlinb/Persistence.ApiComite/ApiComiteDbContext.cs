﻿using Domain.Entities.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite
{
    public sealed class ApiComiteDbContext : DbContext
    {
        public ApiComiteDbContext(DbContextOptions options) : base(options) { }
        public DbSet<Aprendiz> Aprendiz { get; set; }
        public DbSet<Comite> Comite { get; set; }
        public DbSet<Competencia> Competencia { get; set; }
        public DbSet<CompetenciaPrograma> CompetenciaPrograma { get; set; }
        public DbSet<CoordinacionAcademica> CoordinacionAcademica { get; set; }
        public DbSet<CoordinacionAcademicaInstructor> CoordinacionAcademicaInstructor { get; set; }
        public DbSet<DetalleComite> DetalleComite { get; set; }
        public DbSet<EstadoQueja> EstadoQueja { get; set; }
        public DbSet<Ficha> Ficha { get; set; }
        public DbSet<Instructor> Instructor { get; set; }
        public DbSet<Modalidad> Modalidad { get; set; }
        public DbSet<Programa> Programa { get; set; }
        public DbSet<Queja> Queja { get; set; }
        public DbSet<ResultadoAprendizaje> ResultadoAprendizaje { get; set; }
        public DbSet<TipoDocumento> TipoDocumento { get; set; }
        public DbSet<TipoPrograma> TipoPrograma { get; set; }
        public DbSet<TipoQueja> TipoQueja { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder) =>
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApiComiteDbContext).Assembly);
    }
}
