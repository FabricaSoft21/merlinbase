﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class EstadoQuejaRepository : IEstadoQuejaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public EstadoQuejaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<EstadoQueja>> GetAllAsync() =>
            await _dbContext.EstadoQueja.Where(eq => eq.Estado == true).ToListAsync();

        public async Task<EstadoQueja> GetByIdAsync(int id) =>
            await _dbContext.EstadoQueja.Where(eq => eq.Estado == true).FirstOrDefaultAsync(eq => eq.IdEstadoQueja == id);

        public async Task<EstadoQueja> GetByIdNotWhereAsync(int id) =>
            await _dbContext.EstadoQueja.FirstOrDefaultAsync(eq => eq.IdEstadoQueja == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.EstadoQueja.AnyAsync(eq => eq.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            else return 0;
        }

        public void Insert(EstadoQueja estadoQueja) => _dbContext.EstadoQueja.Add(estadoQueja);
    }
}
