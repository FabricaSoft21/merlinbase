﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class CoordinacionAcademicaRepository : ICoordinacionAcademicaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public CoordinacionAcademicaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<CoordinacionAcademica>> GetAllAsync() =>
            await _dbContext.CoordinacionAcademica.Include(ca => ca.TipoDocumento).Where(ca => ca.Estado == true).ToListAsync();

        public async Task<CoordinacionAcademica> GetByIdAsync(int id) =>
            await _dbContext.CoordinacionAcademica.Where(ca => ca.Estado == true)
            .Include(ca => ca.TipoDocumento).FirstOrDefaultAsync(ca => ca.IdCoordinacionAcademica == id);

        public async Task<CoordinacionAcademica> GetByIdNotWhereAsync(int id) =>
            await _dbContext.CoordinacionAcademica.FirstOrDefaultAsync(ca => ca.IdCoordinacionAcademica == id);

        public async Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email)
        {
            bool existeNumDocumento = await _dbContext.CoordinacionAcademica.AnyAsync(ca => ca.NumDocumento.Equals(numDocumento.Trim()));
            if (existeNumDocumento)
                return 1;
            bool existeEmail = await _dbContext.CoordinacionAcademica.AnyAsync(ca => ca.Email.Equals(email.Trim().ToLower()));
            if (existeEmail)
                return 2;
            return 0;
        }

        public void Insert(CoordinacionAcademica coordinacionAcademica) => _dbContext.CoordinacionAcademica.Add(coordinacionAcademica);
    }
}
