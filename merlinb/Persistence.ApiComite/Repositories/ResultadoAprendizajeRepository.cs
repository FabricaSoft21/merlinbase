﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class ResultadoAprendizajeRepository : IResultadoAprendizajeRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public ResultadoAprendizajeRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<ResultadoAprendizaje>> GetAllAsync() =>
            await _dbContext.ResultadoAprendizaje.Where(ra => ra.Estado == true).ToListAsync();

        public async Task<ResultadoAprendizaje> GetByIdAsync(int id) =>
            await _dbContext.ResultadoAprendizaje.Where(ra => ra.Estado == true).Include(ra => ra.Competencia)
            .FirstOrDefaultAsync(ra => ra.IdResultadoAprendizaje == id);

        public async Task<ResultadoAprendizaje> GetByIdNotWhereAsync(int id) =>
            await _dbContext.ResultadoAprendizaje.FirstOrDefaultAsync(ra => ra.IdResultadoAprendizaje == id);

        public async Task<int> GetByCodigoNombreIsUnique(string codigo, string nombre)
        {
            bool existeCodigo = await _dbContext.ResultadoAprendizaje.AnyAsync(ra => ra.Codigo.Equals(codigo.Trim()));
            if (existeCodigo)
                return 1;
            bool existeNombre = await _dbContext.ResultadoAprendizaje.AnyAsync(ra => ra.Nombre.Equals(nombre.Trim()));
            if (existeNombre)
                return 2;
            return 0;
        }

        public void Insert(ResultadoAprendizaje resultadoAprendizaje) => _dbContext.ResultadoAprendizaje.Add(resultadoAprendizaje);
    }
}
