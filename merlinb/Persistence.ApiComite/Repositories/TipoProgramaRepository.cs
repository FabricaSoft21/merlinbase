﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal class TipoProgramaRepository : ITipoProgramaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public TipoProgramaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<TipoPrograma>> GetAllAsync() =>
            await _dbContext.TipoPrograma.Where(tp => tp.Estado == true).ToListAsync();

        public async Task<TipoPrograma> GetByIdAsync(int id) =>
            await _dbContext.TipoPrograma.Where(tp => tp.Estado == true).FirstOrDefaultAsync(tp => tp.IdTipoPrograma == id);

        public async Task<TipoPrograma> GetByIdNotWhereAsync(int id) =>
            await _dbContext.TipoPrograma.FirstOrDefaultAsync(tp => tp.IdTipoPrograma == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.TipoPrograma.AnyAsync(tp => tp.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            return 0;
        }

        public void Insert(TipoPrograma tipoPrograma) => _dbContext.TipoPrograma.Add(tipoPrograma);
    }
}
