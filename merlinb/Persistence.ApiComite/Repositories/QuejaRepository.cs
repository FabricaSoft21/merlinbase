﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class QuejaRepository : IQuejaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public QuejaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Queja>> GetAllAsync() =>
            await _dbContext.Queja.Where(q => q.Estado == true).ToListAsync();

        public async Task<Queja> GetByIdAsync(int id) =>
            await _dbContext.Queja.Where(q => q.Estado == true).Include(q => q.TipoQueja)
            .Include(q => q.EstadoQueja).Include(q => q.Aprendiz).Include(q => q.Instructor)
            .Include(q => q.CoordinacionAcademica).FirstOrDefaultAsync(q => q.IdQueja == id);

        public async Task<Queja> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Queja.FirstOrDefaultAsync(q => q.IdQueja == id);

        public void Insert(Queja queja) => _dbContext.Queja.Add(queja);
    }
}
