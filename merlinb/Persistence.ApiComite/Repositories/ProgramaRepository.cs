﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class ProgramaRepository : IProgramaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public ProgramaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Programa>> GetAllAsync() =>
            await _dbContext.Programa.Where(p => p.Estado == true).Include(p => p.Modalidad)
            .Include(p => p.TipoPrograma).Include(p => p.CoordinacionAcademica).ToListAsync();

        public async Task<int> GetByCodigoModalidadIsUnique(string codigo, int modalidadId, int tipoProgramaId)
        {
            bool existeCodigoModalidadTipoPrograma = await _dbContext.Programa.AnyAsync(p => p.Codigo.Equals(codigo) && p.ModalidadId == modalidadId && p.TipoProgramaId == tipoProgramaId);
            //bool existeCodigoModalidad = await _dbContext.Programa.AnyAsync(p => p.Codigo.Equals(codigo) && p.ModalidadId == modalidadId);
            if (existeCodigoModalidadTipoPrograma)
            {
                return 1;
            }
            return 0;
        }

        public async Task<Programa> GetByIdAsync(int id) =>
            await _dbContext.Programa.Where(p => p.Estado == true).Include(p => p.Modalidad).Include(p => p.TipoPrograma)
            .Include(p => p.CoordinacionAcademica).FirstOrDefaultAsync(p => p.IdPrograma == id);

        public async Task<Programa> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Programa.FirstOrDefaultAsync(p => p.IdPrograma == id);

        public async Task<Programa> Insert(Programa programa)
        {
            _dbContext.Programa.Add(programa);
            await _dbContext.SaveChangesAsync();
            return programa;
        }
    }
}
