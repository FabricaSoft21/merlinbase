﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class AprendizRepository : IAprendizRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public AprendizRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Aprendiz>> GetAllAsync() =>
            await _dbContext.Aprendiz.Where(a => a.Estado == true)
            .Include(a => a.Ficha).Include(a => a.TipoDocumento).ToListAsync();

        public async Task<Aprendiz> GetByIdAsync(int id) =>
            await _dbContext.Aprendiz.Where(a => a.Estado == true).Include(a => a.Ficha).Include(a => a.TipoDocumento)
            .FirstOrDefaultAsync(a => a.IdAprendiz == id);

        public async Task<Aprendiz> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Aprendiz.FirstOrDefaultAsync(a => a.IdAprendiz == id);

        public async Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email)
        {
            bool existeNumDocumento = await _dbContext.Aprendiz.AnyAsync(a => a.NumDocumento.Equals(numDocumento.Trim().ToLower()));
            if (existeNumDocumento)
                return 1;
            bool existeEmail = await _dbContext.Aprendiz.AnyAsync(a => a.Email.Equals(email.Trim().ToLower()));
            if (existeEmail)
                return 2;
            return 0;
        }

        public void Insert(Aprendiz aprendiz) => _dbContext.Aprendiz.Add(aprendiz);
    }
}
