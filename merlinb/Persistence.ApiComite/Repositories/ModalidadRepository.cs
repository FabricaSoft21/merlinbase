﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class ModalidadRepository : IModalidadRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public ModalidadRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Modalidad>> GetAllAsync() =>
            await _dbContext.Modalidad.Where(m => m.Estado == true).ToListAsync();

        public async Task<Modalidad> GetByIdAsync(int id) =>
            await _dbContext.Modalidad.Where(m => m.Estado == true).FirstOrDefaultAsync(m => m.IdModalidad == id);

        public async Task<Modalidad> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Modalidad.FirstOrDefaultAsync(m => m.IdModalidad == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.Modalidad.AnyAsync(m => m.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            return 0;
        }

        public void Insert(Modalidad modalidad) => _dbContext.Modalidad.Add(modalidad);
    }
}
