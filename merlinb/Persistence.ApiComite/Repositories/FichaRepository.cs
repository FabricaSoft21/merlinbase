﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class FichaRepository : IFichaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public FichaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Ficha>> GetAllAsync() =>
            await _dbContext.Ficha.Where(f => f.Estado == true).Include(f => f.Programa).ToListAsync();

        public async Task<Ficha> GetByIdAsync(int id) =>
            await _dbContext.Ficha.Where(f => f.Estado == true).Include(f => f.Programa).FirstOrDefaultAsync(f => f.IdFicha == id);

        public async Task<Ficha> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Ficha.FirstOrDefaultAsync(f => f.IdFicha == id);

        public async Task<int> GetByCodigoIsUnique(string codigo)
        {
            bool existeCodigo = await _dbContext.Ficha.AnyAsync(f => f.Codigo.Equals(codigo.Trim()));
            if (existeCodigo)
                return 1;
            return 0;
        }

        public void Insert(Ficha ficha) => _dbContext.Ficha.Add(ficha);
    }
}
