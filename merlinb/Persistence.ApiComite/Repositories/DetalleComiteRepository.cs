﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class DetalleComiteRepository : IDetalleComiteRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public DetalleComiteRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<DetalleComite>> GetByIdsAsync(int id) =>
            await _dbContext.DetalleComite.Where(dc => dc.ComiteId == id).ToListAsync();

        public void Insert(DetalleComite detalleComite) =>
            _dbContext.DetalleComite.Add(detalleComite);

        public void Remove(DetalleComite detalleComite) =>
            _dbContext.DetalleComite.Remove(detalleComite);
    }
}
