﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class CoordinacionAcademicaInstructorRepository : ICoordinacionAcademicaInstructorRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public CoordinacionAcademicaInstructorRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<CoordinacionAcademicaInstructor>> GetByIdsAsync(int id) =>
            await _dbContext.CoordinacionAcademicaInstructor.Where(cai => cai.InstructorId == id).ToListAsync();

        public void Insert(CoordinacionAcademicaInstructor coordinacionAcademicaInstructor) =>
            _dbContext.CoordinacionAcademicaInstructor.Add(coordinacionAcademicaInstructor);

        public void Remove(CoordinacionAcademicaInstructor coordinacionAcademicaInstructor) =>
            _dbContext.CoordinacionAcademicaInstructor.Remove(coordinacionAcademicaInstructor);
    }
}
