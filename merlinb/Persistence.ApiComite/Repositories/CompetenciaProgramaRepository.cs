﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class CompetenciaProgramaRepository : ICompetenciaProgramaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public CompetenciaProgramaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<CompetenciaPrograma>> GetByIdsAsync(int id) =>
            await _dbContext.CompetenciaPrograma.Where(cp => cp.CompetenciaId == id).ToListAsync();

        public void Insert(CompetenciaPrograma competenciaPrograma) =>
            _dbContext.CompetenciaPrograma.Add(competenciaPrograma);

        public void Remove(CompetenciaPrograma competenciaPrograma) =>
            _dbContext.CompetenciaPrograma.Remove(competenciaPrograma);
    }
}
