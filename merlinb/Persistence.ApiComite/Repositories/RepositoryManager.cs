﻿using Domain.IRepositories.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    public sealed class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<IUnitOfWork> _lazyUnitOfWork;
        private readonly Lazy<ITipoDocumentoRepository> _lazyTipoDocumentoRepository;
        private readonly Lazy<ITipoProgramaRepository> _lazyTipoProgramaRepository;
        private readonly Lazy<IModalidadRepository> _lazyModalidadRepository;
        private readonly Lazy<ITipoQuejaRepository> _lazyTipoQuejaRepository;
        private readonly Lazy<IEstadoQuejaRepository> _lazyEstadoQuejaRepository;
        private readonly Lazy<ICoordinacionAcademicaRepository> _lazyCoordinacionAcademicaRepository;
        private readonly Lazy<IFichaRepository> _lazyFichaRepository;
        private readonly Lazy<ICompetenciaRepository> _lazyCompetenciaRepository;
        private readonly Lazy<IInstructorRepository> _lazyInstructorRepository;
        private readonly Lazy<IAprendizRepository> _lazyAprendizRepository;
        private readonly Lazy<IQuejaRepository> _lazyQuejaRepository;
        private readonly Lazy<IResultadoAprendizajeRepository> _lazyResultadoAprendizajeRepository;
        private readonly Lazy<IProgramaRepository> _lazyProgramaRepository;
        private readonly Lazy<ICoordinacionAcademicaInstructorRepository> _lazyCoordinacionAcademicaInstructorRepository;
        private readonly Lazy<ICompetenciaProgramaRepository> _lazyCompetenciaProgramaRepository;
        private readonly Lazy<IComiteRepository> _lazyComiteRepository;
        private readonly Lazy<IDetalleComiteRepository> _lazyDetalleComiteRepository;
        public RepositoryManager(ApiComiteDbContext dbContext)
        {
            this._lazyUnitOfWork = new Lazy<IUnitOfWork>(() => new UnitOfWork(dbContext));
            this._lazyTipoDocumentoRepository = new Lazy<ITipoDocumentoRepository>(() => new TipoDocumentoRepository(dbContext));
            this._lazyTipoProgramaRepository = new Lazy<ITipoProgramaRepository>(() => new TipoProgramaRepository(dbContext));
            this._lazyModalidadRepository = new Lazy<IModalidadRepository>(() => new ModalidadRepository(dbContext));
            this._lazyTipoQuejaRepository = new Lazy<ITipoQuejaRepository>(() => new TipoQuejaRepository(dbContext));
            this._lazyEstadoQuejaRepository = new Lazy<IEstadoQuejaRepository>(() => new EstadoQuejaRepository(dbContext));
            this._lazyCoordinacionAcademicaRepository = new Lazy<ICoordinacionAcademicaRepository>(() => new CoordinacionAcademicaRepository(dbContext));
            this._lazyFichaRepository = new Lazy<IFichaRepository>(() => new FichaRepository(dbContext));
            this._lazyCompetenciaRepository = new Lazy<ICompetenciaRepository>(() => new CompetenciaRepository(dbContext));
            this._lazyInstructorRepository = new Lazy<IInstructorRepository>(() => new InstructorRepository(dbContext));
            this._lazyAprendizRepository = new Lazy<IAprendizRepository>(() => new AprendizRepository(dbContext));
            this._lazyQuejaRepository = new Lazy<IQuejaRepository>(() => new QuejaRepository(dbContext));
            this._lazyResultadoAprendizajeRepository = new Lazy<IResultadoAprendizajeRepository>(() => new ResultadoAprendizajeRepository(dbContext));
            this._lazyProgramaRepository = new Lazy<IProgramaRepository>(() => new ProgramaRepository(dbContext));
            this._lazyCoordinacionAcademicaInstructorRepository = new Lazy<ICoordinacionAcademicaInstructorRepository>(() => new CoordinacionAcademicaInstructorRepository(dbContext));
            this._lazyCompetenciaProgramaRepository = new Lazy<ICompetenciaProgramaRepository>(() => new CompetenciaProgramaRepository(dbContext));
            this._lazyComiteRepository = new Lazy<IComiteRepository>(() => new ComiteRepository(dbContext));
            this._lazyDetalleComiteRepository = new Lazy<IDetalleComiteRepository>(() => new DetalleComiteRepository(dbContext));
        }

        public IUnitOfWork UnitOfWork => _lazyUnitOfWork.Value;
        public ITipoDocumentoRepository TipoDocumentoRepository => _lazyTipoDocumentoRepository.Value;
        public ITipoProgramaRepository TipoProgramaRepository => _lazyTipoProgramaRepository.Value;
        public IModalidadRepository ModalidadRepository => _lazyModalidadRepository.Value;
        public ITipoQuejaRepository TipoQuejaRepository => _lazyTipoQuejaRepository.Value;
        public IEstadoQuejaRepository EstadoQuejaRepository => _lazyEstadoQuejaRepository.Value;
        public ICoordinacionAcademicaRepository CoordinacionAcademicaRepository => _lazyCoordinacionAcademicaRepository.Value;
        public IFichaRepository FichaRepository => _lazyFichaRepository.Value;
        public ICompetenciaRepository CompetenciaRepository => _lazyCompetenciaRepository.Value;
        public IInstructorRepository InstructorRepository => _lazyInstructorRepository.Value;
        public IAprendizRepository AprendizRepository => _lazyAprendizRepository.Value;
        public IQuejaRepository QuejaRepository => _lazyQuejaRepository.Value;
        public IResultadoAprendizajeRepository ResultadoAprendizajeRepository => _lazyResultadoAprendizajeRepository.Value;
        public IProgramaRepository ProgramaRepository => _lazyProgramaRepository.Value;
        public ICoordinacionAcademicaInstructorRepository CoordinacionAcademicaInstructorRepository => _lazyCoordinacionAcademicaInstructorRepository.Value;
        public ICompetenciaProgramaRepository CompetenciaProgramaRepository => _lazyCompetenciaProgramaRepository.Value;
        public IComiteRepository ComiteRepository => _lazyComiteRepository.Value;
        public IDetalleComiteRepository DetalleComiteRepository => _lazyDetalleComiteRepository.Value;
    }
}
