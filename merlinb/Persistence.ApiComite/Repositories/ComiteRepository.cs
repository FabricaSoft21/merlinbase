﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class ComiteRepository : IComiteRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public ComiteRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Comite>> GetAllAsync() =>
            await _dbContext.Comite.Where(c => c.Estado == true).ToListAsync();

        public async Task<Comite> GetByIdAsync(int id) =>
            await _dbContext.Comite.Where(c => c.Estado == true).Include(c => c.CoordinacionAcademica)
            .Include(c => c.DetalleComite).ThenInclude(c => c.Queja).FirstOrDefaultAsync(c => c.IdComite == id);

        public async Task<Comite> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Comite.FirstOrDefaultAsync(c => c.IdComite == id);

        public async Task<Comite> Insert(Comite comite)
        {
            _dbContext.Comite.Add(comite);
            await _dbContext.SaveChangesAsync();
            return comite;
        }
    }
}
