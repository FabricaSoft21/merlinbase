﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class TipoDocumentoRepository : ITipoDocumentoRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public TipoDocumentoRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<TipoDocumento>> GetAllAsync() =>
            await _dbContext.TipoDocumento.Where(td => td.Estado == true).ToListAsync();

        public async Task<TipoDocumento> GetByIdAsync(int id) =>
            await _dbContext.TipoDocumento.Where(td => td.Estado == true).FirstOrDefaultAsync(td => td.IdTipoDocumento == id);

        public async Task<TipoDocumento> GetByIdNotWhereAsync(int id) =>
            await _dbContext.TipoDocumento.FirstOrDefaultAsync(td => td.IdTipoDocumento == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.TipoDocumento.AnyAsync(td => td.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            return 0;
        }

        public void Insert(TipoDocumento tipoDocumento) => _dbContext.TipoDocumento.Add(tipoDocumento);
    }
}
