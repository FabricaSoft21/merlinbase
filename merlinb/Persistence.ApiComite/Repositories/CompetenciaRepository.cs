﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class CompetenciaRepository : ICompetenciaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public CompetenciaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Competencia>> GetAllAsync() =>
            await _dbContext.Competencia.Where(c => c.Estado == true).ToListAsync();

        public async Task<Competencia> GetByIdAsync(int id) =>
            await _dbContext.Competencia.Where(c => c.Estado == true).Include(c => c.CompetenciasProgramas)
            .ThenInclude(cp => cp.Programa).FirstOrDefaultAsync(c => c.IdCompetencia == id);

        public async Task<Competencia> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Competencia.FirstOrDefaultAsync(c => c.IdCompetencia == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.Competencia.AnyAsync(c => c.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            return 0;
        }

        public async Task<Competencia> Insert(Competencia competencia)
        {
            _dbContext.Add(competencia);
            await _dbContext.SaveChangesAsync();
            return competencia;
        }
    }
}
