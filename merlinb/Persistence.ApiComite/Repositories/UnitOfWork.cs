﻿using Domain.IRepositories.ApiComite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class UnitOfWork : IUnitOfWork
    {
        private readonly ApiComiteDbContext _dbContext;
        public UnitOfWork(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<bool> SaveChangesAsync() => await _dbContext.SaveChangesAsync() > 0;
    }
}
