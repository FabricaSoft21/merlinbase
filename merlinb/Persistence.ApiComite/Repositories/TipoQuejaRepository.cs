﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class TipoQuejaRepository : ITipoQuejaRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public TipoQuejaRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<TipoQueja>> GetAllAsync() =>
            await _dbContext.TipoQueja.Where(tq => tq.Estado == true).ToListAsync();

        public async Task<TipoQueja> GetByIdAsync(int id) =>
            await _dbContext.TipoQueja.Where(tq => tq.Estado == true).FirstOrDefaultAsync(tq => tq.IdTipoQueja == id);

        public async Task<TipoQueja> GetByIdNotWhereAsync(int id) =>
            await _dbContext.TipoQueja.FirstOrDefaultAsync(tq => tq.IdTipoQueja == id);

        public async Task<int> GetByNombreIsUnique(string nombre)
        {
            bool existeNombre = await _dbContext.TipoQueja.AnyAsync(tq => tq.Nombre.Equals(nombre.Trim().ToLower()));
            if (existeNombre)
                return 1;
            return 0;
        }

        public void Insert(TipoQueja tipoQueja) => _dbContext.TipoQueja.Add(tipoQueja);
    }
}
