﻿using Domain.Entities.ApiComite;
using Domain.IRepositories.ApiComite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.ApiComite.Repositories
{
    internal sealed class InstructorRepository : IInstructorRepository
    {
        private readonly ApiComiteDbContext _dbContext;
        public InstructorRepository(ApiComiteDbContext dbContext) => this._dbContext = dbContext;

        public async Task<List<Instructor>> GetAllAsync() =>
            await _dbContext.Instructor.Where(i => i.Estado == true).ToListAsync();

        public async Task<Instructor> GetByIdAsync(int id) =>
            await _dbContext.Instructor.Where(i => i.Estado == true).Include(i => i.TipoDocumento).Include(i => i.CoordinacionesAcademicasInstructores)
            .ThenInclude(i => i.CoordinacionAcademica).FirstOrDefaultAsync(i => i.IdInstructor == id);

        public async Task<Instructor> GetByIdNotWhereAsync(int id) =>
            await _dbContext.Instructor.FirstOrDefaultAsync(i => i.IdInstructor == id);

        public async Task<int> GetByNumDocumentoEmailIsUnique(string numDocumento, string email)
        {
            bool existeNumDocumento = await _dbContext.Instructor.AnyAsync(i => i.NumDocumento.Equals(numDocumento.Trim().ToLower()));
            if (existeNumDocumento)
                return 1;
            bool existeEmail = await _dbContext.Instructor.AnyAsync(i => i.Email.Equals(email.Trim().ToLower()));
            if (existeEmail)
                return 2;
            return 0;
        }

        public async Task<Instructor> Insert(Instructor instructor)
        {
            _dbContext.Instructor.Add(instructor);
            await _dbContext.SaveChangesAsync();
            return instructor;
        }
    }
}
