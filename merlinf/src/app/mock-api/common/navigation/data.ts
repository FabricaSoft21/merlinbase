/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';
export const defaultNavigation: FuseNavigationItem[] = [
    {
        id      : 'admin',
        title   : 'Administrador',
        subtitle: '',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'admin.tipodocumento',
                title: 'Tipos de Documentos',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/admin/tipo-documento'
            },
            {
                id      : 'admin.coordinacionacademica',
                title   : 'Coordinaciones Académicas',
                type    : 'basic',
                icon    : 'heroicons_outline:user',
                link    : '/admin/coordinacion-academica'
            }
        ]
    },
    {
        id      : 'coordinacion',
        title   : 'Coordinación Académica',
        subtitle: '',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'coordinacion.modalidad',
                title: 'Modalidades',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/modalidad'
            },
            {
                id   : 'coordinacion.tipoprograma',
                title: 'Tipos de Programas',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/tipo-programa'
            },
            {
                id   : 'coordinacion.tipoqueja',
                title: 'Tipos de Quejas',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/tipo-queja'
            },
            {
                id   : 'coordinacion.estadoqueja',
                title: 'Estados Quejas',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/estado-queja'
            },
            {
                id   : 'coordinacion.programa',
                title: 'Programas',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/programa'
            },
            {
                id   : 'coordinacion.ficha',
                title: 'Fichas',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion-academica/ficha'
            },
            /*{
                id   : 'coordinacion.instructor',
                title: 'Instructor',
                type : 'basic',
                icon : 'heroicons_outline:user',
                link : '/coordinacion/instructor'
            },
            {
                id   : 'coordinacion.competencia',
                title: 'Competencias',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion/competencia'
            },
            {
                id   : 'coordinacion.resultadoaprendizaje',
                title: 'Resultados de Aprendizaje',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion/competencia'
            },
            {
                id   : 'coordinacion.comite',
                title: 'Comites',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coordinacion/comites'
            }*/
        ]
    },
    {
        id      : 'instructor',
        title   : 'Instructor',
        subtitle: '',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id   : 'instructor.aprendiz',
                title: 'Aprendices',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/instructor/aprendiz'
            },
            /*
            {
                id      : 'instructor.queja',
                title   : 'Quejas',
                type    : 'basic',
                icon    : 'heroicons_outline:clipboard-check',
                link    : '/instructor/queja'
            }*/
        ]
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id      : 'admin',
        title   : 'Administrador',
        tooltip : 'Administrador',
        type    : 'aside',
        icon    : 'heroicons_outline:home',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'coordinacionacademica',
        title   : 'Coordinación Académica',
        tooltip : 'Coordinación Académica',
        type    : 'aside',
        icon    : 'heroicons_outline:qrcode',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'instructor',
        title   : 'Instructor',
        tooltip : 'Instructor',
        type    : 'aside',
        icon    : 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id      : 'admin',
        title   : 'ADMINISTRADOR',
        type    : 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'coordinacionacademica',
        title   : 'COORDINACION ACADEMICA',
        type    : 'group',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id   : 'instructor',
        title: 'INSTRUCTOR',
        type : 'group'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id      : 'admin',
        title   : 'Administrador',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'coordinacion',
        title   : 'Coordinación Académica',
        type    : 'group',
        icon    : 'heroicons_outline:qrcode',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
    {
        id      : 'instructor',
        title   : 'Instructor',
        type    : 'group',
        icon    : 'heroicons_outline:document-duplicate',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
