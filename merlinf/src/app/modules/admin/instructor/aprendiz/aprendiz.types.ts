export interface Aprendiz
{
    idAprendiz: string,
    numDocumento: string,
    nombre: string,
    apellido: string,
    email: string,
    telefono: string,
    tipoDocumentoId: string,
    nombreTipoDocumento: string,
    fichaId: string,
    codigoFicha: string,
}

export interface AprendizPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
