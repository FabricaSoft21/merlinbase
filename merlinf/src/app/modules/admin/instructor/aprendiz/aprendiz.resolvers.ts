import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AprendizService } from 'app/modules/admin/instructor/aprendiz/aprendiz.service';
import { Aprendiz, AprendizPagination } from 'app/modules/admin/instructor/aprendiz/aprendiz.types';



@Injectable({
    providedIn: 'root'
})
export class AprendizResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _aprendizService: AprendizService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Aprendiz>
    {
        return this._aprendizService.getAprendizId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class AprendicesResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _aprendizService: AprendizService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: TipoDocumentoPagination; products: TipoDocumento[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Aprendiz[]>
    {
        return this._aprendizService.getAprendices();
    }
}
