import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Aprendiz, AprendizPagination } from 'app/modules/admin/instructor/aprendiz/aprendiz.types';

@Injectable({
    providedIn: 'root'
})
export class AprendizService
{
    // Private
    private _pagination: BehaviorSubject<AprendizPagination | null> = new BehaviorSubject(null);
    private _aprendiz: BehaviorSubject<Aprendiz | null> = new BehaviorSubject(null);
    private _aprendices: BehaviorSubject<Aprendiz[] | null> = new BehaviorSubject(null);

    private aprendiznew: Aprendiz = 
    {
        idAprendiz: "-1",
        numDocumento: "",
        nombre: "",
        apellido: "",
        email: "",
        telefono: "",
        tipoDocumentoId: "",
        nombreTipoDocumento: "",
        fichaId: "",
        codigoFicha: "",
    };

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<AprendizPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get aprendiz$(): Observable<Aprendiz>
    {
        return this._aprendiz.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get aprendices$(): Observable<Aprendiz[]>
    {
        return this._aprendices.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getAprendices(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      //  Observable<{ /*pagination: CoordinacionAcademicaPagination;*/ tiposdocumentos: CoordinacionAcademica[] }>
      Observable<Aprendiz[]>
    {
        return this._httpClient.get<Aprendiz[]>('https://localhost:44307/api/Aprendiz', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._aprendices.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getAprendizId(id: string): Observable<Aprendiz>
    {
        return this._aprendices.pipe(
            take(1),
            map((aprendices) => {

                // Find the product
                const aprendiz = aprendices.find(item => item.idAprendiz.toString() == id) || null;

                // Update the product
                this._aprendiz.next(aprendiz);

                // Return the product
                return aprendiz;
            }),
            switchMap((aprendiz) => {

                if ( !aprendiz )
                {
                    return throwError('No se encontro el aprendiz con el id ' + id + '!');
                }

                return of(aprendiz);
            })
        );
    }

    /**
     * Create product
     */
    createAprendiz(): Observable<Aprendiz>
    {
        return this.aprendices$.pipe(
            take(1),
            switchMap(aprendices => this._httpClient.get<Aprendiz>('https://localhost:44307/api/Aprendiz', {}).pipe(
                map((newAprendiz) => {

                    // Update the tiposdocumentos with the new product
                    let nt = this.aprendiznew;
                    this._aprendices.next([nt, ...aprendices]);
                    // Return the new product
                    return nt;
                })
            ))
        );
    }



    createAprendizPost(persona): Observable<Aprendiz[]>
    {
        return this.aprendices$.pipe(
            take(1),
            switchMap(aprendices => this._httpClient.post<Aprendiz>('https://localhost:44307/api/Aprendiz',
            {
                numDocumento: persona.numDocumento,
                nombre: persona.nombre,
                apellido: persona.apellido,
                email: persona.email,
                telefono: persona.telefono,
                tipoDocumentoId: persona.tipoDocumentoId,
                nombreTipoDocumento: persona.nombreTipoDocumento,
                fichaId: persona.fichaId,
                codigoFicha: persona.codigoFicha,
            }).pipe(
                switchMap(
                    aprendices => this.getAprendices().pipe(
                            tap((aprendices) => {
                            //this._pagination.next(response.pagination);
                            this._aprendices.next(aprendices);
                            return aprendices;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param aprendiz
     */
    updateAprendiz(id: string, aprendiz: Aprendiz): Observable<Aprendiz>
    {
        return this.aprendices$.pipe(
            take(1),
            switchMap(aprendices => this._httpClient.put<Aprendiz>('https://localhost:44307/api/Aprendiz/' + id, {
                numDocumento: aprendiz.numDocumento,
                nombre: aprendiz.nombre,
                apellido: aprendiz.apellido,
                email: aprendiz.email,
                telefono: aprendiz.telefono,
                tipoDocumentoId: aprendiz.tipoDocumentoId,
                nombreTipoDocumento: aprendiz.nombreTipoDocumento,
                fichaId: aprendiz.fichaId,
                codigoFicha: aprendiz.codigoFicha,
            }).pipe(
                map((updatedAprendiz) => {

                    // Find the index of the updated product
                    const index = aprendices.findIndex(item => item.idAprendiz.toString() == id);

                    // Update the product
                    aprendices[index] = updatedAprendiz;
                    

                    // Update the tiposdocumentos
                    this._aprendices.next(aprendices);

                    // Return the updated product
                    return updatedAprendiz;
                }),
                switchMap(updatedAprendiz => this.aprendiz$.pipe(
                    take(1),
                    filter(item => item && item.idAprendiz.toString() == id),
                    tap(() => {

                        // Update the product if it's selected
                        this._aprendiz.next(updatedAprendiz);

                        // Return the updated product
                        return updatedAprendiz;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteAprendiz(id: string): Observable<boolean>
    {
        return this.aprendices$.pipe(
            take(1),
            switchMap(aprendices => this._httpClient.put('https://localhost:44307/api/Aprendiz/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = aprendices.findIndex(item => item.idAprendiz.toString() == id);

                    // Delete the product
                    aprendices.splice(index, 1);

                    // Update the tiposdocumentos
                    this._aprendices.next(aprendices);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
