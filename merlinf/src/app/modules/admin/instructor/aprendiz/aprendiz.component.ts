import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'aprendiz',
    templateUrl    : './aprendiz.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class aprendizComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
