import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';//Tarea
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { Aprendiz, AprendizPagination } from 'app/modules/admin/instructor/aprendiz/aprendiz.types';
import { AprendizService } from 'app/modules/admin/instructor/aprendiz/aprendiz.service';
import { TipoDocumento } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.types';
import { TipoDocumentoService } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.service';
import { Ficha } from 'app/modules/admin/coordinacion/Ficha/ficha.types';
import { FichaService } from 'app/modules/admin/coordinacion/Ficha/ficha.service';

@Component({
    selector       : 'aprendiz-list',
    templateUrl    : './aprendiz.component.html',
    styles         : [
        /* language=SCSS */
        `
            .coordinacion-academica-grid {
                grid-template-columns: 190px 190px auto;

                @screen sm {
                    grid-template-columns: 190px 190px 190px auto;
                }

                @screen md {
                    grid-template-columns: 190px 190px 190px 190px auto;
                }

                @screen lg {
                    grid-template-columns: 250px 250px 250px 250px 250px auto;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class aprendizListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    aprendices$: Observable<Aprendiz[]>;
    tiposdocumentos: TipoDocumento[];
    fichas: Ficha[];

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: AprendizPagination;
    searchInputControl: FormControl = new FormControl();
    selectedAprendiz: Aprendiz | null = null;
    selectedAprendizForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _aprendizService: AprendizService,
        private _tipoDocumentoService: TipoDocumentoService,
        private _fichaService: FichaService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedAprendizForm = this._formBuilder.group({
            idAprendiz         : [''],
            numDocumento       : ['', [Validators.required]],
            nombre             : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(45)]],
            apellido           : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(45)]],
            email              : ['', [Validators.required, Validators.email]],
            telefono           : ['', [Validators.required]],
            tipoDocumentoId    : ['', [Validators.required]],
            nombreTipoDocumento: ['', [Validators.required]],
            fichaId            : ['', [Validators.required]],
            codigoFicha        : ['', [Validators.required]],
        });

        this._tipoDocumentoService.tiposdocumentos$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tiposdocumentos: TipoDocumento[]) => {

                // Update the categories
                this.tiposdocumentos = tiposdocumentos;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
            this._fichaService.fichas$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((fichas: Ficha[]) => {

                // Update the categories
                this.fichas = fichas;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the pagination
        this._aprendizService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: AprendizPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.aprendices$ = this._aprendizService.aprendices$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._aprendizService.getAprendices(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._aprendizService.getAprendices(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(aprendizId: string): void
    {
        // If the product is already selected...
        if ( this.selectedAprendiz && this.selectedAprendiz.idAprendiz === aprendizId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._aprendizService.getAprendizId(aprendizId)
            .subscribe((aprendiz) => {

                // Set the selected product
                this.selectedAprendiz = aprendiz;

                // Fill the form
                this.selectedAprendizForm.patchValue(aprendiz);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedAprendiz = null;
    }

    /**
     * Create tipoDocumento
     */
    createAprendiz(): void
    {
        // Create the tipoDocumento
        this._aprendizService.createAprendiz().subscribe((newAprendiz) => {

            // Go to new tipoDocumento
            this.selectedAprendiz = newAprendiz;

            // Fill the form
            this.selectedAprendizForm.patchValue(newAprendiz);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedAprendiz(): void
    {
        // Get the product object
        const aprendiz = this.selectedAprendizForm.getRawValue();

        if(aprendiz.idAprendiz == "-1"){
            this._aprendizService.createAprendizPost(aprendiz).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.aprendices$ = this._aprendizService.aprendices$;
            this.selectedAprendiz = null;
        } else{
            // Update the product on the server
            this._aprendizService.updateAprendiz(aprendiz.idAprendiz, aprendiz).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedAprendiz(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Aprendiz',
            message: 'Esta seguro que quiere eliminar el aprendiz?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const aprendiz = this.selectedAprendizForm.getRawValue();

                // Delete the product on the server
                this._aprendizService.deleteAprendiz(aprendiz.idAprendiz).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idAprendiz || index;
    }
}
