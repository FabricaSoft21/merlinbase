import { Route } from '@angular/router';
import { aprendizComponent } from './aprendiz/aprendiz.component';
import { aprendizListComponent } from './aprendiz/list/aprendiz.component';
import { AprendicesResolver } from './aprendiz/aprendiz.resolvers';
import { TiposDocumentosResolver } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.resolvers';
import { FichasResolver } from '../coordinacion/Ficha/ficha.resolvers';

export const instructorRoutes: Route[] = [
    {
        path      : '',
        pathMatch : 'full',
        redirectTo: 'aprendiz'
    },
    {
        path     : 'aprendiz',
        component: aprendizComponent,
        children : [
            {
                path     : '',
                component: aprendizListComponent,
                resolve  : {
                    aprendices  : AprendicesResolver,
                    tiposdocumentos: TiposDocumentosResolver,
                    fichas: FichasResolver
                }
            }
        ]
    }
];
