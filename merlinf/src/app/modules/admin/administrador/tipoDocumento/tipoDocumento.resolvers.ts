import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TipoDocumentoService } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.service';
import { TipoDocumento } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.types';


@Injectable({
    providedIn: 'root'
})
export class TipoDocumentoResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _tipoDocumentoService: TipoDocumentoService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoDocumento>
    {
        return this._tipoDocumentoService.getTipoDocumentoId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested tipodocumento is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class TiposDocumentosResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _tipoDocumentoService: TipoDocumentoService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoDocumento[]>
    {
        return this._tipoDocumentoService.getTiposDocumentos();
    }
}
