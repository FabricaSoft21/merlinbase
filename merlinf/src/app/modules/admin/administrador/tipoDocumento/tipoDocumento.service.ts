import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { TipoDocumentoPagination, TipoDocumento } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.types';

@Injectable({
    providedIn: 'root'
})
export class TipoDocumentoService
{
    // Private
    private _pagination: BehaviorSubject<TipoDocumentoPagination | null> = new BehaviorSubject(null);
    private _tipodocumento: BehaviorSubject<TipoDocumento | null> = new BehaviorSubject(null);
    private _tipodocumentos: BehaviorSubject<TipoDocumento[] | null> = new BehaviorSubject(null);

    private tipodocumentonew: TipoDocumento = {idTipoDocumento: "-1", nombre: "Nuevo Tipo Documento"};

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<TipoDocumentoPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for tipodocumento
     */
    get tipodocumento$(): Observable<TipoDocumento>
    {
        return this._tipodocumento.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get tiposdocumentos$(): Observable<TipoDocumento[]>
    {
        return this._tipodocumentos.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getTiposDocumentos(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      Observable<TipoDocumento[]>
    {
        return this._httpClient.get<TipoDocumento[]>('https://localhost:44307/api/tipodocumento', {
        }).pipe(
            tap((response) => {
                this._tipodocumentos.next(response);
            })
        );
    }

    /**
     * Get tipodocumento by id
     */
    getTipoDocumentoId(id: string): Observable<TipoDocumento>
    {
        return this._tipodocumentos.pipe(
            take(1),
            map((tiposdocumentos) => {
                // Find the tipodocumento
                const tipodocumento = tiposdocumentos.find(item => item.idTipoDocumento.toString() == id) || null;
                // Update the tipodocumento
                this._tipodocumento.next(tipodocumento);
                // Return the tipodocumento
                return tipodocumento;
            }),
            switchMap((tipodocumento) => {
                if ( !tipodocumento )
                {
                    return throwError('No se encontro un tipo de documento con el id ' + id + '!');
                }
                return of(tipodocumento);
            })
        );
    }

    /**
     * Create tipodocumento
     */
    createTipoDocumento(): Observable<TipoDocumento>
    {
        return this.tiposdocumentos$.pipe(
            take(1),
            switchMap(tiposdocumentos => this._httpClient.get<TipoDocumento>('https://localhost:44307/api/tipodocumento', {}).pipe(
                map((newTipoDocumento) => {
                    // Update the tiposdocumentos with the new tipodocumento
                    let nt = this.tipodocumentonew;
                    this._tipodocumentos.next([nt, ...tiposdocumentos]);
                    // Return the new tipodocumento
                    return nt;
                })
            ))
        );
    }

    createTipoDocumentoPost(nombre: string): Observable<TipoDocumento[]>
    {
        return this.tiposdocumentos$.pipe(
            take(1),
            switchMap(tiposdocumentos => this._httpClient.post<TipoDocumento>('https://localhost:44307/api/tipodocumento', {nombre: nombre}).pipe(
                switchMap(
                    tiposdocumentos => this.getTiposDocumentos().pipe(
                            tap((tiposdocumentos) => {
                            //this._pagination.next(response.pagination);
                            this._tipodocumentos.next(tiposdocumentos);
                            return tiposdocumentos;
                        })
                    )
                )
            ))
        );
    }

    /**
     * Update tipodocumento
     *
     * @param id
     * @param tipodocumento
     */
    updateTipoDocumento(id: string, tipodocumento: TipoDocumento): Observable<TipoDocumento>
    {
        return this.tiposdocumentos$.pipe(
            take(1),
            switchMap(tiposdocumentos => this._httpClient.put<TipoDocumento>('https://localhost:44307/api/TipoDocumento/' + id, {
                nombre: tipodocumento.nombre
            }).pipe(
                map((updatedTipoDocumento) => {
                    // Find the index of the updated tipodocumento
                    const index = tiposdocumentos.findIndex(item => item.idTipoDocumento.toString() == id);
                    // Update the tipodocumento
                    tiposdocumentos[index] = updatedTipoDocumento;
                    // Update the tipodocumento
                    this._tipodocumentos.next(tiposdocumentos);
                    // Return the updated tipodocumento
                    return updatedTipoDocumento;
                }),
                switchMap(updatedTipoDocumento => this.tipodocumento$.pipe(
                    take(1),
                    filter(item => item && item.idTipoDocumento.toString() == id),
                    tap(() => {
                        // Update the tipodocumento if it's selected
                        this._tipodocumento.next(updatedTipoDocumento);
                        // Return the updated tipodocumento
                        return updatedTipoDocumento;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the tipodocumento
     *
     * @param id
     */
    deleteProduct(id: string): Observable<boolean>
    {
        return this.tiposdocumentos$.pipe(
            take(1),
            switchMap(tiposdocumentos => this._httpClient.put('https://localhost:44307/api/tipodocumento/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {
                    // Find the index of the deleted tipodocumento
                    const index = tiposdocumentos.findIndex(item => item.idTipoDocumento.toString() == id);
                    // Delete the tipodocumento
                    tiposdocumentos.splice(index, 1);
                    // Update the tipodocumento
                    this._tipodocumentos.next(tiposdocumentos);
                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}