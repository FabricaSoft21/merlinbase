import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'tipodocumento',
    templateUrl    : './tipoDocumento.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipoDocumentoComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
