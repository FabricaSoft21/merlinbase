import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { TipoDocumento, TipoDocumentoPagination } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.types';
import { TipoDocumentoService } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.service';

@Component({
    selector       : 'tipodocumento-list',
    templateUrl    : './tipoDocumento.component.html',
    styles         : [
        /* language=SCSS */
        `
            .tipo-documento-grid {
                grid-template-columns: auto 40px;

                @screen sm {
                    grid-template-columns: auto 72px;
                }

                @screen md {
                    grid-template-columns: auto 72px;
                }

                @screen lg {
                    grid-template-columns: auto 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class TipoDocumentoListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    tiposdocumentos$: Observable<TipoDocumento[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: TipoDocumentoPagination;
    searchInputControl: FormControl = new FormControl();
    selectedTipoDocumento: TipoDocumento | null = null;
    selectedTipoDocumentoForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _tipoDocumentoService: TipoDocumentoService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedTipoDocumentoForm = this._formBuilder.group({
            idTipoDocumento    : [''],
            nombre             : ['', [Validators.required]]
        });

        // Get the pagination
        this._tipoDocumentoService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: TipoDocumentoPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.tiposdocumentos$ = this._tipoDocumentoService.tiposdocumentos$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoDocumentoService.getTiposDocumentos(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoDocumentoService.getTiposDocumentos(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(tipoDocumentoId: string): void
    {
        // If the product is already selected...
        if ( this.selectedTipoDocumento && this.selectedTipoDocumento.idTipoDocumento === tipoDocumentoId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._tipoDocumentoService.getTipoDocumentoId(tipoDocumentoId)
            .subscribe((tipodocumento) => {

                // Set the selected product
                this.selectedTipoDocumento = tipodocumento;

                // Fill the form
                this.selectedTipoDocumentoForm.patchValue(tipodocumento);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedTipoDocumento = null;
    }

    /**
     * Create tipoDocumento
     */
    createTipoDocumento(): void
    {
        // Create the tipoDocumento
        this._tipoDocumentoService.createTipoDocumento().subscribe((newTipoDocumento) => {

            // Go to new tipoDocumento
            this.selectedTipoDocumento = newTipoDocumento;

            // Fill the form
            this.selectedTipoDocumentoForm.patchValue(newTipoDocumento);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedTipoDocumento(): void
    {
        // Get the product object
        const tipodocumento = this.selectedTipoDocumentoForm.getRawValue();

        if(tipodocumento.idTipoDocumento == "-1"){
            this._tipoDocumentoService.createTipoDocumentoPost(tipodocumento.nombre).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.tiposdocumentos$ = this._tipoDocumentoService.tiposdocumentos$;
            this.selectedTipoDocumento = null;
        } else{
            // Update the product on the server
            this._tipoDocumentoService.updateTipoDocumento(tipodocumento.idTipoDocumento, tipodocumento).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedTipoDocumento(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Tipo de Documento',
            message: 'Esta seguro que quiere eliminar el tipo de documento?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const tipodocumento = this.selectedTipoDocumentoForm.getRawValue();

                // Delete the product on the server
                this._tipoDocumentoService.deleteProduct(tipodocumento.idTipoDocumento).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idTipoDocumento || index;
    }
}
