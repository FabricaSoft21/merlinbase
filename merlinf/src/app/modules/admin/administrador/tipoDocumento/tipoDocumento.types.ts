export interface TipoDocumento
{
    idTipoDocumento: string,
    nombre: string
}

export interface TipoDocumentoPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
