import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { CoordinacionAcademica, CoordinacionAcademicaPagination } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.types';

@Injectable({
    providedIn: 'root'
})
export class CoordinacionAcademicaService
{
    // Private
    private _pagination: BehaviorSubject<CoordinacionAcademicaPagination | null> = new BehaviorSubject(null);
    private _coordinacionAcademica: BehaviorSubject<CoordinacionAcademica | null> = new BehaviorSubject(null);
    private _coordinacionAcademicas: BehaviorSubject<CoordinacionAcademica[] | null> = new BehaviorSubject(null);

    private coordinacionAcademicanew: CoordinacionAcademica = 
    {
        idCoordinacionAcademica: "-1",
        numDocumento: "",
        nombre: "",
        apellido: "",
        email: "",
        telefono: "",
        tipoDocumentoId: "",
        nombreTipoDocumento: ""
    };

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<CoordinacionAcademicaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get coordinacionAcademica$(): Observable<CoordinacionAcademica>
    {
        return this._coordinacionAcademica.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get coordinacionesacademicas$(): Observable<CoordinacionAcademica[]>
    {
        return this._coordinacionAcademicas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getCoordinacionesAcademicas(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      //  Observable<{ /*pagination: CoordinacionAcademicaPagination;*/ tiposdocumentos: CoordinacionAcademica[] }>
      Observable<CoordinacionAcademica[]>
    {
        return this._httpClient.get<CoordinacionAcademica[]>('https://localhost:44307/api/CoordinacionAcademica', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._coordinacionAcademicas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getCoordinacionAcademicaId(id: string): Observable<CoordinacionAcademica>
    {
        return this._coordinacionAcademicas.pipe(
            take(1),
            map((coordinacionesacademicas) => {

                // Find the product
                const coordinacionAcademica = coordinacionesacademicas.find(item => item.idCoordinacionAcademica.toString() == id) || null;

                // Update the product
                this._coordinacionAcademica.next(coordinacionAcademica);

                // Return the product
                return coordinacionAcademica;
            }),
            switchMap((coordinacionAcademica) => {

                if ( !coordinacionAcademica )
                {
                    return throwError('No se encontro la coordinación académica con el id ' + id + '!');
                }

                return of(coordinacionAcademica);
            })
        );
    }

    /**
     * Create product
     */
    createCoordinacionAcademica(): Observable<CoordinacionAcademica>
    {
        return this.coordinacionesacademicas$.pipe(
            take(1),
            switchMap(coordinacionesacademicas => this._httpClient.get<CoordinacionAcademica>('https://localhost:44307/api/CoordinacionAcademica', {}).pipe(
                map((newCoordinacionAcademica) => {

                    // Update the tiposdocumentos with the new product
                    let nt = this.coordinacionAcademicanew;
                    this._coordinacionAcademicas.next([nt, ...coordinacionesacademicas]);
                    // Return the new product
                    return nt;
                })
            ))
        );
    }



    createCoordinacionAcademicaPost(persona): Observable<CoordinacionAcademica[]>
    {
        return this.coordinacionesacademicas$.pipe(
            take(1),
            switchMap(coordinacionesacademicas => this._httpClient.post<CoordinacionAcademica>('https://localhost:44307/api/CoordinacionAcademica',
            {
                numDocumento: persona.numDocumento,
                nombre: persona.nombre,
                apellido: persona.apellido,
                email: persona.email,
                telefono: persona.telefono,
                tipoDocumentoId: persona.tipoDocumentoId,
                nombreTipoDocumento: persona.nombreTipoDocumento
            }).pipe(
                switchMap(
                    coordinacionesacademicas => this.getCoordinacionesAcademicas().pipe(
                            tap((coordinacionesacademicas) => {
                            //this._pagination.next(response.pagination);
                            this._coordinacionAcademicas.next(coordinacionesacademicas);
                            return coordinacionesacademicas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param coordinacionAcademica
     */
    updateCoordinacionAcademica(id: string, coordinacionAcademica: CoordinacionAcademica): Observable<CoordinacionAcademica>
    {
        return this.coordinacionesacademicas$.pipe(
            take(1),
            switchMap(coordinacionesacademicas => this._httpClient.put<CoordinacionAcademica>('https://localhost:44307/api/CoordinacionAcademica/' + id, {
                numDocumento: coordinacionAcademica.numDocumento,
                nombre: coordinacionAcademica.nombre,
                apellido: coordinacionAcademica.apellido,
                email: coordinacionAcademica.email,
                telefono: coordinacionAcademica.telefono,
                tipoDocumentoId: coordinacionAcademica.tipoDocumentoId,
                nombreTipoDocumento: coordinacionAcademica.nombreTipoDocumento,
            }).pipe(
                map((updatedCoordinacionAcademica) => {
                    console.log(updatedCoordinacionAcademica);//Aqui se esta depurando

                    // Find the index of the updated product
                    const index = coordinacionesacademicas.findIndex(item => item.idCoordinacionAcademica.toString() == id);

                    // Update the product
                    coordinacionesacademicas[index] = updatedCoordinacionAcademica;
                    

                    // Update the tiposdocumentos
                    this._coordinacionAcademicas.next(coordinacionesacademicas);

                    // Return the updated product
                    return updatedCoordinacionAcademica;
                }),
                switchMap(updatedCoordinacionAcademica => this.coordinacionAcademica$.pipe(
                    take(1),
                    filter(item => item && item.idCoordinacionAcademica.toString() == id),
                    tap(() => {

                        // Update the product if it's selected
                        this._coordinacionAcademica.next(updatedCoordinacionAcademica);

                        // Return the updated product
                        return updatedCoordinacionAcademica;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteCoordinacionAcademica(id: string): Observable<boolean>
    {
        return this.coordinacionesacademicas$.pipe(
            take(1),
            switchMap(coordinacionesacademicas => this._httpClient.put('https://localhost:44307/api/CoordinacionAcademica/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = coordinacionesacademicas.findIndex(item => item.idCoordinacionAcademica.toString() == id);

                    // Delete the product
                    coordinacionesacademicas.splice(index, 1);

                    // Update the tiposdocumentos
                    this._coordinacionAcademicas.next(coordinacionesacademicas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
