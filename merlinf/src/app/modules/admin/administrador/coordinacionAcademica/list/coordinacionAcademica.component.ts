import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';//Tarea
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { CoordinacionAcademica, CoordinacionAcademicaPagination } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.types';
import { CoordinacionAcademicaService } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.service';
import { TipoDocumento } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.types';
import { TipoDocumentoService } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.service';

@Component({
    selector       : 'coordinacionacademica-list',
    templateUrl    : './coordinacionAcademica.component.html',
    styles         : [
        /* language=SCSS */
        `
            .coordinacion-academica-grid {
                grid-template-columns: 190px 190px auto;

                @screen sm {
                    grid-template-columns: 190px 190px 190px auto;
                }

                @screen md {
                    grid-template-columns: 190px 190px 190px 190px auto;
                }

                @screen lg {
                    grid-template-columns: 250px 250px 250px 250px 250px auto;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class coordinacionAcademicaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    coordinacionesacademicas$: Observable<CoordinacionAcademica[]>;
    tiposdocumentos: TipoDocumento[];

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: CoordinacionAcademicaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedCoordinacionAcademica: CoordinacionAcademica | null = null;
    selectedCoordinacionAcademicaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _coordinacionAcademicaService: CoordinacionAcademicaService,
        private _tipoDocumentoService: TipoDocumentoService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedCoordinacionAcademicaForm = this._formBuilder.group({
            idCoordinacionAcademica    : [''],
            numDocumento   : ['', [Validators.required]],
            nombre         : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(45)]],
            apellido       : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(45)]],
            email          : ['', [Validators.required, Validators.email]],
            telefono       : ['', [Validators.required]],
            tipoDocumentoId: ['', [Validators.required]],
            nombreTipoDocumento: ['', [Validators.required]],
            
        });

        this._tipoDocumentoService.tiposdocumentos$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tiposdocumentos: TipoDocumento[]) => {

                // Update the categories
                this.tiposdocumentos = tiposdocumentos;
                console.log(this.tiposdocumentos);

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the pagination
        this._coordinacionAcademicaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: CoordinacionAcademicaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.coordinacionesacademicas$ = this._coordinacionAcademicaService.coordinacionesacademicas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._coordinacionAcademicaService.getCoordinacionesAcademicas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._coordinacionAcademicaService.getCoordinacionesAcademicas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(coordinacionAcademicaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedCoordinacionAcademica && this.selectedCoordinacionAcademica.idCoordinacionAcademica === coordinacionAcademicaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._coordinacionAcademicaService.getCoordinacionAcademicaId(coordinacionAcademicaId)
            .subscribe((coordinacionacademica) => {

                // Set the selected product
                this.selectedCoordinacionAcademica = coordinacionacademica;

                // Fill the form
                this.selectedCoordinacionAcademicaForm.patchValue(coordinacionacademica);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedCoordinacionAcademica = null;
    }

    /**
     * Create tipoDocumento
     */
    createCoordinacionAcademica(): void
    {
        // Create the tipoDocumento
        this._coordinacionAcademicaService.createCoordinacionAcademica().subscribe((newCoordinacionAcademica) => {

            // Go to new tipoDocumento
            this.selectedCoordinacionAcademica = newCoordinacionAcademica;

            // Fill the form
            this.selectedCoordinacionAcademicaForm.patchValue(newCoordinacionAcademica);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedCoordinacionAcademica(): void
    {
        // Get the product object
        const coordinacionAcademica = this.selectedCoordinacionAcademicaForm.getRawValue();

        if(coordinacionAcademica.idCoordinacionAcademica == "-1"){
            this._coordinacionAcademicaService.createCoordinacionAcademicaPost(coordinacionAcademica).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.coordinacionesacademicas$ = this._coordinacionAcademicaService.coordinacionesacademicas$;
            this.selectedCoordinacionAcademica = null;
        } else{
            // Update the product on the server
            this._coordinacionAcademicaService.updateCoordinacionAcademica(coordinacionAcademica.idCoordinacionAcademica, coordinacionAcademica).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedCoordinacionAcademica(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Coordinacón Académica',
            message: 'Esta seguro que quiere eliminar la coordinación académica?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const coordinacionAcademica = this.selectedCoordinacionAcademicaForm.getRawValue();

                // Delete the product on the server
                this._coordinacionAcademicaService.deleteCoordinacionAcademica(coordinacionAcademica.idCoordinacionAcademica).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idCoordinacionAcademica || index;
    }
}
