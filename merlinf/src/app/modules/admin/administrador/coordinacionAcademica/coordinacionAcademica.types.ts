export interface CoordinacionAcademica
{
    idCoordinacionAcademica: string,
    numDocumento: string,
    nombre: string,
    apellido: string,
    email: string,
    telefono: string,
    tipoDocumentoId: string,
    nombreTipoDocumento: string
}

export interface CoordinacionAcademicaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
