import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'app/shared/shared.module';
import { TipoDocumentoComponent } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.component';
import { TipoDocumentoListComponent } from 'app/modules/admin/administrador/tipoDocumento/list/tipoDocumento.component';
import { administradorRoutes } from 'app/modules/admin/administrador/administrador.routing';
import { coordinacionAcademicaComponent } from './coordinacionAcademica/coordinacionAcademica.component';
import { coordinacionAcademicaListComponent } from './coordinacionAcademica/list/coordinacionAcademica.component';

@NgModule({
    declarations: [
        TipoDocumentoComponent,
        TipoDocumentoListComponent,
        coordinacionAcademicaComponent,
        coordinacionAcademicaListComponent
    ],
    imports     : [
        RouterModule.forChild(administradorRoutes),
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSortModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatTooltipModule,
        SharedModule
    ]
})
export class AdministradorModule
{
}
