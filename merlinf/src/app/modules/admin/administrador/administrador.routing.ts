import { Route } from '@angular/router';
import { TipoDocumentoComponent } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.component';
import { TipoDocumentoListComponent } from 'app/modules/admin/administrador/tipoDocumento/list/tipoDocumento.component';
import { TiposDocumentosResolver } from 'app/modules/admin/administrador/tipoDocumento/tipoDocumento.resolvers';
import { coordinacionAcademicaComponent } from './coordinacionAcademica/coordinacionAcademica.component';
import { coordinacionAcademicaListComponent } from './coordinacionAcademica/list/coordinacionAcademica.component';
import { CoordinacionesAcademicasResolver } from './coordinacionAcademica/coordinacionAcademica.resolvers';

export const administradorRoutes: Route[] = [
    {
        path      : '',
        pathMatch : 'full',
        redirectTo: 'tipo-documento'
    },
    {
        path     : 'tipo-documento',
        component: TipoDocumentoComponent,
        children : [
            {
                path     : '',
                component: TipoDocumentoListComponent,
                resolve  : {
                    tiposdocumentos  : TiposDocumentosResolver
                }
            }
        ]
    },
    {
        path     : 'coordinacion-academica',
        component: coordinacionAcademicaComponent,
        children : [
            {
                path     : '',
                component: coordinacionAcademicaListComponent,
                resolve  : {
                    coordinacionesacademicas  : CoordinacionesAcademicasResolver,
                    tiposdocumentos  : TiposDocumentosResolver
                }
            }
        ]
    }
];
