import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Ficha, FichaPagination } from 'app/modules/admin/coordinacion/Ficha/ficha.types';

@Injectable({
    providedIn: 'root'
})
export class FichaService
{
    // Private
    private _pagination: BehaviorSubject<FichaPagination | null> = new BehaviorSubject(null);
    private _ficha: BehaviorSubject<Ficha | null> = new BehaviorSubject(null);
    private _fichas: BehaviorSubject<Ficha[] | null> = new BehaviorSubject(null);

    private fichanew: Ficha = 
    {
        idFicha: "-1",
        codigo: "",
        programaId: "",
        nombrePrograma: ""
    };

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<FichaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get ficha$(): Observable<Ficha>
    {
        return this._ficha.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get fichas$(): Observable<Ficha[]>
    {
        return this._fichas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getFichas(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      //  Observable<{ /*pagination: CoordinacionAcademicaPagination;*/ tiposdocumentos: CoordinacionAcademica[] }>
      Observable<Ficha[]>
    {
        return this._httpClient.get<Ficha[]>('https://localhost:44307/api/Ficha', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._fichas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getFichaId(id: string): Observable<Ficha>
    {
        return this._fichas.pipe(
            take(1),
            map((fichas) => {

                // Find the product
                const ficha = fichas.find(item => item.idFicha.toString() == id) || null;

                // Update the product
                this._ficha.next(ficha);

                // Return the product
                return ficha;
            }),
            switchMap((ficha) => {

                if ( !ficha )
                {
                    return throwError('No se encontro la ficha con el id ' + id + '!');
                }

                return of(ficha);
            })
        );
    }

    /**
     * Create product
     */
    createFicha(): Observable<Ficha>
    {
        return this.fichas$.pipe(
            take(1),
            switchMap(fichas => this._httpClient.get<Ficha>('https://localhost:44307/api/Ficha', {}).pipe(
                map((newFicha) => {

                    // Update the tiposdocumentos with the new product
                    let nt = this.fichanew;
                    this._fichas.next([nt, ...fichas]);
                    // Return the new product
                    return nt;
                })
            ))
        );
    }



    createFichaPost(persona): Observable<Ficha[]>
    {
        return this.fichas$.pipe(
            take(1),
            switchMap(fichas => this._httpClient.post<Ficha>('https://localhost:44307/api/Ficha',
            {
                idFicha: persona.idFicha,
                codigo: persona.codigo,
                programaId: persona.programaId,
                nombrePrograma: persona.nombrePrograma
            }).pipe(
                switchMap(
                    fichas => this.getFichas().pipe(
                            tap((fichas) => {
                            //this._pagination.next(response.pagination);
                            this._fichas.next(fichas);
                            return fichas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param ficha
     */
    updateFicha(id: string, ficha: Ficha): Observable<Ficha>
    {
        return this.fichas$.pipe(
            take(1),
            switchMap(fichas => this._httpClient.put<Ficha>('https://localhost:44307/api/Ficha/' + id, {
                idFicha: ficha.idFicha,
                codigo: ficha.codigo,
                programaId: ficha.programaId,
                nombrePrograma: ficha.nombrePrograma
            }).pipe(
                map((updatedFicha) => {

                    // Find the index of the updated product
                    const index = fichas.findIndex(item => item.idFicha.toString() == id);

                    // Update the product
                    fichas[index] = updatedFicha;
                    

                    // Update the tiposdocumentos
                    this._fichas.next(fichas);

                    // Return the updated product
                    return updatedFicha;
                }),
                switchMap(updatedFicha => this.ficha$.pipe(
                    take(1),
                    filter(item => item && item.idFicha.toString() == id),
                    tap(() => {

                        // Update the product if it's selected
                        this._ficha.next(updatedFicha);

                        // Return the updated product
                        return updatedFicha;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteFicha(id: string): Observable<boolean>
    {
        return this.fichas$.pipe(
            take(1),
            switchMap(fichas => this._httpClient.put('https://localhost:44307/api/Ficha/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = fichas.findIndex(item => item.idFicha.toString() == id);

                    // Delete the product
                    fichas.splice(index, 1);

                    // Update the tiposdocumentos
                    this._fichas.next(fichas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
