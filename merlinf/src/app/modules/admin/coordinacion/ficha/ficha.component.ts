import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'ficha',
    templateUrl    : './ficha.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class fichaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
