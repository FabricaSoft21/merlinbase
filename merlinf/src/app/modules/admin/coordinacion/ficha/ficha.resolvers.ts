import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FichaService } from 'app/modules/admin/coordinacion/Ficha/ficha.service';
import { Ficha, FichaPagination } from 'app/modules/admin/coordinacion/Ficha/ficha.types';



@Injectable({
    providedIn: 'root'
})
export class FichaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _fichaService: FichaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Ficha>
    {
        return this._fichaService.getFichaId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class FichasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _fichaService: FichaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: TipoDocumentoPagination; products: TipoDocumento[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Ficha[]>
    {
        return this._fichaService.getFichas();
    }
}
