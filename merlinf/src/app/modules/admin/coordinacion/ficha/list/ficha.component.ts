import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';//Tarea
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { Ficha, FichaPagination } from 'app/modules/admin/coordinacion/Ficha/ficha.types';
import { FichaService } from 'app/modules/admin/coordinacion/Ficha/ficha.service';
import { Programa } from 'app/modules/admin/coordinacion/programa/programa.types';
import { ProgramaService } from 'app/modules/admin/coordinacion/programa/programa.service';

@Component({
    selector       : 'ficha-list',
    templateUrl    : './ficha.component.html',
    styles         : [
        /* language=SCSS */
        `
            .ficha-grid {
                grid-template-columns: 190px 190px auto;

                @screen sm {
                    grid-template-columns: 190px 190px 190px auto;
                }

                @screen md {
                    grid-template-columns: 190px 190px auto;
                }

                @screen lg {
                    grid-template-columns: 300px 300px auto;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class fichaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    fichas$: Observable<Ficha[]>;
    programas: Programa[];

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: FichaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedFicha: Ficha | null = null;
    selectedFichaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _fichaService: FichaService,
        private _programaService: ProgramaService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedFichaForm = this._formBuilder.group({
            idFicha    : [''],
            codigo   : ['', [Validators.required]],
            programaId: ['', [Validators.required]],
            nombrePrograma: ['', [Validators.required]],
        });

        this._programaService.programas$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((programas: Programa[]) => {

                // Update the categories
                this.programas = programas;
                console.log(this.programas);

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the pagination
        this._fichaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: FichaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.fichas$ = this._fichaService.fichas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._fichaService.getFichas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._fichaService.getFichas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(fichaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedFicha && this.selectedFicha.idFicha === fichaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._fichaService.getFichaId(fichaId)
            .subscribe((ficha) => {

                // Set the selected product
                this.selectedFicha = ficha;

                // Fill the form
                this.selectedFichaForm.patchValue(ficha);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedFicha = null;
    }

    /**
     * Create tipoDocumento
     */
    createFicha(): void
    {
        // Create the tipoDocumento
        this._fichaService.createFicha().subscribe((newFicha) => {

            // Go to new tipoDocumento
            this.selectedFicha = newFicha;

            // Fill the form
            this.selectedFichaForm.patchValue(newFicha);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedFicha(): void
    {
        // Get the product object
        const ficha = this.selectedFichaForm.getRawValue();

        if(ficha.idFicha == "-1"){
            this._fichaService.createFichaPost(ficha).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.fichas$ = this._fichaService.fichas$;
            this.selectedFicha = null;
        } else{
            // Update the product on the server
            this._fichaService.updateFicha(ficha.idFicha, ficha).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedFicha(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Ficha',
            message: 'Esta seguro que quiere eliminar la ficha?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const ficha = this.selectedFichaForm.getRawValue();

                // Delete the product on the server
                this._fichaService.deleteFicha(ficha.idFicha).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idFicha || index;
    }
}
