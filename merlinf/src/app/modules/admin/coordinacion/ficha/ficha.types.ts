export interface Ficha
{
    idFicha: string,
    codigo: string,
    programaId: string,
    nombrePrograma: string
}

export interface FichaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
