import { Route } from '@angular/router';
import { ModalidadComponent } from './modalidad/modalidad.component';
import { ModalidadListComponent } from './modalidad/list/modalidad.component';
import { ModalidadesResolver } from './modalidad/modalidad.resolvers';
import { TipoProgramaComponent } from './tipoPrograma/tipoPrograma.component';
import { TipoProgramaListComponent } from './tipoPrograma/list/tipoPrograma.component';
import { TipoProgramasResolver } from './tipoPrograma/tipoPrograma.resolvers';
import { EstadoQuejaComponent } from './estadoQueja/estadoQueja.component';
import { EstadoQuejaListComponent } from './estadoQueja/list/estadoQueja.component';
import { EstadoQuejasResolver } from './estadoQueja/estadoQueja.resolvers';
import { TipoQuejaComponent } from './tipoQueja/tipoQueja.component';
import { TipoQuejaListComponent } from './tipoQueja/list/tipoQueja.component';
import { TiposQuejasResolver } from './tipoQueja/tipoQueja.resolvers';
import { programaComponent } from './programa/programa.component';
import { programaListComponent } from './programa/list/programa.component';
import { ProgramasResolver } from './programa/programa.resolvers';
import { CoordinacionesAcademicasResolver } from '../administrador/coordinacionAcademica/coordinacionAcademica.resolvers';
import { fichaComponent } from './Ficha/ficha.component';
import { fichaListComponent } from './Ficha/list/ficha.component';
import { FichasResolver } from './Ficha/ficha.resolvers';

export const coordinacionRoutes: Route[] = [
    {
        path      : '',
        pathMatch : 'full',
        redirectTo: 'modalidad'
    },
    {
        path     : 'modalidad',
        component: ModalidadComponent,
        children : [
            {
                path     : '',
                component: ModalidadListComponent,
                resolve  : {
                    modalidades  : ModalidadesResolver
                }
            }
        ]
        /*children : [
            {
                path     : '',
                component: ContactsListComponent,
                resolve  : {
                    tasks    : ContactsResolver,
                    countries: ContactsCountriesResolver
                },
                children : [
                    {
                        path         : ':id',
                        component    : ContactsDetailsComponent,
                        resolve      : {
                            task     : ContactsContactResolver,
                            countries: ContactsCountriesResolver
                        },
                        canDeactivate: [CanDeactivateContactsDetails]
                    }
                ]
            }
        ]*/
    },
    {
        path     : 'tipo-programa',
        component: TipoProgramaComponent,
        children : [
            {
                path     : '',
                component: TipoProgramaListComponent,
                resolve  : {
                    tiposprogramas  : TipoProgramasResolver
                }
            }
        ]
    },
    {
        path     : 'estado-queja',
        component: EstadoQuejaComponent,
        children : [
            {
                path     : '',
                component: EstadoQuejaListComponent,
                resolve  : {
                    estadosquejas  : EstadoQuejasResolver
                }
            }
        ]
    },
    {
        path     : 'tipo-queja',
        component: TipoQuejaComponent,
        children : [
            {
                path     : '',
                component: TipoQuejaListComponent,
                resolve  : {
                    tiposquejas  : TiposQuejasResolver
                }
            }
        ]
    },
    {
        path     : 'programa',
        component: programaComponent,
        children : [
            {
                path     : '',
                component: programaListComponent,
                resolve  : {
                    programas  : ProgramasResolver,
                    coordinacionesacademicas: CoordinacionesAcademicasResolver,
                    tiposprogramas  : TipoProgramasResolver,
                    modalidades  : ModalidadesResolver
                }
            }
        ]
    },
    {
        path     : 'ficha',
        component: fichaComponent,
        children : [
            {
                path     : '',
                component: fichaListComponent,
                resolve  : {
                    fichas  : FichasResolver,
                    programas: ProgramasResolver,
                }
            }
        ]
    },
];
