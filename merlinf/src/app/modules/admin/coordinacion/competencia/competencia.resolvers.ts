import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CoordinacionAcademicaService } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.service';
import { CoordinacionAcademica, CoordinacionAcademicaPagination } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.types';



@Injectable({
    providedIn: 'root'
})
export class CoordinacionAcademicaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _coordinacionAcademicaService: CoordinacionAcademicaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CoordinacionAcademica>
    {
        return this._coordinacionAcademicaService.getCoordinacionAcademicaId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class CoordinacionesAcademicasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _coordinacionAcademicaService: CoordinacionAcademicaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: TipoDocumentoPagination; products: TipoDocumento[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CoordinacionAcademica[]>
    {
        return this._coordinacionAcademicaService.getCoordinacionesAcademicas();
    }
}
