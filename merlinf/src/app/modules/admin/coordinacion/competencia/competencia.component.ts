import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'coordinacionacademica',
    templateUrl    : './coordinacionAcademica.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class coordinacionAcademicaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
