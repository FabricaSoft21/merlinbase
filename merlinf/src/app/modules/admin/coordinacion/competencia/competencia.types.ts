export interface Competencia
{
    idCompetencia: string,
    nombre: string,
    programasIds: number[],
    nombresProgramas: string[],
}

export interface CompetenciaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
