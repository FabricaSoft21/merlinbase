import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { Modalidad, ModalidadPagination } from 'app/modules/admin/coordinacion/modalidad/modalidad.types';
import { ModalidadService } from 'app/modules/admin/coordinacion/modalidad/modalidad.service';

@Component({
    selector       : 'modalidad-list',
    templateUrl    : './modalidad.component.html',
    styles         : [
        /* language=SCSS */
        `
            .inventory-grid {
                grid-template-columns: auto 40px;

                @screen sm {
                    grid-template-columns: auto 72px;
                }

                @screen md {
                    grid-template-columns: auto 72px;
                }

                @screen lg {
                    grid-template-columns: auto 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class ModalidadListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    modalidades$: Observable<Modalidad[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: ModalidadPagination;
    searchInputControl: FormControl = new FormControl();
    selectedModalidad: Modalidad | null = null;
    selectedModalidadForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _modalidadService: ModalidadService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected product form
        this.selectedModalidadForm = this._formBuilder.group({
            idModalidad: [''],
            nombre     : ['', [Validators.required]]
        });

        // Get the pagination
        this._modalidadService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: ModalidadPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the products
        this.modalidades$ = this._modalidadService.modalidades$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._modalidadService.getModalidades(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._modalidadService.getModalidades(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(modalidadId: string): void
    {
        // If the product is already selected...
        if ( this.selectedModalidad && this.selectedModalidad.idModalidad === modalidadId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._modalidadService.getModalidadById(modalidadId)
            .subscribe((modalidad) => {

                // Set the selected product
                this.selectedModalidad = modalidad;

                // Fill the form
                this.selectedModalidadForm.patchValue(modalidad);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedModalidad = null;
    }

    /**
     * Create product
     */
    createModalidad(): void
    {
        // Create the product
        this._modalidadService.createModalidad().subscribe((newModalidad) => {

            // Go to new product
            this.selectedModalidad = newModalidad;

            // Fill the form
            this.selectedModalidadForm.patchValue(newModalidad);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedModalidad(): void
    {
        // Get the product object
        const modalidad = this.selectedModalidadForm.getRawValue();

        if(modalidad.idModalidad == "-1"){
            this._modalidadService.createModalidadPost(modalidad.nombre).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.modalidades$ = this._modalidadService.modalidades$;
            this.selectedModalidad = null;
        } else{
            // Update the product on the server
            this._modalidadService.updateModalidad(modalidad.idModalidad, modalidad).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
     deleteSelectedModalidad(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Modalidad',
            message: 'Esta seguro que quiere eliminar la modalidad?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const modalidad = this.selectedModalidadForm.getRawValue();

                // Delete the product on the server
                this._modalidadService.deleteModalidad(modalidad.idModalidad).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idModalidad || index;
    }
}
