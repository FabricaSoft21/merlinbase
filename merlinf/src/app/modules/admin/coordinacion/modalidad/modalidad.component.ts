import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'modalidad',
    templateUrl    : './modalidad.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalidadComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
