export interface Modalidad
{
    idModalidad: string;
    nombre: string;
}

export interface ModalidadPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
