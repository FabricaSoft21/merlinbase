import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ModalidadService } from 'app/modules/admin/coordinacion/modalidad/modalidad.service';
import { Modalidad, ModalidadPagination } from 'app/modules/admin/coordinacion/modalidad/modalidad.types';


@Injectable({
    providedIn: 'root'
})
export class ModalidadResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _modalidadService: ModalidadService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Modalidad>
    {
        return this._modalidadService.getModalidadById(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class ModalidadesResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _modalidadService: ModalidadService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: ModalidadPagination; products: Modalidad[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Modalidad[]>
    {
        return this._modalidadService.getModalidades();
    }
}
