import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Modalidad, ModalidadPagination } from 'app/modules/admin/coordinacion/modalidad/modalidad.types';

@Injectable({
    providedIn: 'root'
})
export class ModalidadService
{
    // Private
    private _pagination: BehaviorSubject<ModalidadPagination | null> = new BehaviorSubject(null);
    private _modalidad: BehaviorSubject<Modalidad | null> = new BehaviorSubject(null);
    private _modalidades: BehaviorSubject<Modalidad[] | null> = new BehaviorSubject(null);

    private modalidadnew: Modalidad = {idModalidad: "-1", nombre: "Nueva Modalidad"};

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for pagination
     */
    get pagination$(): Observable<ModalidadPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get modalidad$(): Observable<Modalidad>
    {
        return this._modalidad.asObservable();
    }

    /**
     * Getter for products
     */
    get modalidades$(): Observable<Modalidad[]>
    {
        return this._modalidades.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get products
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getModalidades(page: number = 0, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
        //Observable<{ pagination: InventoryPagination; products: InventoryProduct[] }>
        Observable<Modalidad[]>
    {
        return this._httpClient.get<Modalidad[]>('https://localhost:44307/api/modalidad', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._modalidades.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getModalidadById(id: string): Observable<Modalidad>
    {
        return this._modalidades.pipe(
            take(1),
            map((modalidades) => {

                // Find the product
                const modalidad = modalidades.find(item => item.idModalidad.toString() == id) || null;

                // Update the product
                this._modalidad.next(modalidad);

                // Return the product
                return modalidad;
            }),
            switchMap((modalidad) => {

                if ( !modalidad )
                {
                    return throwError('No se encontro la modalidad con el id ' + id + '!');
                }

                return of(modalidad);
            })
        );
    }

    /**
     * Create product
     */
    createModalidad(): Observable<Modalidad>
    {
        return this.modalidades$.pipe(
            take(1),
            switchMap(modalidades => this._httpClient.get<Modalidad>('https://localhost:44307/api/modalidad', {}).pipe(
                map((newModalidad) => {

                    // Update the products with the new product

                    let nm = this.modalidadnew;

                    this._modalidades.next([nm, ...modalidades]);

                    // Return the new product
                    return nm;
                })
            ))
        );
    }

    createModalidadPost(nombre: string): Observable<Modalidad[]>
    {
        return this.modalidades$.pipe(
            take(1),
            switchMap(modalidades => this._httpClient.post<Modalidad>('https://localhost:44307/api/modalidad', {nombre: nombre}).pipe(
                /*map((newTipoDocumento) => {
                    // Update the product
                    //tiposdocumentos[-1] = updatedTipoDocumento;

                    // Return the updated product
                    return newTipoDocumento;
                }),*/
                switchMap(
                    modalidades => this.getModalidades().pipe(
                            tap((modalidades) => {
                            //this._pagination.next(response.pagination);
                            this._modalidades.next(modalidades);
                            return modalidades;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param modalidad
     */
     updateModalidad(id: string, modalidad: Modalidad): Observable<Modalidad>
     {
         return this.modalidades$.pipe(
             take(1),
             switchMap(modalidades => this._httpClient.put<Modalidad>('https://localhost:44307/api/modalidad/' + id, {
                 nombre: modalidad.nombre
             }).pipe(
                 map((updatedModalidad) => {

                     // Find the index of the updated product
                     const index = modalidades.findIndex(item => item.idModalidad.toString() == id);

                     // Update the product
                     modalidades[index] = updatedModalidad;

                     // Update the tiposdocumentos
                     this._modalidades.next(modalidades);

                     // Return the updated product
                     return updatedModalidad;
                 }),
                 switchMap(updatedModalidad => this.modalidad$.pipe(
                     take(1),
                     filter(item => item && item.idModalidad.toString() == id),
                     tap(() => {

                         // Update the product if it's selected
                         this._modalidad.next(updatedModalidad);

                         // Return the updated product
                         return updatedModalidad;
                     })
                 ))
             ))
         );
     }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteModalidad(id: string): Observable<boolean>
    {
        return this.modalidades$.pipe(
            take(1),
            switchMap(modalidades => this._httpClient.put('https://localhost:44307/api/modalidad/statechange/' + id, {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = modalidades.findIndex(item => item.idModalidad.toString() == id);

                    // Delete the product
                    modalidades.splice(index, 1);

                    // Update the products
                    this._modalidades.next(modalidades);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
