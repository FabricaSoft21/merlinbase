import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'tipoprograma',
    templateUrl    : './tipoPrograma.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipoProgramaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
