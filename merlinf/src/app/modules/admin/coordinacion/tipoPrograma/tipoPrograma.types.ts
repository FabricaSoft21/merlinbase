export interface TipoPrograma
{
    idTipoPrograma: string;
    nombre: string;
}

export interface TipoProgramaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
