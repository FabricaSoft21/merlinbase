import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TipoProgramaService } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.service';
import { TipoPrograma, TipoProgramaPagination } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.types';


@Injectable({
    providedIn: 'root'
})
export class TipoProgramaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _tipoProgramaService: TipoProgramaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoPrograma>
    {
        return this._tipoProgramaService.getTipoProgramaById(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class TipoProgramasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _tipoProgramaService: TipoProgramaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: ModalidadPagination; products: Modalidad[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoPrograma[]>
    {
        return this._tipoProgramaService.getTipoProgramas();
    }
}
