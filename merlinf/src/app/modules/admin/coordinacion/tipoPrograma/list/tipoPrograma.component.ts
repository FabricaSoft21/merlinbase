import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { TipoPrograma, TipoProgramaPagination } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.types';
import { TipoProgramaService } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.service';

@Component({
    selector       : 'tipoprograma-list',
    templateUrl    : './tipoPrograma.component.html',
    styles         : [
        /* language=SCSS */
        `
            .inventory-grid {
                grid-template-columns: auto 40px;

                @screen sm {
                    grid-template-columns: auto 72px;
                }

                @screen md {
                    grid-template-columns: auto 72px;
                }

                @screen lg {
                    grid-template-columns: auto 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class TipoProgramaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    tiposprogramas$: Observable<TipoPrograma[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: TipoProgramaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedTipoPrograma: TipoPrograma | null = null;
    selectedTipoProgramaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _tipoProgramaService: TipoProgramaService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected product form
        this.selectedTipoProgramaForm = this._formBuilder.group({
            idTipoPrograma: [''],
            nombre     : ['', [Validators.required]]
        });

        // Get the pagination
        this._tipoProgramaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: TipoProgramaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the products
        this.tiposprogramas$ = this._tipoProgramaService.tiposprogramas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoProgramaService.getTipoProgramas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoProgramaService.getTipoProgramas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(tipoProgramaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedTipoPrograma && this.selectedTipoPrograma.idTipoPrograma === tipoProgramaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._tipoProgramaService.getTipoProgramaById(tipoProgramaId)
            .subscribe((tipoprograma) => {

                // Set the selected product
                this.selectedTipoPrograma = tipoprograma;

                // Fill the form
                this.selectedTipoProgramaForm.patchValue(tipoprograma);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedTipoPrograma = null;
    }

    /**
     * Create product
     */
    createTipoPrograma(): void
    {
        // Create the product
        this._tipoProgramaService.createTipoPrograma().subscribe((newTipoPrograma) => {

            // Go to new product
            this.selectedTipoPrograma = newTipoPrograma;

            // Fill the form
            this.selectedTipoProgramaForm.patchValue(newTipoPrograma);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedTipoPrograma(): void
    {
        // Get the product object
        const tipoprograma = this.selectedTipoProgramaForm.getRawValue();

        if(tipoprograma.idTipoPrograma == "-1"){
            this._tipoProgramaService.createTipoProgramaPost(tipoprograma.nombre).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.tiposprogramas$ = this._tipoProgramaService.tiposprogramas$;
            this.selectedTipoPrograma = null;
        } else{
            // Update the product on the server
            this._tipoProgramaService.updateTipoPrograma(tipoprograma.idTipoPrograma, tipoprograma).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
     deleteSelectedTipoPrograma(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Tipo de Programa',
            message: 'Esta seguro que quiere eliminar el tipo de programa?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const tipoprograma = this.selectedTipoProgramaForm.getRawValue();

                // Delete the product on the server
                this._tipoProgramaService.deleteTipoPrograma(tipoprograma.idTipoPrograma).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idTipoPrograma || index;
    }
}
