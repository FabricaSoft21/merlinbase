import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { TipoPrograma, TipoProgramaPagination } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.types';

@Injectable({
    providedIn: 'root'
})
export class TipoProgramaService
{
    // Private
    private _pagination: BehaviorSubject<TipoProgramaPagination | null> = new BehaviorSubject(null);
    private _tipoprograma: BehaviorSubject<TipoPrograma | null> = new BehaviorSubject(null);
    private _tipoprogramas: BehaviorSubject<TipoPrograma[] | null> = new BehaviorSubject(null);

    private tipoprogramanew: TipoPrograma = {idTipoPrograma: "-1", nombre: "Nuevo Tipo Programa"};

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for pagination
     */
    get pagination$(): Observable<TipoProgramaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get tipoprograma$(): Observable<TipoPrograma>
    {
        return this._tipoprograma.asObservable();
    }

    /**
     * Getter for products
     */
    get tiposprogramas$(): Observable<TipoPrograma[]>
    {
        return this._tipoprogramas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get products
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getTipoProgramas(page: number = 0, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
        //Observable<{ pagination: InventoryPagination; products: InventoryProduct[] }>
        Observable<TipoPrograma[]>
    {
        return this._httpClient.get<TipoPrograma[]>('https://localhost:44307/api/TipoPrograma', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._tipoprogramas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getTipoProgramaById(id: string): Observable<TipoPrograma>
    {
        return this._tipoprogramas.pipe(
            take(1),
            map((tipoprogramas) => {

                // Find the product
                const tipoprograma = tipoprogramas.find(item => item.idTipoPrograma.toString() == id) || null;

                // Update the product
                this._tipoprograma.next(tipoprograma);

                // Return the product
                return tipoprograma;
            }),
            switchMap((tipoprograma) => {

                if ( !tipoprograma )
                {
                    return throwError('No se encontro el tipo de programa con el id ' + id + '!');
                }

                return of(tipoprograma);
            })
        );
    }

    /**
     * Create product
     */
    createTipoPrograma(): Observable<TipoPrograma>
    {
        return this.tiposprogramas$.pipe(
            take(1),
            switchMap(tiposprogramas => this._httpClient.get<TipoPrograma>('https://localhost:44307/api/TipoPrograma', {}).pipe(
                map((newTipoPrograma) => {

                    // Update the products with the new product

                    let nm = this.tipoprogramanew;

                    this._tipoprogramas.next([nm, ...tiposprogramas]);

                    // Return the new product
                    return nm;
                })
            ))
        );
    }

    createTipoProgramaPost(nombre: string): Observable<TipoPrograma[]>
    {
        return this.tiposprogramas$.pipe(
            take(1),
            switchMap(tiposprogramas => this._httpClient.post<TipoPrograma>('https://localhost:44307/api/TipoPrograma', {nombre: nombre}).pipe(
                /*map((newTipoDocumento) => {
                    // Update the product
                    //tiposdocumentos[-1] = updatedTipoDocumento;

                    // Return the updated product
                    return newTipoDocumento;
                }),*/
                switchMap(
                    tiposprogramas => this.getTipoProgramas().pipe(
                            tap((tiposprogramas) => {
                            //this._pagination.next(response.pagination);
                            this._tipoprogramas.next(tiposprogramas);
                            return tiposprogramas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param tipoprograma
     */
     updateTipoPrograma(id: string, tipoprograma: TipoPrograma): Observable<TipoPrograma>
     {
         return this.tiposprogramas$.pipe(
             take(1),
             switchMap(tiposprogramas => this._httpClient.put<TipoPrograma>('https://localhost:44307/api/TipoPrograma/' + id, {
                 nombre: tipoprograma.nombre
             }).pipe(
                 map((updatedTipoPrograma) => {

                     // Find the index of the updated product
                     const index = tiposprogramas.findIndex(item => item.idTipoPrograma.toString() == id);

                     // Update the product
                     tiposprogramas[index] = updatedTipoPrograma;

                     // Update the tiposdocumentos
                     this._tipoprogramas.next(tiposprogramas);

                     // Return the updated product
                     return updatedTipoPrograma;
                 }),
                 switchMap(updatedTipoPrograma => this.tipoprograma$.pipe(
                     take(1),
                     filter(item => item && item.idTipoPrograma.toString() == id),
                     tap(() => {

                         // Update the product if it's selected
                         this._tipoprograma.next(updatedTipoPrograma);

                         // Return the updated product
                         return updatedTipoPrograma;
                     })
                 ))
             ))
         );
     }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteTipoPrograma(id: string): Observable<boolean>
    {
        return this.tiposprogramas$.pipe(
            take(1),
            switchMap(tiposprogramas => this._httpClient.put('https://localhost:44307/api/TipoPrograma/statechange/' + id, {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = tiposprogramas.findIndex(item => item.idTipoPrograma.toString() == id);

                    // Delete the product
                    tiposprogramas.splice(index, 1);

                    // Update the products
                    this._tipoprogramas.next(tiposprogramas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
