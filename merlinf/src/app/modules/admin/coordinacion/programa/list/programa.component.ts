import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';//Tarea
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { Programa, ProgramaPagination } from 'app/modules/admin/coordinacion/programa/programa.types';
import { ProgramaService } from 'app/modules/admin/coordinacion/programa/programa.service';
import { TipoPrograma } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.types';
import { TipoProgramaService } from 'app/modules/admin/coordinacion/tipoPrograma/tipoPrograma.service';
import { Modalidad } from 'app/modules/admin/coordinacion/modalidad/modalidad.types';
import { ModalidadService } from 'app/modules/admin/coordinacion/modalidad/modalidad.service';
import { CoordinacionAcademica } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.types';
import { CoordinacionAcademicaService } from 'app/modules/admin/administrador/coordinacionAcademica/coordinacionAcademica.service';

@Component({
    selector       : 'programa-list',
    templateUrl    : './programa.component.html',
    styles         : [
        /* language=SCSS */
        `
            .coordinacion-academica-grid {
                grid-template-columns: 190px 190px auto;

                @screen sm {
                    grid-template-columns: 190px 190px 190px auto;
                }

                @screen md {
                    grid-template-columns: 190px 190px 190px 190px auto;
                }

                @screen lg {
                    grid-template-columns: 250px 250px 250px 250px 250px auto;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class programaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    programas$: Observable<Programa[]>;
    tiposprogramas: TipoPrograma[];
    modalidades: Modalidad[];
    coordinacionesacademicas: CoordinacionAcademica[];

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: ProgramaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedPrograma: Programa | null = null;
    selectedProgramaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _programaService: ProgramaService,
        private _tipoProgramaService: TipoProgramaService,
        private _modalidadService: ModalidadService,
        private _coordinacionAcademicaService: CoordinacionAcademicaService,
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedProgramaForm = this._formBuilder.group({
            idPrograma                 : [''],
            codigo                     : ['', [Validators.required]],
            nombre                     : ['', [Validators.required]],
            tipoProgramaId             : ['', [Validators.required]],
            nombreTipoPrograma         : ['', [Validators.required]],
            modalidadId                : ['', [Validators.required]],
            nombreModalidad            : ['', [Validators.required]],
            coordinacionAcademicaId    : ['', [Validators.required]],
            nombreCoordinacionAcademica: ['', [Validators.required]],
        });

        this._tipoProgramaService.tiposprogramas$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((tiposprogramas: TipoPrograma[]) => {
                
                // Update the categories
                this.tiposprogramas = tiposprogramas;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
        this._modalidadService.modalidades$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((modalidades: Modalidad[]) => {
                
                // Update the categories
                this.modalidades = modalidades;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
        this._coordinacionAcademicaService.coordinacionesacademicas$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((coordinacionesacademicas: CoordinacionAcademica[]) => {

                // Update the categories
                this.coordinacionesacademicas = coordinacionesacademicas;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });


        // Get the pagination
        this._programaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: ProgramaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.programas$ = this._programaService.programas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._programaService.getProgramas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._programaService.getProgramas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(programaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedPrograma && this.selectedPrograma.idPrograma === programaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._programaService.getProgramaId(programaId)
            .subscribe((programa) => {

                // Set the selected product
                this.selectedPrograma = programa;

                // Fill the form
                this.selectedProgramaForm.patchValue(programa);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedPrograma = null;
    }

    /**
     * Create tipoDocumento
     */
    createPrograma(): void
    {
        // Create the tipoDocumento
        this._programaService.createPrograma().subscribe((newPrograma) => {

            // Go to new tipoDocumento
            this.selectedPrograma = newPrograma;

            // Fill the form
            this.selectedProgramaForm.patchValue(newPrograma);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedPrograma(): void
    {
        // Get the product object
        const programa = this.selectedProgramaForm.getRawValue();

        if(programa.idPrograma == "-1"){
            this._programaService.createProgramaPost(programa).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.programas$ = this._programaService.programas$;
            this.selectedPrograma = null;
        } else{
            // Update the product on the server
            this._programaService.updatePrograma(programa.idPrograma, programa).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedPrograma(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Programa',
            message: 'Esta seguro que quiere eliminar el programa?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const programa = this.selectedProgramaForm.getRawValue();

                // Delete the product on the server
                this._programaService.deletePrograma(programa.idPrograma).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idPrograma || index;
    }
}
