import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { Programa, ProgramaPagination } from 'app/modules/admin/coordinacion/programa/programa.types';

@Injectable({
    providedIn: 'root'
})
export class ProgramaService
{
    // Private
    private _pagination: BehaviorSubject<ProgramaPagination | null> = new BehaviorSubject(null);
    private _programa: BehaviorSubject<Programa | null> = new BehaviorSubject(null);
    private _programas: BehaviorSubject<Programa[] | null> = new BehaviorSubject(null);

    private programanew: Programa = 
    {
        idPrograma: "-1",
        codigo: "",
        nombre: "",
        tipoProgramaId: "",
        nombreTipoPrograma: "",
        modalidadId: "",
        nombreModalidad: "",
        coordinacionAcademicaId: "",
        nombreCoordinacionAcademica: ""
    };

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<ProgramaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get programa$(): Observable<Programa>
    {
        return this._programa.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get programas$(): Observable<Programa[]>
    {
        return this._programas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getProgramas(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      //  Observable<{ /*pagination: CoordinacionAcademicaPagination;*/ tiposdocumentos: CoordinacionAcademica[] }>
      Observable<Programa[]>
    {
        return this._httpClient.get<Programa[]>('https://localhost:44307/api/Programa', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._programas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getProgramaId(id: string): Observable<Programa>
    {
        return this._programas.pipe(
            take(1),
            map((programas) => {

                // Find the product
                const programa = programas.find(item => item.idPrograma.toString() == id) || null;

                // Update the product
                this._programa.next(programa);

                // Return the product
                return programa;
            }),
            switchMap((programa) => {

                if ( !programa )
                {
                    return throwError('No se encontro el programa con el id ' + id + '!');
                }

                return of(programa);
            })
        );
    }

    /**
     * Create product
     */
    createPrograma(): Observable<Programa>
    {
        return this.programas$.pipe(
            take(1),
            switchMap(programas => this._httpClient.get<Programa>('https://localhost:44307/api/Programa', {}).pipe(
                map((newPrograma) => {

                    // Update the tiposdocumentos with the new product
                    let nt = this.programanew;
                    this._programas.next([nt, ...programas]);
                    // Return the new product
                    return nt;
                })
            ))
        );
    }



    createProgramaPost(persona): Observable<Programa[]>
    {
        return this.programas$.pipe(
            take(1),
            switchMap(programas => this._httpClient.post<Programa>('https://localhost:44307/api/Programa',
            {
                idPrograma: persona.idPrograma,
                codigo: persona.codigo,
                nombre: persona.nombre,
                tipoProgramaId: persona.tipoProgramaId,
                nombreTipoPrograma: persona.nombreTipoPrograma,
                modalidadId: persona.modalidadId,
                nombreModalidad: persona.nombreModalidad,
                coordinacionAcademicaId: persona.coordinacionAcademicaId,
                nombreCoordinacionAcademica: persona.nombreCoordinacionAcademica,
            }).pipe(
                switchMap(
                    programas => this.getProgramas().pipe(
                            tap((programas) => {
                            //this._pagination.next(response.pagination);
                            this._programas.next(programas);
                            return programas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param programa
     */
    updatePrograma(id: string, programa: Programa): Observable<Programa>
    {
        return this.programas$.pipe(
            take(1),
            switchMap(programas => this._httpClient.put<Programa>('https://localhost:44307/api/Programa/' + id, {
                idPrograma: programa.idPrograma,
                codigo: programa.codigo,
                nombre: programa.nombre,
                tipoProgramaId: programa.tipoProgramaId,
                nombreTipoPrograma: programa.nombreTipoPrograma,
                modalidadId: programa.modalidadId,
                nombreModalidad: programa.nombreModalidad,
                coordinacionAcademicaId: programa.coordinacionAcademicaId,
                nombreCoordinacionAcademica: programa.nombreCoordinacionAcademica,
            }).pipe(
                map((updatedPrograma) => {

                    // Find the index of the updated product
                    const index = programas.findIndex(item => item.idPrograma.toString() == id);

                    // Update the product
                    programas[index] = updatedPrograma;
                    

                    // Update the tiposdocumentos
                    this._programas.next(programas);

                    // Return the updated product
                    return updatedPrograma;
                }),
                switchMap(updatedPrograma => this.programa$.pipe(
                    take(1),
                    filter(item => item && item.idPrograma.toString() == id),
                    tap(() => {

                        // Update the product if it's selected
                        this._programa.next(updatedPrograma);

                        // Return the updated product
                        return updatedPrograma;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the product
     *
     * @param id
     */
    deletePrograma(id: string): Observable<boolean>
    {
        return this.programas$.pipe(
            take(1),
            switchMap(programas => this._httpClient.put('https://localhost:44307/api/Programa/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = programas.findIndex(item => item.idPrograma.toString() == id);

                    // Delete the product
                    programas.splice(index, 1);

                    // Update the tiposdocumentos
                    this._programas.next(programas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
