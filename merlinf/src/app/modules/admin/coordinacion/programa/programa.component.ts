import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'programa',
    templateUrl    : './programa.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class programaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
