export interface Programa
{
    idPrograma: string,
    codigo: string,
    nombre: string,
    tipoProgramaId: string,
    nombreTipoPrograma: string,
    modalidadId: string,
    nombreModalidad: string,
    coordinacionAcademicaId: string,
    nombreCoordinacionAcademica: string
}

export interface ProgramaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
