import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ProgramaService } from 'app/modules/admin/coordinacion/programa/programa.service';
import { Programa, ProgramaPagination } from 'app/modules/admin/coordinacion/programa/programa.types';



@Injectable({
    providedIn: 'root'
})
export class ProgramaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _programaService: ProgramaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Programa>
    {
        return this._programaService.getProgramaId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProgramasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _programaService: ProgramaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: TipoDocumentoPagination; products: TipoDocumento[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Programa[]>
    {
        return this._programaService.getProgramas();
    }
}
