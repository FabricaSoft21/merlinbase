import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'app/shared/shared.module';
import { ModalidadComponent } from './modalidad/modalidad.component';
import { ModalidadListComponent } from './modalidad/list/modalidad.component';
import { coordinacionRoutes } from './coordinacion.routing';
import { TipoProgramaComponent } from './tipoPrograma/tipoPrograma.component';
import { TipoProgramaListComponent } from './tipoPrograma/list/tipoPrograma.component';
import { EstadoQuejaComponent } from './estadoQueja/estadoQueja.component';
import { EstadoQuejaListComponent } from './estadoQueja/list/estadoQueja.component';
import { TipoQuejaComponent } from './tipoQueja/tipoQueja.component';
import { TipoQuejaListComponent } from './tipoQueja/list/tipoQueja.component';
import { programaComponent } from './programa/programa.component';
import { programaListComponent } from './programa/list/programa.component';
import { fichaComponent } from './Ficha/ficha.component';
import { fichaListComponent } from './Ficha/list/ficha.component';

@NgModule({
    declarations: [
        ModalidadComponent,
        ModalidadListComponent,
        TipoProgramaComponent,
        TipoProgramaListComponent,
        EstadoQuejaComponent,
        EstadoQuejaListComponent,
        TipoQuejaComponent,
        TipoQuejaListComponent,
        programaComponent,
        programaListComponent,
        fichaComponent,
        fichaListComponent
    ],
    imports     : [
        RouterModule.forChild(coordinacionRoutes),
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatRippleModule,
        MatSortModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatTooltipModule,
        SharedModule
    ]
})
export class CoordinacionModule
{
}
