import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TipoQuejaService } from 'app/modules/admin/coordinacion/tipoQueja/tipoQueja.service';
import { TipoQueja, TipoQuejaPagination } from 'app/modules/admin/coordinacion/tipoQueja/tipoQueja.types';



@Injectable({
    providedIn: 'root'
})
export class TipoQuejaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _tipoQuejaService: TipoQuejaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoQueja>
    {
        return this._tipoQuejaService.getTipoQuejaId(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class TiposQuejasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _tipoQuejaService: TipoQuejaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: TipoDocumentoPagination; products: TipoDocumento[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TipoQueja[]>
    {
        return this._tipoQuejaService.getTiposQuejas();
    }
}
