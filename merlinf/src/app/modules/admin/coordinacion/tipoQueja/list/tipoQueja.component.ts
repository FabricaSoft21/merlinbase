import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { TipoQueja, TipoQuejaPagination } from 'app/modules/admin/coordinacion/tipoQueja/tipoQueja.types';
import { TipoQuejaService } from 'app/modules/admin/coordinacion/tipoQueja/tipoQueja.service';

@Component({
    selector       : 'tipoqueja-list',
    templateUrl    : './tipoQueja.component.html',
    styles         : [
        /* language=SCSS */
        `
            .tipo-documento-grid {
                grid-template-columns: auto 40px;

                @screen sm {
                    grid-template-columns: auto 72px;
                }

                @screen md {
                    grid-template-columns: auto 72px;
                }

                @screen lg {
                    grid-template-columns: auto 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class TipoQuejaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    tiposquejas$: Observable<TipoQueja[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: TipoQuejaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedTipoQueja: TipoQueja | null = null;
    selectedTipoQuejaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _tipoQuejaService: TipoQuejaService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected tipoDocumento form
        this.selectedTipoQuejaForm = this._formBuilder.group({
            idTipoQueja    : [''],
            nombre             : ['', [Validators.required]]
        });

        // Get the pagination
        this._tipoQuejaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: TipoQuejaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the tipoDocumentos
        this.tiposquejas$ = this._tipoQuejaService.tiposquejas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoQuejaService.getTiposQuejas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._tipoQuejaService.getTiposQuejas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(tipoQuejaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedTipoQueja && this.selectedTipoQueja.idTipoQueja === tipoQuejaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._tipoQuejaService.getTipoQuejaId(tipoQuejaId)
            .subscribe((tipoqueja) => {

                // Set the selected product
                this.selectedTipoQueja = tipoqueja;

                // Fill the form
                this.selectedTipoQuejaForm.patchValue(tipoqueja);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedTipoQueja = null;
    }

    /**
     * Create tipoDocumento
     */
    createTipoQueja(): void
    {
        // Create the tipoDocumento
        this._tipoQuejaService.createTipoQueja().subscribe((newTipoQueja) => {

            // Go to new tipoDocumento
            this.selectedTipoQueja = newTipoQueja;

            // Fill the form
            this.selectedTipoQuejaForm.patchValue(newTipoQueja);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedTipoQueja(): void
    {
        // Get the product object
        const tipoqueja = this.selectedTipoQuejaForm.getRawValue();

        if(tipoqueja.idTipoQueja == "-1"){
            this._tipoQuejaService.createTipoQuejaPost(tipoqueja.nombre).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.tiposquejas$ = this._tipoQuejaService.tiposquejas$;
            this.selectedTipoQueja = null;
        } else{
            // Update the product on the server
            this._tipoQuejaService.updateTipoQueja(tipoqueja.idTipoQueja, tipoqueja).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
    deleteSelectedTipoQueja(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Tipo de Queja',
            message: 'Esta seguro que quiere eliminar el tipo de queja?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const tipoqueja = this.selectedTipoQuejaForm.getRawValue();

                // Delete the product on the server
                this._tipoQuejaService.deleteTipoQueja(tipoqueja.idTipoQueja).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        //this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            //this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idTipoQueja || index;
    }
}
