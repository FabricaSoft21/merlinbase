export interface TipoQueja
{
    idTipoQueja: string,
    nombre: string
}

export interface TipoQuejaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
