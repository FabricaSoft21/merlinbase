import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { TipoQueja, TipoQuejaPagination } from 'app/modules/admin/coordinacion/tipoQueja/tipoQueja.types';

@Injectable({
    providedIn: 'root'
})
export class TipoQuejaService
{
    // Private
    private _pagination: BehaviorSubject<TipoQuejaPagination | null> = new BehaviorSubject(null);
    private _tipoqueja: BehaviorSubject<TipoQueja | null> = new BehaviorSubject(null);
    private _tipoquejas: BehaviorSubject<TipoQueja[] | null> = new BehaviorSubject(null);

    private tipoquejanew: TipoQueja = {idTipoQueja: "-1", nombre: "Nuevo Tipo Queja"};

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------


    /**
     * Getter for pagination
     */
    get pagination$(): Observable<TipoQuejaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get tipoqueja$(): Observable<TipoQueja>
    {
        return this._tipoqueja.asObservable();
    }

    /**
     * Getter for tiposdocumentos
     */
    get tiposquejas$(): Observable<TipoQueja[]>
    {
        return this._tipoquejas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get tiposdocumentos
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getTiposQuejas(page: number = 0, size: number = 10, sort: string = 'nombre', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
      //  Observable<{ /*pagination: TipoDocumentoPagination;*/ tiposdocumentos: TipoDocumento[] }>
      Observable<TipoQueja[]>
    {
        return this._httpClient.get<TipoQueja[]>('https://localhost:44307/api/TipoQueja', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._tipoquejas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getTipoQuejaId(id: string): Observable<TipoQueja>
    {
        return this._tipoquejas.pipe(
            take(1),
            map((tiposquejas) => {

                // Find the product
                const tipoqueja = tiposquejas.find(item => item.idTipoQueja.toString() == id) || null;

                // Update the product
                this._tipoqueja.next(tipoqueja);

                // Return the product
                return tipoqueja;
            }),
            switchMap((tipoqueja) => {

                if ( !tipoqueja )
                {
                    return throwError('No se encontro un tipo de queja con el id ' + id + '!');
                }

                return of(tipoqueja);
            })
        );
    }

    /**
     * Create product
     */
    createTipoQueja(): Observable<TipoQueja>
    {
        return this.tiposquejas$.pipe(
            take(1),
            switchMap(tiposquejas => this._httpClient.get<TipoQueja>('https://localhost:44307/api/TipoQueja', {}).pipe(
                map((newTipoQueja) => {

                    // Update the tiposdocumentos with the new product
                    let nt = this.tipoquejanew;
                    this._tipoquejas.next([nt, ...tiposquejas]);
                    // Return the new product
                    return nt;
                })
            ))
        );
    }

    createTipoQuejaPost(nombre: string): Observable<TipoQueja[]>
    {
        return this.tiposquejas$.pipe(
            take(1),
            switchMap(tiposquejas => this._httpClient.post<TipoQueja>('https://localhost:44307/api/TipoQueja', {nombre: nombre}).pipe(
                switchMap(
                    tiposquejas => this.getTiposQuejas().pipe(
                            tap((tiposquejas) => {
                            //this._pagination.next(response.pagination);
                            this._tipoquejas.next(tiposquejas);
                            return tiposquejas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param tipoqueja
     */
    updateTipoQueja(id: string, tipoqueja: TipoQueja): Observable<TipoQueja>
    {
        return this.tiposquejas$.pipe(
            take(1),
            switchMap(tiposquejas => this._httpClient.put<TipoQueja>('https://localhost:44307/api/TipoQueja/' + id, {
                nombre: tipoqueja.nombre
            }).pipe(
                map((updatedTipoQueja) => {

                    // Find the index of the updated product
                    const index = tiposquejas.findIndex(item => item.idTipoQueja.toString() == id);

                    // Update the product
                    tiposquejas[index] = updatedTipoQueja;

                    // Update the tiposdocumentos
                    this._tipoquejas.next(tiposquejas);

                    // Return the updated product
                    return updatedTipoQueja;
                }),
                switchMap(updatedTipoQueja => this.tipoqueja$.pipe(
                    take(1),
                    filter(item => item && item.idTipoQueja.toString() == id),
                    tap(() => {

                        // Update the product if it's selected
                        this._tipoqueja.next(updatedTipoQueja);

                        // Return the updated product
                        return updatedTipoQueja;
                    })
                ))
            ))
        );
    }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteTipoQueja(id: string): Observable<boolean>
    {
        return this.tiposquejas$.pipe(
            take(1),
            switchMap(tiposquejas => this._httpClient.put('https://localhost:44307/api/TipoQueja/statechange/' + id , {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = tiposquejas.findIndex(item => item.idTipoQueja.toString() == id);

                    // Delete the product
                    tiposquejas.splice(index, 1);

                    // Update the tiposdocumentos
                    this._tipoquejas.next(tiposquejas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
