import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'tipoqueja',
    templateUrl    : './tipoQueja.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TipoQuejaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
