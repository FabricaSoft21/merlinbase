import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { EstadoQueja, EstadoQuejaPagination } from 'app/modules/admin/coordinacion/estadoQueja/estadoQueja.types';
import { EstadoQuejaService } from 'app/modules/admin/coordinacion/estadoQueja/estadoQueja.service';

@Component({
    selector       : 'estadoqueja-list',
    templateUrl    : './estadoQueja.component.html',
    styles         : [
        /* language=SCSS */
        `
            .inventory-grid {
                grid-template-columns: auto 40px;

                @screen sm {
                    grid-template-columns: auto 72px;
                }

                @screen md {
                    grid-template-columns: auto 72px;
                }

                @screen lg {
                    grid-template-columns: auto 72px;
                }
            }
        `
    ],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations     : fuseAnimations
})
export class EstadoQuejaListComponent implements OnInit, AfterViewInit, OnDestroy
{
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    estadosquejas$: Observable<EstadoQueja[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    pagination: EstadoQuejaPagination;
    searchInputControl: FormControl = new FormControl();
    selectedEstadoQueja: EstadoQueja | null = null;
    selectedEstadoQuejaForm: FormGroup;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseConfirmationService: FuseConfirmationService,
        private _formBuilder: FormBuilder,
        private _estadoQuejaService: EstadoQuejaService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected product form
        this.selectedEstadoQuejaForm = this._formBuilder.group({
            idEstadoQueja: [''],
            nombre     : ['', [Validators.required]]
        });

        // Get the pagination
        this._estadoQuejaService.pagination$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((pagination: EstadoQuejaPagination) => {

                // Update the pagination
                this.pagination = pagination;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Get the products
        this.estadosquejas$ = this._estadoQuejaService.estadosquejas$;

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(300),
                switchMap((query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._estadoQuejaService.getEstadoQuejas(0, 10, 'nombre', 'asc', query);
                }),
                map(() => {
                    this.isLoading = false;
                })
            )
            .subscribe();
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void
    {
        if ( this._sort && this._paginator )
        {
            // Set the initial sort
            this._sort.sort({
                id          : 'nombre',
                start       : 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get products if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).pipe(
                switchMap(() => {
                    this.closeDetails();
                    this.isLoading = true;
                    return this._estadoQuejaService.getEstadoQuejas(this._paginator.pageIndex, this._paginator.pageSize, this._sort.active, this._sort.direction);
                }),
                map(() => {
                    this.isLoading = false;
                })
            ).subscribe();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(estadoQuejaId: string): void
    {
        // If the product is already selected...
        if ( this.selectedEstadoQueja && this.selectedEstadoQueja.idEstadoQueja === estadoQuejaId )
        {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the product by id
        this._estadoQuejaService.getEstadoQuejaById(estadoQuejaId)
            .subscribe((tipoqueja) => {

                // Set the selected product
                this.selectedEstadoQueja = tipoqueja;

                // Fill the form
                this.selectedEstadoQuejaForm.patchValue(tipoqueja);

                /*// Mark for check
                this._changeDetectorRef.markForCheck();*/
            });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedEstadoQueja = null;
    }

    /**
     * Create product
     */
    createEstadoQueja(): void
    {
        // Create the product
        this._estadoQuejaService.createEstadoQueja().subscribe((newEstadoQueja) => {

            // Go to new product
            this.selectedEstadoQueja = newEstadoQueja;

            // Fill the form
            this.selectedEstadoQuejaForm.patchValue(newEstadoQueja);

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected product using the form data
     */
    updateSelectedEstadoQueja(): void
    {
        // Get the product object
        const estadoqueja = this.selectedEstadoQuejaForm.getRawValue();

        if(estadoqueja.idEstadoQueja == "-1"){
            this._estadoQuejaService.createEstadoQuejaPost(estadoqueja.nombre).subscribe(() =>{
                this.showFlashMessage('success');
            });
            this.estadosquejas$ = this._estadoQuejaService.estadosquejas$;
            this.selectedEstadoQueja = null;
        } else{
            // Update the product on the server
            this._estadoQuejaService.updateEstadoQueja(estadoqueja.idEstadoQueja, estadoqueja).subscribe(() => {

                // Show a success message
                this.showFlashMessage('success');
            });
        }
    }

    /**
     * Delete the selected product using the form data
     */
     deleteSelectedEstadoQueja(): void
    {
        // Open the confirmation dialog
        const confirmation = this._fuseConfirmationService.open({
            title  : 'Eliminar Estado Queja',
            message: 'Esta seguro que quiere eliminar el estado de la queja?!',
            actions: {
                confirm: {
                    label: 'Eliminar'
                }
            }
        });

        // Subscribe to the confirmation dialog closed action
        confirmation.afterClosed().subscribe((result) => {

            // If the confirm button pressed...
            if ( result === 'confirmed' )
            {

                // Get the product object
                const estadoqueja = this.selectedEstadoQuejaForm.getRawValue();

                // Delete the product on the server
                this._estadoQuejaService.deleteEstadoQueja(estadoqueja.idEstadoQueja).subscribe(() => {

                    // Close the details
                    this.closeDetails();
                });
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.idEstadoQueja || index;
    }
}
