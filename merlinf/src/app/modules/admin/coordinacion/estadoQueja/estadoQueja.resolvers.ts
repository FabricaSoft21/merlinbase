import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EstadoQuejaService } from 'app/modules/admin/coordinacion/estadoQueja/estadoQueja.service';
import { EstadoQueja, EstadoQuejaPagination } from 'app/modules/admin/coordinacion/estadoQueja/estadoQueja.types';


@Injectable({
    providedIn: 'root'
})
export class EstadoQuejaResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _estadoQuejaService: EstadoQuejaService,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EstadoQueja>
    {
        return this._estadoQuejaService.getEstadoQuejaById(route.paramMap.get('id'))
                   .pipe(
                       // Error here means the requested product is not available
                       catchError((error) => {

                           // Log the error
                           console.error(error);

                           // Get the parent url
                           const parentUrl = state.url.split('/').slice(0, -1).join('/');

                           // Navigate to there
                           this._router.navigateByUrl(parentUrl);

                           // Throw an error
                           return throwError(error);
                       })
                   );
    }
}

@Injectable({
    providedIn: 'root'
})
export class EstadoQuejasResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _estadoQuejaService: EstadoQuejaService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    //resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ pagination: ModalidadPagination; products: Modalidad[] }>
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EstadoQueja[]>
    {
        return this._estadoQuejaService.getEstadoQuejas();
    }
}
