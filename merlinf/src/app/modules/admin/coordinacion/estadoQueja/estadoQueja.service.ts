import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { EstadoQueja, EstadoQuejaPagination } from 'app/modules/admin/coordinacion/estadoQueja/estadoQueja.types';

@Injectable({
    providedIn: 'root'
})
export class EstadoQuejaService
{
    // Private
    private _pagination: BehaviorSubject<EstadoQuejaPagination | null> = new BehaviorSubject(null);
    private _estadoqueja: BehaviorSubject<EstadoQueja | null> = new BehaviorSubject(null);
    private _estadoquejas: BehaviorSubject<EstadoQueja[] | null> = new BehaviorSubject(null);

    private estadoquejanew: EstadoQueja = {idEstadoQueja: "-1", nombre: "Nuevo Tipo Programa"};

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for pagination
     */
    get pagination$(): Observable<EstadoQuejaPagination>
    {
        return this._pagination.asObservable();
    }

    /**
     * Getter for product
     */
    get estadoqueja$(): Observable<EstadoQueja>
    {
        return this._estadoqueja.asObservable();
    }

    /**
     * Getter for products
     */
    get estadosquejas$(): Observable<EstadoQueja[]>
    {
        return this._estadoquejas.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get products
     *
     *
     * @param page
     * @param size
     * @param sort
     * @param order
     * @param search
     */
    getEstadoQuejas(page: number = 0, size: number = 10, sort: string = 'name', order: 'asc' | 'desc' | '' = 'asc', search: string = ''):
        //Observable<{ pagination: InventoryPagination; products: InventoryProduct[] }>
        Observable<EstadoQueja[]>
    {
        return this._httpClient.get<EstadoQueja[]>('https://localhost:44307/api/EstadoQueja', {
            /*params: {
                page: '' + page,
                size: '' + size,
                sort,
                order,
                search
            }*/
        }).pipe(
            tap((response) => {
                //this._pagination.next(response.pagination);
                this._estadoquejas.next(response);
            })
        );
    }

    /**
     * Get product by id
     */
    getEstadoQuejaById(id: string): Observable<EstadoQueja>
    {
        return this._estadoquejas.pipe(
            take(1),
            map((estadoquejas) => {

                // Find the product
                const estadoqueja = estadoquejas.find(item => item.idEstadoQueja.toString() == id) || null;

                // Update the product
                this._estadoqueja.next(estadoqueja);

                // Return the product
                return estadoqueja;
            }),
            switchMap((estadoqueja) => {

                if ( !estadoqueja )
                {
                    return throwError('No se encontro el estado de la queja con el id ' + id + '!');
                }

                return of(estadoqueja);
            })
        );
    }

    /**
     * Create product
     */
    createEstadoQueja(): Observable<EstadoQueja>
    {
        return this.estadosquejas$.pipe(
            take(1),
            switchMap(estadosquejas => this._httpClient.get<EstadoQueja>('https://localhost:44307/api/EstadoQueja', {}).pipe(
                map((newEstadoQueja) => {

                    // Update the products with the new product

                    let nm = this.estadoquejanew;

                    this._estadoquejas.next([nm, ...estadosquejas]);

                    // Return the new product
                    return nm;
                })
            ))
        );
    }

    createEstadoQuejaPost(nombre: string): Observable<EstadoQueja[]>
    {
        return this.estadosquejas$.pipe(
            take(1),
            switchMap(estadosquejas => this._httpClient.post<EstadoQueja>('https://localhost:44307/api/EstadoQueja', {nombre: nombre}).pipe(
                /*map((newTipoDocumento) => {
                    // Update the product
                    //tiposdocumentos[-1] = updatedTipoDocumento;

                    // Return the updated product
                    return newTipoDocumento;
                }),*/
                switchMap(
                    estadosquejas => this.getEstadoQuejas().pipe(
                            tap((estadosquejas) => {
                            //this._pagination.next(response.pagination);
                            this._estadoquejas.next(estadosquejas);
                            return estadosquejas;
                        })
                    )
                )
            )
            )
        );
    }

    /**
     * Update product
     *
     * @param id
     * @param estadoqueja
     */
     updateEstadoQueja(id: string, estadoqueja: EstadoQueja): Observable<EstadoQueja>
     {
         return this.estadosquejas$.pipe(
             take(1),
             switchMap(estadosquejas => this._httpClient.put<EstadoQueja>('https://localhost:44307/api/EstadoQueja/' + id, {
                 nombre: estadoqueja.nombre
             }).pipe(
                 map((updatedEstadoQueja) => {

                     // Find the index of the updated product
                     const index = estadosquejas.findIndex(item => item.idEstadoQueja.toString() == id);

                     // Update the product
                     estadosquejas[index] = updatedEstadoQueja;

                     // Update the tiposdocumentos
                     this._estadoquejas.next(estadosquejas);

                     // Return the updated product
                     return updatedEstadoQueja;
                 }),
                 switchMap(updatedEstadoQueja => this.estadoqueja$.pipe(
                     take(1),
                     filter(item => item && item.idEstadoQueja.toString() == id),
                     tap(() => {

                         // Update the product if it's selected
                         this._estadoqueja.next(updatedEstadoQueja);

                         // Return the updated product
                         return updatedEstadoQueja;
                     })
                 ))
             ))
         );
     }

    /**
     * Delete the product
     *
     * @param id
     */
    deleteEstadoQueja(id: string): Observable<boolean>
    {
        return this.estadosquejas$.pipe(
            take(1),
            switchMap(estadosquejas => this._httpClient.put('https://localhost:44307/api/EstadoQueja/statechange/' + id, {}).pipe(
                map((isDeleted: boolean) => {

                    // Find the index of the deleted product
                    const index = estadosquejas.findIndex(item => item.idEstadoQueja.toString() == id);

                    // Delete the product
                    estadosquejas.splice(index, 1);

                    // Update the products
                    this._estadoquejas.next(estadosquejas);

                    // Return the deleted status
                    return isDeleted;
                })
            ))
        );
    }
}
