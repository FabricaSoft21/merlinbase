export interface EstadoQueja
{
    idEstadoQueja: string;
    nombre: string;
}

export interface EstadoQuejaPagination
{
    length: number;
    size: number;
    page: number;
    lastPage: number;
    startIndex: number;
    endIndex: number;
}
