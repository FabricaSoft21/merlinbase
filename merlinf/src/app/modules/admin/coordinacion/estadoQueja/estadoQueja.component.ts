import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector       : 'estadoqueja',
    templateUrl    : './estadoQueja.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EstadoQuejaComponent
{
    /**
     * Constructor
     */
    constructor()
    {
    }
}
